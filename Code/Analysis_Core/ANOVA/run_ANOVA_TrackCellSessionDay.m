%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [dataStructure] = run_ANOVA_TrackCellSessionDay(dataStructure)

% Unpack the structure              
    v2struct(dataStructure);
    
% Update the relevant IDs
    trackIDByTrackForANOVAResults = (1:numTracks)';
    cellIDByTrackForANOVAResults = cellIDByTrack;
    imagingSessionIDByTrackForANOVAResults = imagingSessionIDByTrack;
    dayIDByTrackForANOVAResults = dayIDByTrack;
           
% Run the n-way nested anova    
    % ANOVA on ln(D)
    'Performing ANOVA on ln(D)'
    [p_ln_D,tbl_ln_D] = anovan(...
        log(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack),...
        {trackIDByTrackForANOVAResults cellIDByTrackForANOVAResults ...
        imagingSessionIDByTrackForANOVAResults ...
        dayIDByTrackForANOVAResults},'varnames',...
        {'Track','Cell','Session','Day'},'nested',...
        [0 1 1 1; 0 0 1 1; 0 0 0 1; 0 0 0 0 ]);    
    % ANOVA on alpha
    'Performing ANOVA on alpha'
    [p_Alpha,tbl_Alpha] = anovan(MSDFitResults.alphaVal_FitMeanByTrack,...
        {trackIDByTrackForANOVAResults cellIDByTrackForANOVAResults ...
        imagingSessionIDByTrackForANOVAResults ...
        dayIDByTrackForANOVAResults},'varnames',...
        {'Track','Cell','Session','Day'},'nested',...
        [0 1 1 1; 0 0 1 1; 0 0 0 1; 0 0 0 0 ]); 
    
% Load the results into the structure
    ANOVAResults.trackIDByTrackForANOVAResults = ...
        trackIDByTrackForANOVAResults;
    ANOVAResults.cellIDByTrackForANOVAResults = ...
        cellIDByTrackForANOVAResults;
    ANOVAResults.imagingSessionIDByTrackForANOVAResults = ...
        imagingSessionIDByTrackForANOVAResults;
    ANOVAResults.dayIDByTrackForANOVAResults = ...
        dayIDByTrackForANOVAResults;
    ANOVAResults.p_ln_D = p_ln_D;
    ANOVAResults.tbl_ln_D = tbl_ln_D;
    ANOVAResults.p_Alpha = p_Alpha;
    ANOVAResults.tbl_Alpha = tbl_Alpha;
    
% Add the results to the structure  
    dataStructure.ANOVAResults = ANOVAResults;

