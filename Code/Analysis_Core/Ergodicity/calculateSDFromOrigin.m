%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [SD] = calculateSDFromOrigin(data,dim2CalcMSD,dim2SumMSD)

% This function intakes a 3D matrix, and calculates the squared 
% displacement over all possible intervals along one of the three dimensions 
% This function was written and developed by Rikki M. Garner and was last
% updated on 2022/12/16.

% Inputs: 
    % data = A 3D matrix
    % dim2CalcMSD = An integer storing the dimension over which to
        % calculate the MSD
    % dim2SumMSD = An integer storing the dimension over which to
        % sum the MSD (eg., physical dimensions)
% Outputs: 
    % SD = A 2D matrix, in which the first dimension is the number of 
        % particles, and the 2nd dimension is the interval over which 
        % the MSD was calculated (e.g., time interval)   

% Reshuffle the matrix dimensions to put the dimension we want to find
% the MSD over as the last dimension 
    % Calculate the number of dimensions
        numDimensions = length(size(data));
    % Make a vector of the data dimensions
        dimensions_shuffled = 1:numDimensions;
    % Remove the dimension we're averaging over
        dimensions_shuffled(dimensions_shuffled==dim2CalcMSD)=[];
    % Re-add that dimension to the end
        dimensions_shuffled = [dimensions_shuffled dim2CalcMSD];
    % Re-calculate the new dimension to sum the MSD
        dim2SumMSD = find(dimensions_shuffled==dim2SumMSD);
    % Reshuffle the matrix dimensions to put the dim2CalcMSD last
        data_reshuffled = permute(data,dimensions_shuffled);
    % Calculate the number of measurements
        numMeasurements = size(data_reshuffled,numDimensions);

% Calculate the SD
    SD = squeeze(sum((data_reshuffled(:,:,1:numMeasurements) - ...
         data_reshuffled(:,:,1)).^2,dim2SumMSD));



    
    
    
    
    
    
    