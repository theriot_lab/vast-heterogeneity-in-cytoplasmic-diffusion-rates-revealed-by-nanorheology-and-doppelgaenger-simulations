% This script takes in the 
% variable trackPositions_nm, a [numParticles x numDimensions x numTimepoints]
% matrix of particle positions, and msPerTimeStep, the timestep. It then
% calls the function calculateMSDNonOverlapping to calculate the time-average 
% MSD along each dimension for each track, and then removes MSD measurements 
% for each track/interval combo without at least 3 datapoints to average 
% over. Finally, it calls the function calculateSummaryStats
% calculates the ensemble-averaged, time-averaged MSD for each dimension 
% separately as well as the total MSD, averaging either by cell or by condition. 
% This script was written and developed by Rikki M. Garner and was last
% updated on 2021/06/09.

function [dataStructure] = performMSD_EnsembleAverageByTrackCellAndCondition(dataStructure)
    
% Unpack the structure              
    v2struct(dataStructure);
        
% Perform the time-averaged MSD for each track
    % Choose the dimension across which to calculate the MSD
        dim2CalcMSD = 3;
    % Perform the MSD calculation (return time-averaged MSD for each track)
        [MSDResults.MSD_nm2_TimeAvgByTrack_dims, MSDResults.numSDMeasurements_AvgByTrack] = ...
            calculateMSDNonOverlapping(trackPositions_nm,dim2CalcMSD);
    % Trim the MSD to exclude measurements averaged over fewer than 3 data
    % points along the track
        MSDResults.MSD_nm2_TimeAvgByTrack_dims(MSDResults.numSDMeasurements_AvgByTrack<3) = nan;

% Perform the ensemble-averaged MSD for each T compared to t=0
    % Choose the dimension across which to calculate the MSD
        dim2CalcEnsAvgMSD = 1;
    % Choose the dimension across which to sum the MSD
        dim2SumMSD = 2;
%     % Perform the MSD calculation (return ensemble-averaged MSD for each T compared to t=0)
%         [MSDResults.MSD_nm2_EnsAvg_dims, MSDResults.SDSD_nm2_EnsAvg_dims, ...
%             MSDResults.SESD_nm2_EnsAvg_dims, MSDResults.numSDMeasurements_EnsAvg] = ...
%             calculateEnsembleAveragedMSD(trackPositions_nm,dim2CalcMSD,dim2CalcEnsAvgMSD);
%     % Create the associated time interval vector
%         % Determine the number of time intervals
%             MSDResults.numTimeOIntervals_MSD = size(MSDResults.MSD_nm2_TimeAvgByTrack_dims,3)-1;
%         % Create the time offset vector
%             MSDResults.time_interval_for_MSD_ms = msPerTimeStep*(0:MSDResults.numTimeOIntervals_MSD); 
%     % Trim the MSD to exclude measurements averaged over fewer than 3 data
%     % points along the track
%         MSDResults.MSD_nm2_EnsAvg_dims(MSDResults.numSDMeasurements_EnsAvg<3) = nan; 
    % Perform the MSD calculation (return ensemble-averaged MSD for each T compared to t=0)
        [MSDResults.MSD_nm2_EnsAvg, MSDResults.SDSD_nm2_EnsAvg, ...
            MSDResults.SESD_nm2_EnsAvg, MSDResults.numSDMeasurements_EnsAvg] = ...
            calculateEnsembleAveragedTotalMSD(trackPositions_nm,dim2CalcMSD,dim2SumMSD,dim2CalcEnsAvgMSD);
    % Create the associated time interval vector
        % Determine the number of time intervals
            MSDResults.numTimeOIntervals_MSD = length(MSDResults.MSD_nm2_EnsAvg)-1;
        % Create the time offset vector
            MSDResults.time_interval_for_MSD_ms = msPerTimeStep*(0:MSDResults.numTimeOIntervals_MSD); 
    % Trim the MSD to exclude measurements averaged over fewer than 3 data
    % points along the track
        MSDResults.MSD_nm2_EnsAvg(MSDResults.numSDMeasurements_EnsAvg<3) = nan; 
  
% Perform the ensemble(track)-averaged, time-averaged MSD (along with
% associated summary statistics)
    % Choose the dimension to average over
        dim2AverageOver = 1;
    % Perform ensemble-averaging by cell
        % Calculate the ensemble-averaged MSD and summary stats separately for 
        % each dimension    
            [MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCell_dims] = ...
                calculateSummaryStatsByCondition(MSDResults.MSD_nm2_TimeAvgByTrack_dims,...
                dim2AverageOver,cellIDByTrack);
        % Calculate the total ensemble-averaged MSD and summary stats
            [MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCell] = ...
                calculateSummaryStatsByCondition(squeeze(sum(MSDResults.MSD_nm2_TimeAvgByTrack_dims,2)),...
                dim2AverageOver,cellIDByTrack);
    % Perform ensemble-averaging by condition
        % Calculate the ensemble-averaged MSD and summary stats separately for 
        % each dimension    
            [MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition_dims] = ...
                calculateSummaryStatsByCondition(MSDResults.MSD_nm2_TimeAvgByTrack_dims,...
                dim2AverageOver,conditionIDByTrack);
        % Calculate the total ensemble-averaged MSD and summary stats
            [MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition] = ...
                calculateSummaryStatsByCondition(squeeze(sum(MSDResults.MSD_nm2_TimeAvgByTrack_dims,2)),...
                dim2AverageOver,conditionIDByTrack);



    
            
% Add the results to the structure  
    dataStructure.MSDResults = MSDResults;
    
end