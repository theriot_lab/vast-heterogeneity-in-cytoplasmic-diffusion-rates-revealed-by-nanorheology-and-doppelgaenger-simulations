%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [error] = propagate_error_addition_noCovariance(A,errorA,B,errorB)

    error = sqrt(errorA.^2+errorB.^2);

end