%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [errorAB] = propagate_error_division_noCovariance(A,errorA,B,errorB)

    errorAB = sqrt((A./B).^2.*((errorA./A).^2+(errorB./B).^2));

end