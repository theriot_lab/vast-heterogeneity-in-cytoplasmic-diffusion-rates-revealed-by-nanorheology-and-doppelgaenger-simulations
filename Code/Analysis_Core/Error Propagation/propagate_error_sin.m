%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [error] = propagate_error_sin(A,errorA)

    error = sqrt((cos(A).*errorA).^2);

end