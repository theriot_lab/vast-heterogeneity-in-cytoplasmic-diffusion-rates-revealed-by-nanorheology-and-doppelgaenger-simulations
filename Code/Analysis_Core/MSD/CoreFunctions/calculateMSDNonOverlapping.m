%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [MSD, numSDMeasurements] = calculateMSDNonOverlapping(data,dim2CalcMSD)

% This function intakes a 3D matrix, and calculates the average mean-squared 
% displacement over all possible intervals along one of the three dimensions 
% This function was written and developed by Rikki M. Garner and was last
% updated on 2020/10/15.

% Inputs: 
    % data = A 3D matrix
    % dim2CalcMSD = An integer storing the dimension over which to
        % calculate the MSD
% Outputs: 
    % MSD = A 3D matrix, in which the intervals for the dimension over 
        % which the MSD was calculated is now located along the last 
        % dimension, and the other two dimensions have been shifted to the
        % left as necessary
    % numSDMeasurements = a 3D matrix the size of MSD holding the number of
    % measurements used to calculate the MSD
       

% Reshuffle the matrix dimensions to put the dimension we want to find
% the MSD over as the last dimension 
    % Calculate the number of dimensions
        numDimensions = length(size(data));
    % Make a vector of the data dimensions
        dimensions_shuffled = 1:numDimensions;
    % Remove the dimension we're averaging over
        dimensions_shuffled(dimensions_shuffled==dim2CalcMSD)=[];
    % Re-add that dimension to the end
        dimensions_shuffled = [dimensions_shuffled dim2CalcMSD];
    % Reshuffle the matrix dimensions to put the dim2CalcMSD last
        data_reshuffled = permute(data,dimensions_shuffled);
    % Calculate the new dimenstion sizes
        dimLengths_reshuffled = size(data_reshuffled);

% Preallocate space to store the measured MSD values
    % Calculate the number of measurements
        numMeasurements = size(data_reshuffled,numDimensions);
    % Choose the number of different intervals to calculate the MSD over
        numIntervals = (numMeasurements-1);
    % Create a cell to store the MSD values
        MSD = nan([dimLengths_reshuffled(1:(end-1)), numIntervals+1]);
    % Create a vector to store the number of SD values for each MSD
    % measurement
        numSDMeasurements = nan([dimLengths_reshuffled(1:(end-1)), ...
            numIntervals+1]);
    
% Loop through each interval and calculate the MSD
for intervalNum = 0:numIntervals
    % Pull out the non-overlapping timepoints to use
        timePoints1 = 1:max(1,intervalNum):(numMeasurements-intervalNum);
        timePoints2 = (1+intervalNum):max(1,intervalNum):numMeasurements;
    % Calculate the dim2CalcMSD-averaged SD
        MSD(:,:,intervalNum+1) = nanmean((data_reshuffled(:,:,timePoints2) - ...
            data_reshuffled(:,:,timePoints1)).^2,numDimensions);
    % Calculate the number of non-nan SD measurements
        numSDMeasurements(:,:,intervalNum+1) = sum(~isnan(data_reshuffled(:,:,timePoints2) - ...
            data_reshuffled(:,:,timePoints1)),numDimensions);
end

    
    
    
    
    
    
    