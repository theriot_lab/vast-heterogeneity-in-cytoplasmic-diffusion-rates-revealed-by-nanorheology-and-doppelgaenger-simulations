%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [powerLawExponent, powerLawExponent_CI, prefactor,prefactor_CI] = ...
    fitDataToPowerLaw(xVals,yVals,xValForPrefactor,alphaForCI,doPlot)

% This function takes in two vectors (xVals and yVals) and performs a best 
% fit of the data to the function ...
% yVals = prefactor*(xVals/xValForPrefactor)^powerLawExponent,
% outputting the best fit parameters as well as the associated confidence
% intervals. In this case the prefactor predicts the value of yVal when 
% xVal = xValForPrefactor.
% This function was written by Rikki M. Garner and last updated on
% 2021/10/04.

% Inputs: 
    % xVals = A vector of data points
    % yVals = A vector of equal length to xVals
    % xValForPrefactor = A double containing the xVal for which to
        % evaluate the prefactor (the prefactor predicts the value of 
        % yVal when xVal = xValForPrefactor)
    % alphaForCI = The alpha value (a fraction between 0 and 1) for 
        % calculating the confidence interval
    % doPlot = A boolean determining whether to plot the fit
% Outputs: 
    % powerLawExponent = A double containing the best fit power law exponent
    % powerLawExponent_CI = A double containing the confidence intervals
        % for the best fit of the power law exponent
    % prefactor = A double containing the best fit power law prefactor
    % prefactor_CI = A double containing the confidence intervals
        % for the best fit of the power law prefactor

% Perform the fit 
    % Find the datapoints to fit (must exclude nans and values <0, as we
    % will take the log of the yVals)
        datapoints2Fit = ((~isnan(yVals))&(yVals>0));
    % If there are at least three valid intervals, fit the
    % MSD vs time interval to a power law
    if sum(datapoints2Fit)>=3        
        % Perform the fit
            % Example with regress
                % Fit these data points and calculate confidence intervals for the fit
                    % Run the regress algorithm and reformat the data
                        [p,cib] = regress(log(yVals(datapoints2Fit))',...
                            [log(xVals(datapoints2Fit)./xValForPrefactor)', ...
                            ones(size(xVals(datapoints2Fit)'))],...
                            alphaForCI);
                        % Reformat the data
                            p = p';
                            cib = cib';
                        % Convert the confience interval bounds to
                        % confidence intervals
                            ci = cib-[p;p];
%             % Also possible with polyfit
%                 % Fit these data points
%                     [p,S] = polyfit(log(xVals(datapoints2Fit)),log(yVals(datapoints2Fit)),1);
%                 % Calculate confidence intervals for the fit
%                     cib = polyparci(p,S,alphaForCI);     
%                 % Convert the confience interval bounds to
%                 % confidence intervals
%                     ci = cib-[p;p];                
        % Record the fit results        
            % Store the power law exponent value
                powerLawExponent = p(1);  
            % Store the confidence intervals for the power law exponent 
                powerLawExponent_CI = ci(:,1);
            % Store the prefactor value
            % for each track
                prefactor = exp(p(2)); 
            % Store the confidence intervals for the prefactor
                prefactor_CI = propagate_error_exponentiation(p(2),ci(:,2));
                
        % Plot the result
        if doPlot
            figure(11);
            loglog(xVals,yVals,'ro')
            hold on;
            loglog(xVals(datapoints2Fit),...
                prefactor*xVals(datapoints2Fit).^powerLawExponent,'r--');
            plot(xVals(datapoints2Fit),...
                prefactor*xVals(datapoints2Fit).^powerLawExponent,'r--');
            hold off;
        end
                
    else
        % Return nans      
            powerLawExponent = nan;   
            powerLawExponent_CI = nan([1 2]);
            prefactor = nan; 
            prefactor_CI = nan([1 2]);
    end

end