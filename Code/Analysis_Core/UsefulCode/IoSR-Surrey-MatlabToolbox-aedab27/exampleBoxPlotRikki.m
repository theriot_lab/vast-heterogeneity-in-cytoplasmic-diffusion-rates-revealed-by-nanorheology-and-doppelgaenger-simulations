    
%% Example with many conditions for a single group

    % Prepare the sample data
        % Choose the number of data points you want to generate for each
        % condition
            numDataPoints = 100;
        % Choose the mean value for each condition
            meansByCondition = [1 2 3];
        % Choose the standard deviation for each condition
            stdsByCondition = [1 2 3];
        % Choose the names for each condition
            namesByCondition = {'Condition 1','Condition 2', 'Condition 3'};
        % Choose what color you want to plot each condition (in this case
        % red,green, and blue)
            markerColorsByCondition = {[1 0 0],[0 1 0],[0 0 1]};
        % Calculate the number of conditions
            numConditions = length(meansByCondition);

        % Create an n by m by w matrix containing n datapoints each for m groups and w
        % conditions within each group using a random number generator
        % (In this case we only have one group)
            data2Plot = nan(numDataPoints,1,numConditions);
            for numCondition = 1:numConditions
                    data2Plot(:,1,numCondition) = meansByCondition(numCondition) + stdsByCondition(numCondition)*randn([1 numDataPoints]);
            end

    % Run the plotting
        figure(1)
        h = iosr.statistics.boxPlot({''},data2Plot,'showScatter',true,'scattercolor',markerColorsByCondition,...
            'scattermarker','.','scatterSize',180,'showMean',true,'meanMarker','*','meanColor','k','medianColor','k','symbolMarker','.','outlierSize',180,...
            'groupLabels',namesByCondition,'symbolColor',markerColorsByCondition,'style','hierarchy');
        title({'Value distributions by Condition'});
        ylabel('Value')  
        set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold');
    
%% Example with both groups and conditions

    % Prepare the sample data
        % Choose the number of data points you want to generate for each
        % condition
            numDataPoints = 100;
        % Choose the mean value for each condition
            meansByCondition = [1 2 3];
        % Choose the standard deviation for each condition
            stdsByCondition = [1 2 3];
        % Choose the names for each condition
            namesByCondition = {'Condition 1','Condition 2', 'Condition 3'};
        % Choose the names for each group
            namesByGroup = {'Group 1','Group 2', 'Group 3'};
        % Choose what color you want to plot each condition (in this case
        % red,green, and blue)
            markerColorsByCondition = {[1 0 0],[0 1 0],[0 0 1]};
        % Calculate the number of conditions
            numConditions = length(meansByCondition);
        % Calculate the number of groups
            numGroups = length(namesByGroup);

        % Create an n by m by w matrix containing n datapoints each for m groups and w
        % conditions within each group using a random number generator
        % (In this case we only have one group)
            data2Plot = nan(numDataPoints,1,numConditions);
            for numCondition = 1:numConditions
                for numGroup = 1:numGroups
                    data2Plot(:,numGroup,numCondition) = meansByCondition(numCondition) + stdsByCondition(numCondition)*randn([1 numDataPoints]);
                end
            end

    % Run the plotting
        figure(2)
        h = iosr.statistics.boxPlot(namesByGroup,data2Plot,'showScatter',true,'scattercolor',markerColorsByCondition,...
            'scattermarker','.','scatterSize',180,'showMean',true,'meanMarker','*','meanColor','k','medianColor','k','symbolMarker','.','outlierSize',180,...
            'groupLabels',namesByCondition,'symbolColor',markerColorsByCondition,'style','hierarchy');
        title({'Value distributions by Condition'});
        ylabel('Value')  
        set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold');