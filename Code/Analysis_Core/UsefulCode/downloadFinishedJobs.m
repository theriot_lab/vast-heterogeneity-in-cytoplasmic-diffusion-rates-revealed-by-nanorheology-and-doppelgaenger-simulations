% Clear the system
    close all; 
    clear all;
    
% Start up the cluster
    parallel.defaultClusterProfile('Workers_120_V_R2017b_1');
    parallel.defaultClusterProfile('Workers_120_V_R2017b_2');
    myCluster = parcluster;
    start(myCluster);
    wait(myCluster);
    shutdown(myCluster,'After','never')
    
%% Delete all completed jobs
   finishedJobs = findJob(myCluster,'State','finished');
     %   finishedJobs = findJob(myCluster,'State','failed');
     %   finishedJobs = findJob(myCluster,'State','pending');

    for numJob = 1:length(finishedJobs)
        numJob
        job = finishedJobs(numJob,1);
        delete(job)
%         try
%             jobCompletedParameterScanReplicates(job)
%         catch ME
%             ME
%         end
    end
%%
% % Find all running jobs that have been running for more than a week
%     runningJobs = findJob(myCluster,'State','running');
%     for numJob = 1:length(runningJobs)
%         job = runningJobs(numJob,1);
%         runTime = datetime(now,'ConvertFrom','datenum','TimeZone','local') - ...
%             datetime(job.StartDateTime,'TimeZone','local');
%         if runTime>duration([24*7,0,0])
%            delete(job) 
%         end
%     end    
%         
% % Delete all pending jobs that still haven't started after a week for some reason
%     allJobs = findJob(myCluster);
%     if length(allJobs)<120
%         pendingJobs = findJob(myCluster,'State','pending');
%         for numJob = 1:length(pendingJobs)
%             job = pendingJobs(numJob,1);
%             pendingTime = datetime(now,'ConvertFrom','datenum','TimeZone','local') - ...
%                 datetime(job.CreateDateTime,'TimeZone','local');
%             if pendingTime>duration([24*7,0,0])
%                delete(job) 
%             end
%         end 
%     end
    
%% Optionally delete all jobs on the cluster

    allJobs = findJob(myCluster);
    for numJob = 1:length(allJobs)
        job = allJobs(numJob,1);
        numJob
        delete(job) 
    end
%%       
    
% Shut down the cluster when idle
    shutdown(myCluster,'After','idle')