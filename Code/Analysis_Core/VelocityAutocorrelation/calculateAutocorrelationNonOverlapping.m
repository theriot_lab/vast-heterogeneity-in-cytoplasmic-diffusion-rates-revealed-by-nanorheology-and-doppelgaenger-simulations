%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [autocorrelation] = calculateAutocorrelationNonOverlapping(data,dim2CalcAutocorr)

% This function intakes a 3D matrix, and calculates the average autocorrelation
% over all possible offsets intervals along one of the three dimensions 

% Inputs: 
    % data = A 3D matrix
    % dim2CalcAutocorr = An integer storing the dimension over which to
        % calculate the autocorrelation
% Outputs: 
    % autocorrelation = A 3D matrix, in which the dimension over which the 
        % autocorrelation was calculated is now the last dimension, and 
        % the other two dimensions have been shifted to the left as 
        % necessary        

% Reshuffle the matrix dimensions to put the dimension we want to find the 
% autocorrelation over last 
    % Calculate the number of dimensions
        numDimensions = length(size(data));
    % Make a vector of the data dimensions
        dimensions_shuffled = 1:numDimensions;
    % Remove the dimension we're averaging over
        dimensions_shuffled(dimensions_shuffled==dim2CalcAutocorr)=[];
    % Re-add that dimension to the end
        dimensions_shuffled = [dimensions_shuffled dim2CalcAutocorr];
    % Reshuffle the matrix dimensions to put the dim2CalcAutocorr last
        data_reshuffled = permute(data,dimensions_shuffled);
    % Calculate the new dimenstion sizes
        dimLengths_reshuffled = size(data_reshuffled);

% Preallocate space to store the measured autocorrelation values
    % Calculate the number of measurements
        numMeasurements = size(data_reshuffled,numDimensions);
    % Choose the number of different offsets to calculate the
    % autocorrelation
        numOffsets = numMeasurements-1;        
    % Create a matrix to store the autocorrelation values
        autocorrelation = nan([dimLengths_reshuffled(1:(end-1)), numOffsets+1]);
    
% Loop through each discretization and calculate the autocorrelation
for timeOffsetNum = 0:numOffsets
    % Pull out the timepoints to use
        timePoints1 = 1:max(1,timeOffsetNum):(numMeasurements-timeOffsetNum);
        timePoints2 = (1+timeOffsetNum):max(1,timeOffsetNum):numMeasurements;
    % Calculate and record the dim2CalcAutocorr-averaged autocorrelation values
        autocorrelation(:,:,timeOffsetNum+1) = nanmean(data_reshuffled(:,:,timePoints2).*...
            conj(data_reshuffled(:,:,timePoints1)),numDimensions);
end
