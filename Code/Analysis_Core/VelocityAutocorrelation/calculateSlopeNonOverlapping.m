%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [slopes] = calculateSlopeNonOverlapping(data,dim2CalcSlope,varargin)

% This function intakes a 3D matrix, and calculates the slope/derivative
% over all possible intervals along one of the three dimensions 

% Inputs: 
    % data = A 3D matrix
    % dim2CalcSlope = An integer storing the dimension over which to
        % calculate the slope
    % varargin = an optional input variable storing a list of time offsets 
        % with which to calculate the slopes
% Outputs: 
    % slopes = a {1 x numIntervals} cell, where each cell contains a 3D 
        % matrix, in which the dimension over which the slopes were
        % calculated is now located along the last dimension, and the 
        % other two dimensions have been shifted to the left as necessary
        
% Reshuffle the matrix dimensions to put the dimension we want to find
% the slopes over as the last dimension 
    % Calculate the number of dimensions
        numDimensions = length(size(data));
    % Make a vector of the data dimensions
        dimensions_shuffled = 1:numDimensions;
    % Remove the dimension we're averaging over
        dimensions_shuffled(dimensions_shuffled==dim2CalcSlope)=[];
    % Re-add that dimension to the end
        dimensions_shuffled = [dimensions_shuffled dim2CalcSlope];
    % Reshuffle the matrix dimensions to put the dim2CalcSlope last
        data_reshuffled = permute(data,dimensions_shuffled);

% Preallocate space to store the measured slopes
    % Calculate the number of measurements
        numMeasurements = size(data_reshuffled,numDimensions);
    % Choose the number of different intervals to calculate the slope over
        if isempty(varargin)
            numIntervals = (numMeasurements-1);
            intervalstoCompute = 1:numIntervals;
        else
            intervalstoCompute = varargin{1};
            numIntervals = length(intervalstoCompute);
        end
    % Create a cell to store the slopes for each interval choice
        slopes = cell([1,numIntervals]);
    
% Loop through each interval and calculate the slopes
for intervalNum = 1:numIntervals
    % Pull out the inveral length
        intervalLength = intervalstoCompute(intervalNum);
    % Pull out the timepoints to use
        timePoints1 = 1:intervalLength:(numMeasurements-intervalLength);
        timePoints2 = (1+intervalLength):intervalLength:numMeasurements;  
    % Calculate the slopes
        slopes{1,intervalNum} = (data_reshuffled(:,:,timePoints2) - ...
            data_reshuffled(:,:,timePoints1))./intervalLength;
end

    
    
    
    
    
    
    