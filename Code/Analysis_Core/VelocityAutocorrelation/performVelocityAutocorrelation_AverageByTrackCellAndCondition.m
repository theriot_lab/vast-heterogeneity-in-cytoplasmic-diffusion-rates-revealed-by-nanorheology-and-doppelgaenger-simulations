%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% This script is called by analyzeGEMTracks_OneExampleFile.m, and takes in 
% the variable trackPositions_nm, a [numParticles x numDimensions x 
% numTimepoints] matrix of particle positions, and msPerTimeStep, the 
% timestep. It then calculates the velocity of each particle at each 
% timestep for a series of temporal discretizations using the function 
% calculateSlopeNonOverlapping.m. It then chooses one temporal
% discretization, and uses those velocity values to calculate the 
% time-averaged velocity autocorrelation (non-overlapping intervals) as a 
% function of time offset for each particle using the function 
% calculateAutocorrelationNonOverlapping.m. Finally, it calls 
% the function calculateSummaryStats to calculate the ensemble-averaged, 
% time-averaged velocity aurocorrelation for each dimension separately as
% well as the total. 

function [dataStructure] = performVelocityAutocorrelation_AverageByTrackCellAndCondition(dataStructure)
    
% Unpack the structure              
    v2struct(dataStructure);

%% Calculate the velocities

    % Calculate the number of timepoints
        numTimepoints = size(trackPositions_nm,3);

    % Calculate the velocities (with non-overlapping intervals)
        % Choose the dimension over which to calculate the slope
            VelAutocorrResults.dim2CalcSlope = 3;
        % Choose the interval length used to calculate the slope
        % (timepoints)
            VelAutocorrResults.intervalLengths2Try = 1;
        % Perform the calculation
            [VelAutocorrResults.velocities_nm_per_frame] = ...
                calculateSlopeNonOverlapping(trackPositions_nm,...
                VelAutocorrResults.dim2CalcSlope,VelAutocorrResults.intervalLengths2Try);
  
%% Perform the velocity autocorrelation

    % Choose the velocity discretization to pull out the velocities for
        % Choose the interval length for further calculations
            VelAutocorrResults.intervalNum = 1;
        % Choose the discretization (in number of timepoints)
            VelAutocorrResults.intervalLength = VelAutocorrResults.intervalLengths2Try(...
                VelAutocorrResults.intervalNum);
        % Pull out the time vector associated with this velocity            
            VelAutocorrResults.time_for_velocities_ms = msPerTimeStep*(((VelAutocorrResults.intervalLength+1):...
                VelAutocorrResults.intervalLength:numTimepoints)-(VelAutocorrResults.intervalLength/2)-1);
        % Pull out just this discretization
            VelAutocorrResults.velocities_nm_per_ms_1Disc = ...
                VelAutocorrResults.velocities_nm_per_frame{1,...
                VelAutocorrResults.intervalNum}.*(1/msPerTimeStep);     
            
    % Calculate the velocity autocorrelation (with non-overlapping intervals)
        % Choose the dimension across which to perform the autocorrelation
            VelAutocorrResults.dim2Autocorr = 3;
        % Perform the autocorrelation
            [VelAutocorrResults.v_autocorr_nm2_per_ms2] = ...
                calculateAutocorrelationNonOverlapping(...
                VelAutocorrResults.velocities_nm_per_ms_1Disc,...
                VelAutocorrResults.dim2Autocorr);
        
    % Create the time offset vector
        % Determine the number of time offsets
            VelAutocorrResults.numTimeOffsets = size(VelAutocorrResults.v_autocorr_nm2_per_ms2,3)-1;
        % Create the time offset vector
            VelAutocorrResults.timelag_for_v_autocorr_ms = msPerTimeStep*...
                VelAutocorrResults.intervalLength*(0:VelAutocorrResults.numTimeOffsets);
%             % Account for the fact that the 0th offset is made on velocity 
%             % measurements that are not truly instantaneous (which would 
%             % give a 0th time offset = 0, but instead calculated over an 
%             % interval of defined time            
%                 timelag_for_velocity_autocorr_ms(1) = (msPerTimeStep*intervalLength/2);

% Calculate the ensemble(track)-averaged, time-averaged MSD (along with
% associated summary statistics)
    % Choose the dimension to average over
        dim2AverageOver = 1;
    % Perform ensemble-averaging by cell
        % Calculate the ensemble-averaged MSD and summary stats separately for 
        % each dimension    
            [VelAutocorrResults.v_autocorr_nm2_per_ms2_TimeAvgByTrack_EnsAvgByCell_dims] = ...
                calculateSummaryStatsByCondition(...
                VelAutocorrResults.v_autocorr_nm2_per_ms2,...
                dim2AverageOver,cellIDByTrack);
        % Calculate the total ensemble-averaged MSD and summary stats
            [VelAutocorrResults.v_autocorr_nm2_per_ms2_TimeAvgByTrack_EnsAvgByCell] = ...
                calculateSummaryStatsByCondition(squeeze(sum(...
                VelAutocorrResults.v_autocorr_nm2_per_ms2,2)),...
                dim2AverageOver,cellIDByTrack);
    % Perform ensemble-averaging by condition
        % Calculate the ensemble-averaged MSD and summary stats separately for 
        % each dimension    
            [VelAutocorrResults.v_autocorr_nm2_per_ms2_TimeAvgByTrack_EnsAvgByCondition_dims] = ...
                calculateSummaryStatsByCondition(...
                VelAutocorrResults.v_autocorr_nm2_per_ms2,...
                dim2AverageOver,conditionIDByTrack);
        % Calculate the total ensemble-averaged MSD and summary stats
            [VelAutocorrResults.v_autocorr_nm2_per_ms2_TimeAvgByTrack_EnsAvgByCondition] = ...
                calculateSummaryStatsByCondition(squeeze(sum(...
                VelAutocorrResults.v_autocorr_nm2_per_ms2,2)),...
                dim2AverageOver,conditionIDByTrack);
    
                    
% Add the results to the structure  
    dataStructure.VelAutocorrResults = VelAutocorrResults;
    
    