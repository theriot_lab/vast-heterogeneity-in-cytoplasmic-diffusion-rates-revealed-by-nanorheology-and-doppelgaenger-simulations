function [MSD, SDSD, SESD, numSDMeasurements] = calculateEnsembleAveragedTotalMSD(data,dim2CalcMSD,dim2SumMSD,dim2AvgMSD)

% This function intakes a 3D matrix, and calculates the average mean-squared 
% displacement over all possible intervals along one of the three dimensions 
% This function was written and developed by Rikki M. Garner and was last
% updated on 2022/8/30.

% Inputs: 
    % data = A 3D matrix
    % dim2CalcMSD = An integer storing the dimension over which to
        % calculate the MSD
    % dim2SumMSD = An integer storing the dimension over which to
        % sum the MSD (eg., physical dimensions)
    % dim2AvgMSD = An integer storing the dimension over which to
        % average the resultant MSD
% Outputs: 
    % MSD = A 3D matrix, in which the intervals for the dimension over 
        % which the MSD was calculated is now located along the last 
        % dimension, and the other two dimensions have been shifted to the
        % left as necessary
    % numSDMeasurements = a 3D matrix the size of MSD holding the number of
    % measurements used to calculate the MSD
       

% Reshuffle the matrix dimensions to put the dimension we want to find
% the MSD over as the last dimension 
    % Calculate the number of dimensions
        numDimensions = length(size(data));
    % Make a vector of the data dimensions
        dimensions_shuffled = 1:numDimensions;
    % Remove the dimension we're averaging over
        dimensions_shuffled(dimensions_shuffled==dim2CalcMSD)=[];
    % Re-add that dimension to the end
        dimensions_shuffled = [dimensions_shuffled dim2CalcMSD];
    % Reshuffle the matrix dimensions to put the dim2CalcMSD last
        data_reshuffled = permute(data,dimensions_shuffled);
    % Calculate the number of measurements
        numMeasurements = size(data_reshuffled,numDimensions);

% Calculate the SD by dims
    SD = sum((data_reshuffled(:,:,1:numMeasurements) - ...
         data_reshuffled(:,:,1)).^2,dim2SumMSD);
    MSD = squeeze(nanmean(SD,dim2AvgMSD));
    SDSD = squeeze(nanstd(SD,dim2AvgMSD));
    numSDMeasurements = squeeze(sum(~isnan(SD),dim2AvgMSD));
    SESD = SDSD./sqrt(numSDMeasurements);


    
    
    
    
    
    
    