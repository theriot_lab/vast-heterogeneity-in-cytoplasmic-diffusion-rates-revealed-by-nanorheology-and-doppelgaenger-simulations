%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [results] = calculateSummaryStats(data,dim2AverageOver)

% This function intakes a matrix of arbitrary dimensions, then calculates 
% summary atatistics about the mean and median behaviors over a chosen
% dimension

% Inputs: 
    % data = A matrix of arbitrary number of dimensions
    % dim2AverageOver = An integer storing the dimension over which to
        % average
% Outputs: 
    % results = structure containing the mean, standard deviaiton of the mean, 
        % standard error of the mean,
        % median, standard deviation of the median, standard error of the
        % median

% Reshuffle the matrix dimensions to put the dimension we want to average over last 
    % Calculate the number of dimensions
        numDimensions = length(size(data));
    % Make a vector of the data dimensions
        dimensions_shuffled = 1:numDimensions;
    % Remove the dimension we're averaging over
        dimensions_shuffled(dimensions_shuffled==dim2AverageOver)=[];
    % Re-add that sdimension to the end
        dimensions_shuffled = [dimensions_shuffled dim2AverageOver];
    % Reshuffle the matrix dimensions to put the tracks last
        data_reshuffled = permute(data,dimensions_shuffled);

% Calculate the summary statistics
    % Mean, std, and se of the mean
        % Calculate the mean over the last dimension           
            data_mean = nanmean(data_reshuffled,numDimensions);
        % Calculate the squared difference from the mean over the last dimension     
            data_squared_diff_from_mean = ...
                bsxfun(@minus,data_reshuffled,data_mean).^2;
        % Calculate the standard deviation across the last dimension    
            data_stdmean = ...
                sqrt(nansum(data_squared_diff_from_mean,numDimensions)./...
                (sum(~isnan(data_squared_diff_from_mean),numDimensions)-1));
        % Calculate the standard error of the mean    
            data_semean = data_stdmean./...
                sqrt(sum(~isnan(data_squared_diff_from_mean),numDimensions));
	% Median, std, and se of the median
        % Calculate the median over the last dimension            
            data_median = nanmedian(data_reshuffled,numDimensions);
        % Calculate the squared difference from the median for each track    
            data_squared_diff_from_median = ...
                bsxfun(@minus,data_reshuffled,data_median).^2;
        % Calculate the standard deviation across the last dimension    
            data_stdmedian = ...
                sqrt(nansum(data_squared_diff_from_median,numDimensions)./...
                (sum(~isnan(data_squared_diff_from_median),numDimensions)-1));
        % Calculate the standard error of the median    
            data_semedian = data_stdmedian./...
                sqrt(sum(~isnan(data_squared_diff_from_median),numDimensions));            
            
% Load the data into a struture
    results.mean = data_mean;
    results.stdmean = data_stdmean;
    results.semean = data_semean;
    results.median = data_median;
    results.stdmedian = data_stdmedian;
    results.semedian = data_semedian;

end