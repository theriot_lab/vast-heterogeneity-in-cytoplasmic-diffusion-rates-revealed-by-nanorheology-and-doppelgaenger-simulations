%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [results] = calculateSummaryStatsByCondition(data,dim2AverageOver,conditionID)

% This function intakes a matrix of arbitrary dimensions, then calculates 
% summary atatistics about the mean and median behaviors over a chosen
% dimension, indexed by condition

% Inputs: 
    % data = A matrix of arbitrary number of dimensions
    % dim2AverageOver = An integer storing the dimension over which to
        % average
    % conditionID = a vector the size of the matrix along dim2AverageOver 
        % containing the condition identifier for each element 
% Outputs: 
    % results = structure containing the mean, standard deviaiton of the mean, ...
        % standard error of the mean,
        % median, standard deviation of the median, standard error of the
        % median

% Check the data inputs are of appropriate size
if length(conditionID)~=size(data,dim2AverageOver)
   'Number of condition IDs does not match matrix dimension to average over'
    return 
end
        
% Reshuffle the matrix dimensions to put the dimension we want to average over last 
    % Calculate the number of dimensions
        numDimensions = length(size(data));
    % Make a vector of the data dimensions
        dimensions_shuffled = 1:numDimensions;
    % Remove the dimension we're averaging over
        dimensions_shuffled(dimensions_shuffled==dim2AverageOver)=[];
    % Re-add that sdimension to the end
        dimensions_shuffled = [dimensions_shuffled dim2AverageOver];
    % Reshuffle the matrix dimensions to put the tracks last
        data_reshuffled = permute(data,dimensions_shuffled);

% Pre-allocate space to store the results
    % Determine the size of the output data
        % Calculate the size of the new matrix
            size_data_reshuffled = size(data_reshuffled);
        % Calculate the number of conditions
            [uniqueConditionIDs, ~, ~] = unique(conditionID);
        % Count the number of unique conditions
            numConditions = length(uniqueConditionIDs);
        % Determine the size of the output
            size_output = [size_data_reshuffled(1:end-1) max(uniqueConditionIDs)];
        % Deretmine the number of elements in a slice
            numElementsPerSlice = prod(size_data_reshuffled(1:(end-1)));
    % Create the empty matrices
        data_mean = nan(size_output);
        data_stdmean = nan(size_output);
        data_semean = nan(size_output);
        data_median = nan(size_output);
        data_stdmedian = nan(size_output);
        data_semedian = nan(size_output);
        
for conditionNum = 1:numConditions
    
    % Determine the indices for this slice in the input and output matrices            
        % Determine the linear indices for the input matrix
            % Find the subcript indices along the last dimension corresponding to
            % this condition
                subIndCondition = find(conditionID==uniqueConditionIDs(conditionNum));
            % Count the number of slices for this condition
                numDataPointsInCondition = length(subIndCondition);
            % Preallocate space to store the linear indices
                linIdxInput = nan(numElementsPerSlice*numDataPointsInCondition,1);
        % Create the linear indices for the input matrix
            for sliceNum = 1:numDataPointsInCondition
                % Pull out the index of this slice
                    subIdxConditionSlice = subIndCondition(sliceNum);
                % Create the linear indices
                    linIdxInput(((sliceNum-1)*numElementsPerSlice)+...
                        (1:numElementsPerSlice)) = ...
                        ((subIdxConditionSlice-1)*numElementsPerSlice)+...
                        (1:numElementsPerSlice);
            end 
        % Create the data subset to analyze
            dataSubset = nan([numElementsPerSlice numDataPointsInCondition]);
            dataSubset(:) = data_reshuffled(linIdxInput);   
    % Determine the linear indices for this condition's slice in the output matrix 
        % Create the linear indices
            linIdxOutput = ((uniqueConditionIDs(conditionNum)-1)*numElementsPerSlice)+...
                (1:numElementsPerSlice);         
    
    % Calculate the total ensemble-averaged MSD and summary stats
        [resultsTemp] = calculateSummaryStats(dataSubset,numDimensions); 
        
    % Record the results    
        data_mean(linIdxOutput) = resultsTemp.mean;
        data_stdmean(linIdxOutput) = resultsTemp.stdmean;
        data_semean(linIdxOutput) = resultsTemp.semean;
        data_median(linIdxOutput) = resultsTemp.median;
        data_stdmedian(linIdxOutput) = resultsTemp.stdmedian;
        data_semedian(linIdxOutput) = resultsTemp.semedian;
end


% Reshuffle the matrix dimensions to put the dimension we averaged over 
% back to its original position 
    % Calculate the number of dimensions
        numDimensions = length(size(data_mean));
    if numDimensions~=dim2AverageOver
        % Make a vector of the data dimensions
            dimensions_shuffled = 1:numDimensions;
        % Push all subsequent dimensions (after the one averaged over) to the end
            dimensions_shuffled((dim2AverageOver+1):end)=...
                dimensions_shuffled(dim2AverageOver:(end-1));
        % Re-add that dimension to its original place
            dimensions_shuffled(dim2AverageOver) = numDimensions;
        % Reshuffle the matrix dimensions to put the tracks last
            data_mean = permute(data_mean,dimensions_shuffled);
            data_stdmean = permute(data_stdmean,dimensions_shuffled);
            data_semean = permute(data_semean,dimensions_shuffled);
            data_median = permute(data_median,dimensions_shuffled);
            data_stdmedian = permute(data_stdmedian,dimensions_shuffled);
            data_semedian = permute(data_semedian,dimensions_shuffled);
    end
    
    % Load the data into a struture
        results.mean = data_mean;
        results.stdmean = data_stdmean;
        results.semean = data_semean;
        results.median = data_median;
        results.stdmedian = data_stdmedian;
        results.semedian = data_semedian;
    
end