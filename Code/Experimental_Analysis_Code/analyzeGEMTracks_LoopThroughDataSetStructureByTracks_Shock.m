%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    close all;
    clear all;    

% Choose the file contining the particle track data
    % All datasets combined
        % Choose the filepath
             filePath = ['Garner_Molines_et_al\Experimental_Data\Raw_Data\'...
                'All_Conditions_sorbitol_20210726.mat'];
        % Name the dataset
            datasetName = 'Cytoplasmic 40 nm GEMs: Shock Conditions';    
        % Choose which experimental dataset to plot
            % Choose the condition number for each folder
                condition2plot = 1:11;
            % Choose which color to plot the conditions    
                CMByCondition = jet(length(condition2plot))*0.75;
            % Load the dataset info into a structure
                experimentalDatasetInfo = v2struct();
        % Load the dataset
            load(filePath)
        % Copy the dataset
            DataOrganizedByCell = Structure_20210726;

% Name the dimensions
    dimensionNames = {'X','Y'};
        
% Choose the pixel and timestep conversion ratios
    nmPerPixel = 106; 
    msPerTimeStep = 10.7;   

% Choose which plots to make
    doPlotMSDByCondition = false;
    doPlotMSDByCell = false;
    doPlotMSDByTrack = false;
    doPlotVelAutocorrDByCondition = false;
  
% Choose whether to save the images
    doSaveImages = true;
    
% Determine where to save the data
    % Determine the file prefix for the analyzed results    
        saveFilePathPrefix = filePath(1:(find(filePath=='.',1,'last')-1));
    % Change the folder to the analyzed data folder
        saveFilePathPrefix = strrep(saveFilePathPrefix,'Raw','Analyzed');
    % Determine the file name for the structure containing the summary
    % statistics
        saveFilePathSummaryStats = [saveFilePathPrefix '_Analyzed.mat'];
       
% Create IDs for the unique conditions
    % Use condition and localization
        % Concatenate the condition and localization
            ConditionWithDayAndLocalization = ...
                strcat([convertStringsToChars([DataOrganizedByCell.Condition]')],{', '}, ...
                [convertStringsToChars([DataOrganizedByCell.Localization]')]);
        % Add this new condition to the structure
            [DataOrganizedByCell.ConditionWithDayAndLocalization] = ...
                ConditionWithDayAndLocalization{:};
    % Assign an imaging session ID
        % Concatenate the condition and date info
            imagingSession = ...
                strcat([convertStringsToChars([DataOrganizedByCell.Condition]')],{', '}, ...
                [convertStringsToChars([DataOrganizedByCell.Date]')],{', '}, ...
                [convertStringsToChars([DataOrganizedByCell.Localization]')],{', '}, ...
                [convertStringsToChars(num2str([DataOrganizedByCell.Repetition]'))]);
        % Add this new condition to the structure
            [DataOrganizedByCell.imagingSession] = imagingSession{:};                
    % Use these conditions for subsequent analysis    
        conditionNames = imagingSession;      
                
% Assign IDs
    dayNames = convertStringsToChars([DataOrganizedByCell.Date]');
    treatmentNames = convertStringsToChars([DataOrganizedByCell.Condition]');
    localizationNames = convertStringsToChars([DataOrganizedByCell.Localization]');
    imagingSessionNames = imagingSession;

% Package the data into a structure                
    Experimental_Results = v2struct();
    clearvars -except Experimental_Results
    
% Reshape the data by track
    Experimental_Results = reshapeCellDataByTracks_NoCellInfo(Experimental_Results);        
        
% Perform the MSD analysis

    % Perform the time-averaged MSD vs time
        [Experimental_Results] = ...
            performMSD_AverageByTrackCellAndCondition(Experimental_Results);
    
    % Fit the MSD data
        [Experimental_Results] = ...
            performMSDFit_AverageByTrackCellAndCondition(Experimental_Results);

% Perform analysis of variance
    [Experimental_Results] = run_ANOVA_TrackCellConditionSessionDay(Experimental_Results);
    close all hidden; % Close ANOVA tables
    
% Save the file
    save(Experimental_Results.saveFilePathSummaryStats,'-struct',...
        'Experimental_Results')
               