%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    close all;
    clear all;    

% Choose the file contining the particle track data
    % All datasets combined
        % Choose the filepath
             filePath = ['Garner_Molines_et_al\Experimental_Data\Raw_Data\'...
                'Big_Struct_20210601_Corrected.mat'];    
        % Determine where to save the data
            % Determine the file prefix for the analyzed results    
                saveFilePathPrefix = filePath(1:(find(filePath=='.',1,'last')-1));
            % Change the folder to the analyzed data folder
                saveFilePathPrefix = strrep(saveFilePathPrefix,'Raw','Analyzed');
            % Determine the file name for the structure containing the summary
            % statistics
                saveFilePathSummaryStats = [saveFilePathPrefix '_Temp_Analyzed.mat'];
        % Name the dataset
            datasetName = 'Cytoplasmic 40 nm GEMs: Temperature'; 
        % Choose the conditions to keep
            conditionsToKeep = {'Imaged at 20C','Imaged at 30C'};
        % Choose which experimental dataset to plot
            % Choose the condition number for each folder
                condition2plot = 1:11;
            % Choose which color to plot the conditions    
                CMByCondition = jet(length(condition2plot))*0.75;
            % Load the dataset info into a structure
                experimentalDatasetInfo = v2struct();
        % Load the dataset
            load(filePath)
        % Copy the dataset
            %DataOrganizedByCell = Datasets;
            DataOrganizedByCell = Global;
        % Remove the uneeded data
            % Loop through and find cells not in any category
                inGroup = false(1,length(DataOrganizedByCell));
                for numGroups = 1:length(conditionsToKeep)
                    inGroup = inGroup|([DataOrganizedByCell.Condition]==...
                        conditionsToKeep{numGroups});
                end
            % Remove the unwanted points
                DataOrganizedByCell(~inGroup) = [];

% Name the dimensions
    dimensionNames = {'X','Y'};
        
% Choose the pixel and timestep conversion ratios
    nmPerPixel = 106; 
    msPerTimeStep = 10.7;   

% Choose which plots to make
    doPlotMSDByCondition = false;
    doPlotMSDByCell = false;
    doPlotMSDByTrack = false;
    doPlotVelAutocorrDByCondition = false;
  
% Choose whether to save the images
    doSaveImages = true;

% Create boolean for new pole left/right
    % Pull out the unqiue pole position identifiers
        [uniqueNewPolePos, uniqueNewPolePosByCellFirstInstance, newPolePosIDByCell] = ...
            unique([DataOrganizedByCell.NewPolePos]);
   % Choose randomly for unknown new pole position
        % Find the nan cells
            nanUniqueNewPolePos = find(uniqueNewPolePos=="N/A");
            nonNanUniqueNewPolePos = find(uniqueNewPolePos~="N/A");
        % Replace with random boolean
            newPolePosIDByCell(newPolePosIDByCell==nanUniqueNewPolePos) = ...
                nonNanUniqueNewPolePos(randi([1 2],1,...
                sum((newPolePosIDByCell==nanUniqueNewPolePos))));
    % Create boolean for new pole left/right
        [uniqueNewPolePos, uniqueNewPolePosByCellFirstInstance, newPolePosIDByCell] = ...
            unique([DataOrganizedByCell.NewPolePos]);
    % Find the poles to the left and right
        newPolePosLeft = (newPolePosIDByCell==find(uniqueNewPolePos=="Left"));
        newPolePosRight = (newPolePosIDByCell==find(uniqueNewPolePos=="Right"));
    % Change the boolean to -1,1
        newPolePosIDByCell(newPolePosRight) = 1;
        newPolePosIDByCell(newPolePosLeft) = -1;
    % Update the structure
        newPolePosIDByCell = num2cell(newPolePosIDByCell);
        [DataOrganizedByCell.newPolePosIDByCell] = newPolePosIDByCell{:};        
        
% Create IDs for the unique conditions
    % Keep the current condition names
        conditionNames = convertStringsToChars([DataOrganizedByCell.Condition]');
    % Assign an imaging session ID
        % Concatenate the condition and date info
            imagingSession = ...
                strcat([convertStringsToChars([DataOrganizedByCell.Condition]')],{', '}, ...
                [convertStringsToChars([DataOrganizedByCell.Date]')],{', '}, ...
                [convertStringsToChars([DataOrganizedByCell.Localization]')],{', '});
        % Add this new condition to the structure
            [DataOrganizedByCell.imagingSession] = imagingSession{:};                
                
% Assign IDs
    dayNames = convertStringsToChars([DataOrganizedByCell.Date]');
    treatmentNames = convertStringsToChars([DataOrganizedByCell.Condition]');
    localizationNames = convertStringsToChars([DataOrganizedByCell.Localization]');
    imagingSessionNames = imagingSession;

% Package the data into a structure                
    Experimental_Results = v2struct();
    clearvars -except Experimental_Results
    
% Reshape the data by track
    Experimental_Results = reshapeCellDataByTracks(Experimental_Results);        
        
% Perform the MSD analysis

    % Perform the time-averaged MSD vs time
        [Experimental_Results] = ...
            performMSD_AverageByTrackCellAndCondition(Experimental_Results);
    
    % Fit the MSD data
        [Experimental_Results] = ...
            performMSDFit_AverageByTrackCellAndCondition(Experimental_Results);
        
% Perform analysis of variance
    [Experimental_Results] = run_ANOVA_TrackCellConditionSessionDay(Experimental_Results);
    close all hidden; % Close ANOVA tables
    
% Save the file
    save(Experimental_Results.saveFilePathSummaryStats,'-struct',...
        'Experimental_Results')
               
