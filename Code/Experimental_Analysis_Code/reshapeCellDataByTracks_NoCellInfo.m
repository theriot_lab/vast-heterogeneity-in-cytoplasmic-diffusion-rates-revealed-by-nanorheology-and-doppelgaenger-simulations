%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [dataStructure] = reshapeCellDataByTracks_NoCellInfo(dataStructure)

    
% Unpack the structure              
    v2struct(dataStructure);
    
% Count the number of cells
    numCells = size(dataStructure.DataOrganizedByCell,2);
 
% Assign the relevant IDs 
    % Assign condition IDs
        % Determine the unique conditions and condition for each cell
            [uniqueConditions, uniqueConditionsByCellFirstInstance, conditionIDByCell] = ...
                unique(conditionNames) ;
        % Count the number of conditions        
            numConditions = length(uniqueConditions);  
    % Assign day IDs
        % Determine the unique days and day for each cell
            [uniqueDays, uniqueDaysByCellFirstInstance, dayIDByCell] = ...
                unique(dayNames) ;
        % Count the number of days        
            numDays = length(uniqueDays); 
    % Assign treatment IDs
        % Determine the unique treatments and treatment for each cell
            [uniqueTreatments, uniqueTreatmentsByCellFirstInstance, treatmentIDByCell] = ...
                unique(treatmentNames) ;
        % Count the number of tratment        
            numTreatments = length(uniqueTreatments);   
    % Assign localization IDs
        % Determine the unique localization and localization for each cell
            [uniqueLocalizations, uniqueLocalizationsByCellFirstInstance, localizationIDByCell] = ...
                unique(localizationNames) ;
        % Count the number of localizations       
            numLocalizations = length(uniqueLocalizations); 
    % Assign imaging session IDs
        % Determine the unique imaging session and imaging session for each cell
            [uniqueImagingSessions, uniqueImagingSessionsByCellFirstInstance, imagingSessionIDByCell] = ...
                unique(imagingSessionNames) ;
        % Count the number of imaging sessions        
            numImagingSessions = length(uniqueImagingSessions);            
        
% Concatenate all of the track data into one matrix to analyze
    % First, calculate the total number of trajectories and the maximum 
    % number of timepoints recorded
        numTimepoints = 0;
        numTracks = 0;
        for numCell = 1:numCells
            numTimepoints = max([numTimepoints ...
                size(dataStructure.DataOrganizedByCell(numCell).TrajPosX,1)]);
            numTracks = numTracks + ...
                size(dataStructure.DataOrganizedByCell(numCell).TrajPosX,2);
        end    
    % Pre-allocate space to store the trajectories and associated
    % identifiers
        trackPositions_nm = nan(numTracks,2,numTimepoints);
        cellIDByTrack = nan(numTracks,1);
        imagingSessionIDByTrack = nan(numTracks,1);
        conditionIDByTrack = nan(numTracks,1);
        treatmentIDByTrack = nan(numTracks,1);
        localizationIDByTrack = nan(numTracks,1);
        dayIDByTrack = nan(numTracks,1);
    % Load the data into the matrix
        totalNumTrajectoriesSoFar = 0;
        for numCell = 1:numCells
            % Count the number of tracjectories and timepoints for this cell
                [numTPThisCell, numTrajThisCell] = ...
                    size(dataStructure.DataOrganizedByCell(numCell).TrajPosX); 
            % Pull out and normalize the track trajectories
                % Pull out the trajectories
                    trackPositionsX = dataStructure.DataOrganizedByCell(numCell).TrajPosX'*nmPerPixel;
                    trackPositionsY = dataStructure.DataOrganizedByCell(numCell).TrajPosY'*nmPerPixel;
                % Turn the empty entries (0) into nans, so they aren't confused
                % with positions actually equal to zero
                    trackPositionsX(trackPositionsX==0) = nan; 
                    trackPositionsY(trackPositionsY==0) = nan; 
%                 % Recenter the tracks around the cell centroid
%                     trackPositionsX = trackPositionsX - ...
%                         (dataStructure.DataOrganizedByCell(numCell).Centroid_X*10^3);
%                     trackPositionsY = trackPositionsY - ...
%                         (dataStructure.DataOrganizedByCell(numCell).Centroid_Y*10^3);
%                 % Place the new pole to the right
%                     trackPositionsX = trackPositionsX./newPolePosIDByCell{numCell};
            % Add and reformat the position data for all dimensions into a 
            % single matrix of size (numTrack x numDimensions x numTimePoints)
                trackPositions_nm(totalNumTrajectoriesSoFar+(1:numTrajThisCell),1,1:numTPThisCell) = ...
                    trackPositionsX;
                trackPositions_nm(totalNumTrajectoriesSoFar+(1:numTrajThisCell),2,1:numTPThisCell) = ...
                    trackPositionsY;
            % Update the condition and cell ID vectors for these tracks 
                cellIDByTrack(totalNumTrajectoriesSoFar+(1:numTrajThisCell)) = ...
                    numCell*ones(numTrajThisCell,1);
                dayIDByTrack(totalNumTrajectoriesSoFar+(1:numTrajThisCell)) = ...
                    dayIDByCell(numCell)*ones(numTrajThisCell,1);
                conditionIDByTrack(totalNumTrajectoriesSoFar+(1:numTrajThisCell)) = ...
                    conditionIDByCell(numCell)*ones(numTrajThisCell,1);
                treatmentIDByTrack(totalNumTrajectoriesSoFar+(1:numTrajThisCell)) = ...
                    treatmentIDByCell(numCell)*ones(numTrajThisCell,1);
                localizationIDByTrack(totalNumTrajectoriesSoFar+(1:numTrajThisCell)) = ...
                    localizationIDByCell(numCell)*ones(numTrajThisCell,1);
                imagingSessionIDByTrack(totalNumTrajectoriesSoFar+(1:numTrajThisCell)) = ...
                    imagingSessionIDByCell(numCell)*ones(numTrajThisCell,1); 
            % Update the number of trajectories input so far
                totalNumTrajectoriesSoFar = totalNumTrajectoriesSoFar + numTrajThisCell;
        end
        
% Calculate the size of the data
    % Calculate the number of tracks
        numTracks = size(trackPositions_nm,1);
    % Calculate the number of dimensions
        numDimensions = size(trackPositions_nm,2);
    % Calculate the number of timepoints
        numTimepoints = size(trackPositions_nm,3);
% Create the associated vector of time points in ms
    timePoints_ms = (0:(numTimepoints-1)).*msPerTimeStep;  
    
% Add the results to the structure  
    dataStructure.trackPositions_nm = trackPositions_nm;
    dataStructure.numTracks = numTracks;
    dataStructure.numDimensions = numDimensions;
    dataStructure.numTimepoints = numTimepoints;
    dataStructure.timePoints_ms = timePoints_ms;
    dataStructure.numCells = numCells;
    dataStructure.cellIDByTrack = cellIDByTrack;
    dataStructure.numConditions = numConditions;
    dataStructure.uniqueConditions = uniqueConditions;
    dataStructure.conditionIDByCell = conditionIDByCell;
    dataStructure.conditionIDByTrack = conditionIDByTrack;
    dataStructure.uniqueConditions = uniqueConditions;
    dataStructure.imagingSessionIDByTrack = imagingSessionIDByTrack;
    dataStructure.imagingSessionIDByCell = imagingSessionIDByCell;
    dataStructure.uniqueImagingSessions = uniqueImagingSessions;
    dataStructure.treatmentIDByTrack = treatmentIDByTrack;
    dataStructure.treatmentIDByCell = treatmentIDByCell;
    dataStructure.uniqueTreatments = uniqueTreatments;
    dataStructure.localizationIDByTrack = localizationIDByTrack;
    dataStructure.localizationIDByCell = localizationIDByCell;
    dataStructure.uniqueLocalizations = uniqueLocalizations;
    dataStructure.dayIDByTrack = dayIDByTrack;
    dataStructure.dayIDByCell = dayIDByCell;
    dataStructure.uniqueDays = uniqueDays;  
    
end