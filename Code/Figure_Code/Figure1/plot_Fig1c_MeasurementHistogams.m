%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    close all;
    clear all;

% Choose the file contining the particle track data
    filePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
        '\All_Controls_Combined_Analyzed.mat'];

% Load the dataset
    load(filePath)   

%% Plot a histogram of the number of tracks per cell

    % Calculate the number of tracks per cell
        [numTracksPerCell, uniqueCellIDs] = ...
            hist(cellIDByTrack,unique(cellIDByTrack));

    % Plot the histogram for the number of tracks in each cell
        % Pull up the figure
            figureNumHistTracksPerCell = 2;
            figure(figureNumHistTracksPerCell)
        % Plot the histogram
            histogram(numTracksPerCell,10,'Normalization','pdf')
        % Clean up the plot
            xlim([0 55])
            xlabel('Number of tracks per cell')
            ylabel('Probability')
            set(gca,'FontName','Helvetica','FontSize',6); 

    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure1\Fig1c_Hist_TracksPerCell'];
        % Choose the figure size and position
            figHandle = figure(figureNumHistTracksPerCell);
            figHandle.Position =  [100   613   162   137];
        % Save the file
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig']) 
        
%% Plot a histogram of the step sizes

    % Calculate the step sizes for each track and timepoint  
        % Pull out the trajectories and calculate the step size in each 
        % dimension
            % Pull out the track positions
                x = squeeze(trackPositions_nm(:,1,:))'./10^3;
                y = squeeze(trackPositions_nm(:,2,:))'./10^3;
            % Calculate the step sizes
                dx = diff(x,1,1);
                dy = diff(y,1,1);
            % Concatenate step sizes for all tracks into a single vector
                dx = dx(:);
                dy = dy(:);
            % Pull out the track positions
                x = x(1:(end-1),:);
                y = y(1:(end-1),:);
        % Add these step sizes        
            stepSizesInUMPerSec = ...
                sqrt(dx.^2 + dy.^2)/(msPerTimeStep./10^3);

    % Plot the histogram
        % Pull up the figure
            figureNumHistStepSize = 1;
            figure(figureNumHistStepSize)
        % Plot the histogram
            h = histogram(stepSizesInUMPerSec*msPerTimeStep,100,...
                'Normalization','pdf','FaceColor',[1 1 1]*0.7);
        % Clean up the plot
            xlim([0 400])
            xlabel('Step size (nm)')
            ylabel('Probability')
            set(gca,'FontName','Helvetica','FontSize',6); 

    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure1\Fig1c_Hist_StepSize'];
        % Choose the figure size and position
            figHandle = figure(figureNumHistStepSize);
            figHandle.Position =  [100   600   200   150];
        % Save the file
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])      
    
%% Plot a histogram of the track lengths

    % Calculate the track lengths
        trackLengthInTimeSteps = ...
            sum(~isnan(squeeze(sum(trackPositions_nm,2))),2);

    % Plot the histogram
        % Pull up the figure
            figureNumHistTrackLength = 3;
            figure(figureNumHistTrackLength)
        % Plot the histogram
            h = histogram((trackLengthInTimeSteps-1)*msPerTimeStep,...
                (10:6:1000)*msPerTimeStep,'Normalization','pdf',...
                'FaceColor',[1 1 1]*0.7);
        % Clean up the plot
            xlim([0 1100])
            hold off;
            xlabel('Track length (ms)')
            ylabel('Probability')
            set(gca,'FontName','Helvetica','FontSize',6); 

    % Save the figure as a png
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure1\Fig1c_Hist_TrackLength'];
        % Choose the figure size and position
            figHandle = figure(figureNumHistTrackLength);
            figHandle.Position =  [100   613   162   137];
        % Save the file
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])     
