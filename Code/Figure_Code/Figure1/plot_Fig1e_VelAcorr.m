%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    % Close all figures
        close all;
    % Clear all variables
        clear all; 
    
% Choose the experimental dataset

    % Write the path to the experimental dataset
        experimentalDatasetFilePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
        '\All_Controls_Combined_Analyzed.mat'];
        Experimental_Results = load(experimentalDatasetFilePath);

    % Reset the conditions to be all one condition
        Experimental_Results.conditionIDByCell = ...
            ones(size(Experimental_Results.conditionIDByCell));
        Experimental_Results.conditionIDByTrack = ...
            ones(size(Experimental_Results.conditionIDByTrack));
        Experimental_Results.numConditions = 1;
           
    % Perform the velocity autocorrelation        
        [Experimental_Results] = ...
            performVelocityAutocorrelation_AverageByTrackCellAndCondition(Experimental_Results);


% Plot the results    
    % Update the condition names
        Experimental_Results.uniqueConditions{1} = '40 nm GEMs';
    % Choose the color of the plot
        CMByCondition(1,:) =[0.1 0.1 0.9]; 
    % Choose the figure number
        figureNumVelAutocorr = 10;
    % Pull out the condition to plot
        conditionNum = 1;
    % Isolate the velocity autocorrelation reuslts
        VelAutocorrResults = Experimental_Results.VelAutocorrResults;
    
% Plot the correlation across all tracks in each condition
figure(figureNumVelAutocorr)
% Plot the mean and se of the mean
    % Plot the mean
        plot(VelAutocorrResults.timelag_for_v_autocorr_ms,...
            VelAutocorrResults.v_autocorr_nm2_per_ms2_TimeAvgByTrack_EnsAvgByCondition.mean(conditionNum,:)',...
            '','Color',CMByCondition(conditionNum,:))
        hold on;
    % Plot the SE of the mean as error bars
        errorbar(VelAutocorrResults.timelag_for_v_autocorr_ms,...
            VelAutocorrResults.v_autocorr_nm2_per_ms2_TimeAvgByTrack_EnsAvgByCondition.mean(conditionNum,:)',...
            VelAutocorrResults.v_autocorr_nm2_per_ms2_TimeAvgByTrack_EnsAvgByCondition.semean(conditionNum,:)',...
            'r','LineStyle', 'none','Color',CMByCondition(conditionNum,:)) 
       % Update the legend text
        legendText = {[sprintf('Mean across tracks \n (time-avg, track-avg)')]};
        legendText = [legendText {['SE']}];


% Clean up the mean plot
    figure(figureNumVelAutocorr)
    % Draw the axes lines
        xl = xlim();
        plot([xl(1) xl(2)],[0 0],'k')
    % Updat the axes limits
        xlim([0 300])
        maxVAcorr = max(VelAutocorrResults.v_autocorr_nm2_per_ms2_TimeAvgByTrack_EnsAvgByCondition.mean(:,1));
        ylim([-0.1 1.1]*maxVAcorr)
        hold off;
    % Label the plot
        legend(legendText,'Location','Northeast','FontSize',5) 
        xlabel('Time offset (ms)')
        ylabel(sprintf('Velocity-autocorrelation \n (nm/ms)^2'))
        title({'Velocity-autocorrelation'})
        set(gca,'FontName','Helvetica','FontSize',6);

            
        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                    'Figure_Panels\Figure1\Fig1e_VelAutocorr'];
            % Choose the figure size and position
                figHandle = figure(figureNumVelAutocorr);
                figHandle.Position =  [100   615  210   160];
            % Save the figure in PDF format         
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])    
  
