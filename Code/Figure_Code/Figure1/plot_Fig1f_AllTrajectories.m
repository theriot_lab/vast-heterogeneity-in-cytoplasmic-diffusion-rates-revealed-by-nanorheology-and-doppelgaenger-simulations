%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    close all;
    clear all;

% Choose the file path to the data containing the combined Simulational and simulation
% results
    filePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
        '\All_Controls_Combined_Analyzed.mat'];
    load(filePath);

% Choose the figure names
    figureNumPlotAllTracks_RealPositions = 3;

% Choose the maximum number of trajectories to plot
    maxNumTracks2Plot = 400;

% Calculate the mean and std of the step size across all tracks     
    % Pull out the trajectories and calculate the step size in each dimension
        % Pull out the track positions in nanometers
            x = squeeze(trackPositions_nm(:,1,:))';
            y = squeeze(trackPositions_nm(:,2,:))';
        % Calculate the step sizes in nanometers
            dx = diff(x,1,1);
            dy = diff(y,1,1);
        % Concatenate step sizes for all tracks into a single vector
            dx = dx(:);
            dy = dy(:);
        % Pull out the track positions
            x = x(1:(end-1),:);
            y = y(1:(end-1),:);
    % Calculate the total step size       
        stepSizesInNM = sqrt(dx.^2 + dy.^2);
     % Calculate the mean and sd of this distribution
        meanStepSizeInNM = round(nanmean(stepSizesInNM));
        stdStepSizeInNM = round(nanstd(stepSizesInNM));
           
% Determine which and how many tracks there are
    tracksInThisCondition = find(dayIDByTrack>0)';
    numTracksTotal = length(tracksInThisCondition);  

% Subsample the tracks so we aren't plotting that many
    numTracks2Skip = floor(numTracksTotal/maxNumTracks2Plot);
    tracksInThisCondition = tracksInThisCondition(5:numTracks2Skip:end);
    numTracksPlotted = length(tracksInThisCondition);

% Count the number of cells represented
    length(unique(cellIDByTrack(tracksInThisCondition)))
    
% Loop through each track and plot it
    for trackNum = tracksInThisCondition
    
        % Prepare the data
            % Calculate the step size in each dimension
                % Pull out the track positions
                    x = squeeze(trackPositions_nm(trackNum,1,:))'./10^3;
                    y = squeeze(trackPositions_nm(trackNum,2,:))'./10^3;
                    z = zeros(size(x));
                % Calculate the step sizes
                    dx = diff(x)*10^3;
                    dy = diff(y)*10^3;
            % Set the color equal to the step size        
                color = sqrt(dx.^2 + dy.^2);
                color = (color-meanStepSizeInNM)./stdStepSizeInNM;          
            % Remove the last point 9that doesn't have an associated step
            % size)
                x = x(1:(end-1));
                y = y(1:(end-1));
                z = z(1:(end-1));
    
        % Optionally plot all tracks from a single cell together
            % Plot the tracks in real distances   
                figure(figureNumPlotAllTracks_RealPositions)
                % Plot the track
                    s = surface([x;x],[y;y],[z;z],[color;color],...
                        'facecol','no',...
                        'edgecol','interp',...
                        'linew',1);
                    hold on;
                    s.EdgeAlpha = 0.2;
                    colormap(bluewhitered*0.8)
                    caxis([-(meanStepSizeInNM./stdStepSizeInNM) 3])
                  %  trackNum
                   % ginput()
    end

% Clean up the plots
    figure(figureNumPlotAllTracks_RealPositions)
    hold off;
    axis equal  
    box(gca,'on')  
    ylim([-2 2])
    xlim([-6 6])
    cb = colorbar;
    cb.Ticks = ([0 100 200  300]-meanStepSizeInNM)./stdStepSizeInNM;
    cb.TickLabels = num2cell((cb.Ticks*stdStepSizeInNM)+meanStepSizeInNM);
    colorTitleHandle = get(cb,'Title');
    titleString = sprintf('step size (nm) \n gray indicates mean');
    set(colorTitleHandle ,'String',titleString);
    xlabel({'X-position (\mu m) relative to cell centroid','(new pole to right)'})
    ylabel({'Y-position (\mu m)','relative to cell centroid'})
    title({'All particle trajectories, subsampled',sprintf('%1.0f out of %1.0f tracks plotted',numTracksPlotted,numTracksTotal)})    
    set(gca,'FontName','Helvetica','FontSize',6);  
    drawnow  

% Save the figure 
    % Choose the filename and path for the figure
        destinationTrack = ['Garner_Molines_et_al\'...
            'Figure_Panels\Figure1\Fig1f_TrajectoryPlot_AllTracks'];
    % Choose the figure size and position
        figHandle = figure(figureNumPlotAllTracks_RealPositions);
        figHandle.Position =  [500 400 400 350];
    % Save the file
        exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
    % Save the figure in matlab figure format
        savefig([destinationTrack '.fig'])  

