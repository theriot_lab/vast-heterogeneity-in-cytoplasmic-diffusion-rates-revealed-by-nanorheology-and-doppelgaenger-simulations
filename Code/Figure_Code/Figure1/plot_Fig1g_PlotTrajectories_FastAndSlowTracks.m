%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    close all;
    clear all;

% Load the data
    % Choose the data file path
        filePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
        '\All_Controls_Combined_Analyzed.mat'];
    % Load the data
        load(filePath)

% Choose the figure number
    figureNumPlotLongestTracks = 2;

% Calculate the mean and std of the step size across all tracks     
    % Pull out the trajectories and calculate the step size in each dimension
        % Pull out the track positions
            x = squeeze(trackPositions_nm(:,1,:))';
            y = squeeze(trackPositions_nm(:,2,:))';
        % Calculate the step sizes
            dx = diff(x,1,1);
            dy = diff(y,1,1);
        % Concatenate step sizes for all tracks into a single vector
            dx = dx(:);
            dy = dy(:);
        % Pull out the track positions
            x = x(1:(end-1),:);
            y = y(1:(end-1),:);
    % Add these step sizes        
        stepSizesInNM = sqrt(dx.^2 + dy.^2);
     % Calculate the mean and sd of this distribution
        meanStepSizeInNM = round(nanmean(stepSizesInNM));
        stdStepSizeInNM = round(nanstd(stepSizesInNM));

% Pull out the longest tracks
    % Calculate each track's length
        trackLengthInTimeSteps = sum(~isnan(squeeze(sum(trackPositions_nm,2))),2);
    % Find the top 12
        num2Choose = 12;
        [maxTLVals,maxTLValIdx] = maxk(trackLengthInTimeSteps,num2Choose);

    % Plot the fast tracks colored by step size
        % Initialize a counter variable
            trackCounter = 0;
        % Loop through each track and plot it
        for trackNum = maxTLValIdx'

            % Update the counter
                trackCounter = trackCounter + 1
        
            % Prepare the data
                % Calculate the step size in each dimension
                    % Pull out the track positions
                        x = squeeze(trackPositions_nm(trackNum,1,:))'./10^3;
                        y = squeeze(trackPositions_nm(trackNum,2,:))'./10^3;
                        z = zeros(size(x));
                    % Calculate the step sizes
                        dx = diff(x)*10^3;
                        dy = diff(y)*10^3;
                % Set the color equal to the step size        
                    color = sqrt(dx.^2 + dy.^2);
                    color = (color-meanStepSizeInNM)./stdStepSizeInNM;      
                % Remove the last point that doesn't have an associated step
                % size)
                    x = x(1:(end-1));
                    x = x - nanmean(x);%+trackCounter;
                    y = y(1:(end-1));
                    y = y - nanmean(y);%+trackCounter;
                    z = z(1:(end-1));
                    z = z - nanmean(z);%+trackCounter;
        

                % Plot the tracks in real distances   
                    figure(figureNumPlotLongestTracks)
                    subplot(3,ceil(num2Choose/3),trackCounter)
                    % Plot the track
                        s = surface([x;x],[y;y],[z;z],[color;color],...
                            'facecol','no',...
                            'edgecol','interp',...
                            'linew',1);
                        hold on;
                        s.EdgeAlpha = 0.2;
                        colormap(bluewhitered*0.8)
                        caxis([-(meanStepSizeInNM./stdStepSizeInNM) 3])

                % Clean up the plots
                    figure(figureNumPlotLongestTracks)
                    hold off;
                    axis equal    
                    box(gca,'on')
                    ylim([-1.75 1.75])
                    xlim([-1.75 1.75])
                    cb = colorbar;
                    cb.Ticks = ([0 100 200  300]-meanStepSizeInNM)./stdStepSizeInNM;
                    cb.TickLabels = num2cell((cb.Ticks*stdStepSizeInNM)+meanStepSizeInNM);
                    colorTitleHandle = get(cb,'Title');
                    titleString = sprintf('step size (nm) \n gray indicates mean');
                    set(colorTitleHandle ,'String',titleString);
                    xlabel({'X-position (\mu m) relative to cell centroid','(new pole to right)'})
                    ylabel({'Y-position (\mu m)','relative to cell centroid'})
                    title({'Particle trajectories for a single cell'})    
                    set(gca,'FontName','Helvetica','FontSize',6);  
                    drawnow  
        end

% Save the figure
    % Choose the filename and path for the figure
        destinationTrack = ['Garner_Molines_et_al\'...
        'Figure_Panels\Figure1\Fig1g_TrajectoryPlot_LongTracks'];
    % Choose the figure size and position
        figHandle = figure(figureNumPlotLongestTracks);
        figHandle.Position =  [1   1  1536   789];
    % Save the figure in PDF format         
        exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
    % Save the figure in matlab figure format
        savefig(gcf,destinationTrack)

