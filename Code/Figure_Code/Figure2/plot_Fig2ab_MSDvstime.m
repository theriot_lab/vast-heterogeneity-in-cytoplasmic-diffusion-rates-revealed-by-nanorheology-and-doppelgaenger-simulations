%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system 
    % Close all figures
        close all;
        close all hidden; % ANOVA tables
    % Clear all variables
        clear all; 
    
% Choose the datasets to plot
    experimentalDatasetFilePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
        '\All_Controls_Combined_Analyzed.mat'];
    Experimental_Results = load(experimentalDatasetFilePath);
    
% Choose whether to save the images
    doSaveImages = true;  
   
% Choose where to make the plot
    % Choose the figure number
        figureNumMSDByCell = 11;
        figureNumMSDByTrack = 12;
    % Choose the condition numbers
        conditionNum2Plot = 1;

% Reset the conditions to be all one condition
    Experimental_Results.conditionIDByCell = ...
        ones(size(Experimental_Results.conditionIDByCell));
    Experimental_Results.conditionIDByTrack = ...
        ones(size(Experimental_Results.conditionIDByTrack));
    Experimental_Results.numConditions = 1;

% Perform the MSD analysis

    % Perform the time-averaged MSD vs time
        [Experimental_Results] = ...
            performMSD_AverageByTrackCellAndCondition(Experimental_Results);
    
    % Fit the MSD data
        [Experimental_Results] = ...
            performMSDFit_AverageByTrackCellAndCondition(Experimental_Results);


% Make the cell-wise MSD plot        

    % Choose the color of the plot
        experimentColor = [0.1 0.1 0.9 0.1];
                
    % Plot the MSD for each cell        
    for cellNum = 1:Experimental_Results.numCells
    
        figure(figureNumMSDByCell)
        % Plot the mean
            plot(Experimental_Results.MSDResults.time_interval_for_MSD_ms,...
                Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCell.mean(cellNum,:),...
                '-','Color',experimentColor)
            hold on;            
    end   

    % Clean up the plot
        xl = xlim();
        plot([xl(1) xl(2)],[0 0],'k')
        hold off;
        xlim([10^1 10^3])
        ylim([10^3 2*10^6])
        set(gca, 'YScale', 'log')
        set(gca, 'XScale', 'log')
        xlabel('Time offset (ms)')
        ylabel('MSD (nm^2)')
        title({'Time-averaged, ensemble(track)-averaged MSD'})
         set(gca,'FontName','Helvetica','FontSize',6); 
        legend({'Mean across cell'},...
            'Location','Northwest','FontSize',5) 
    
    if doSaveImages
        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                    'Figure_Panels\Figure2\Fig2b_AllControls_MSDByCell'];
            % Choose the figure size and position
                figHandle = figure(figureNumMSDByCell);
                figHandle.Position =  [100   615  190   160];
            % Turn off the legend
                legend(gca,'off');
            % Save the figure in PDF format         
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])
    end

% Make the tracks-wise MSD plot        

    % Choose the color of the plot
        experimentColor = [0.1 0.1 0.9 0.04];

    % Calculate the track averages        
        Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack.mean = ...
            squeeze(sum(Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_dims,2));    
                
    % Plot the MSD for each track        
    for trackNum = 1:Experimental_Results.numTracks

        figure(figureNumMSDByCell)
        % Plot the mean
            plot(Experimental_Results.MSDResults.time_interval_for_MSD_ms,...
                Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack.mean(trackNum,:),...
                '-','Color',experimentColor)
            hold on;            
    end   

    % Clean up the plot
        xl = xlim();
        plot([xl(1) xl(2)],[0 0],'k')
        hold off;
        xlim([10^1 10^3])
        ylim([10^3 2*10^6])
        set(gca, 'YScale', 'log')
        set(gca, 'XScale', 'log')
        xlabel('Time offset (ms)')
        ylabel('MSD (nm^2)')
        title({'Time-averaged, ensemble(track)-averaged MSD'})
         set(gca,'FontName','Helvetica','FontSize',6); 
        legend({'Mean across track'},...
            'Location','Northwest','FontSize',5) 
    
    if doSaveImages
        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                    'Figure_Panels\Figure2\Fig2a_AllControls_MSDByTrack'];
            % Choose the figure size and position
                figHandle = figure(figureNumMSDByCell);
                figHandle.Position =  [100   615  190   160];
            % Turn off the legend
                legend(gca,'off');
            % Save the figure in PDF format         
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])
    end











