%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
close all;
clear all;

% Load the data into a new table

    % Load the data
        % Choose the data file path
            filePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
        '\All_Controls_Combined_Analyzed.mat'];
        % Load the relevant information
            % Load the condition information    
                load(filePath,'conditionIDByTrack')
                load(filePath,'conditionIDByCell')
            % Load the MSD fitting results    
                load(filePath,'MSDFitResults')

    % Initialize the number of tracks counter
        numEntriesTotal = 0;

    % Concatenate the track-wise data into one structure
        % Count the number of tracks
            numTracks = length(conditionIDByTrack);
        % Add the diffusivities and alpha values
            allData.Log10_of_D_app_Val_nm2_per_ms_FitMean(numEntriesTotal + (1:numTracks),1) = ...
                log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack);
            allData.alphaVal_FitMean(numEntriesTotal + (1:numTracks),1) = ...
                MSDFitResults.alphaVal_FitMeanByTrack;
        % Add the replicates
           allData.ReplicateID(numEntriesTotal + (1:numTracks),1) = conditionIDByTrack;
        % Add the experiment vs simulationID
           allData.ExpmtVsSim(numEntriesTotal + (1:numTracks),1) = {'Experiment by Track'};
        % Add the simulation replicate ID
           allData.SimReplicateID(numEntriesTotal + (1:numTracks),1) = nan;
        % Update the track counter
            numEntriesTotal = numEntriesTotal + numTracks;

    % Concatenate the cell-wise data into one structure
        % Count the number of tracks
            numCells = length(conditionIDByCell);
        % Add the diffusivities and alpha values
            allData.Log10_of_D_app_Val_nm2_per_ms_FitMean(numEntriesTotal + (1:numCells),1) = ...
                log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell);
            allData.alphaVal_FitMean(numEntriesTotal + (1:numCells),1) = ...
                MSDFitResults.alphaVal_FitMeanByCell;
        % Add the replicates
           allData.ReplicateID(numEntriesTotal + (1:numCells),1) = conditionIDByCell;
        % Add the experiment vs simulationID
           allData.ExpmtVsSim(numEntriesTotal + (1:numCells),1) = {'Experiment by Cell'};
        % Add the simulation replicate ID
           allData.SimReplicateID(numEntriesTotal + (1:numCells),1) = nan;
        % Update the track counter
            numEntriesTotal = numEntriesTotal + numCells;

% Convert the result to a table
    T = struct2table(allData);

% Extract the track data

    % Pull out the track data only
        dataIdx = find(startsWith(T.ExpmtVsSim,'Experiment'));

    % Pre-allocate space to store the rearranged dataset  
        % Assign groups
            [uniqueGroups, uniqueGroupsFirstInstance, groupID] = ...
                unique(T.ExpmtVsSim(dataIdx)); 
        % Choose the order to plot the groups
            groupOrder2Plot = [2 1];
        % Count the number of datapoints in each group
            % List the unqiue condition IDs
                uniqueGroupIDs = 1:max(groupID);
            % Count the number of tracks per ID
                numTracksPerGroup = histc(groupID,...
                    uniqueGroupIDs);
            % Count the number of groups
                numGroups = length(uniqueGroups);
        % Create the empty matrices
            D2Plot = nan(max(numTracksPerGroup),1,numGroups);
            alpha2Plot = nan(max(numTracksPerGroup),1,numGroups);

    % Load the track data into a single matrix
        for groupNum = 1:numGroups
                
            % Fill in the matrix
                D2Plot(1:numTracksPerGroup(groupNum),1,...
                    groupNum) = T.Log10_of_D_app_Val_nm2_per_ms_FitMean(...
                        dataIdx(groupID==groupNum));
                alpha2Plot(1:numTracksPerGroup(groupNum),1,...
                    groupNum) = T.alphaVal_FitMean(...
                        dataIdx(groupID==groupNum));
        
        end   

% Plot the results

    % Choose the plotting parameters
        % Choose the plot locations
            figureNumBoxPlots = 1;
            numCols = 2;
            numRows = 1;
            numSubplotD = 1;
            numSubplotAlpha = 2;
        % Choose which color to plot the conditions    
            CMByGroup = jet(numGroups)*0.75;
            CMByGroup = repmat([0.37 0.42 1],[2 1]);
          %  CMByGroup(:) = 0;
           % CMByGroup(:,4) = 0.1;
           % CMByGroup(4,:) = CMByGroup(4,:)*0.75;
        % Choose the dot size
            dotSizeTrack = 5;
        % Choose the transparency
            alphaValTrack = 0.1;
            alphaValCell = 0.3;
        % Choose the group names
            labels = {'Tracks','Cells'};

% Make box plots

    % Plot the track-wise diffusivity, where stars are test of equal variance    
        [hBoxPlot_D] = ...
            plotBoxPlotsDiffusivity(figureNumBoxPlots,numRows,numCols,...
                numSubplotD,D2Plot(:,:,groupOrder2Plot),...
                CMByGroup,...
                labels,dotSizeTrack);
        % Make closed circles
            hBoxPlot_D.handles.scatters(1).MarkerFaceColor = hBoxPlot_D.handles.scatters(1).MarkerEdgeColor;
            hBoxPlot_D.handles.scatters(1).MarkerFaceAlpha = hBoxPlot_D.handles.scatters(1).MarkerEdgeAlpha;
            hBoxPlot_D.handles.scatters(1).MarkerFaceAlpha = alphaValTrack;
            hBoxPlot_D.handles.scatters(1).MarkerEdgeColor = 'none';
            hBoxPlot_D.handles.outliers(1).MarkerFaceColor = hBoxPlot_D.handles.outliers(1).MarkerEdgeColor;
            hBoxPlot_D.handles.outliers(1).MarkerFaceAlpha = hBoxPlot_D.handles.outliers(1).MarkerEdgeAlpha;
            hBoxPlot_D.handles.outliers(1).MarkerFaceAlpha = alphaValTrack;
            hBoxPlot_D.handles.outliers(1).MarkerEdgeColor = 'none';
            hBoxPlot_D.handles.scatters(2).MarkerFaceColor = hBoxPlot_D.handles.scatters(2).MarkerEdgeColor;
            hBoxPlot_D.handles.scatters(2).MarkerFaceAlpha = hBoxPlot_D.handles.scatters(2).MarkerEdgeAlpha;
            hBoxPlot_D.handles.scatters(2).MarkerFaceAlpha = alphaValCell;
            hBoxPlot_D.handles.scatters(2).MarkerEdgeColor = 'none';
            hBoxPlot_D.handles.outliers(2).MarkerFaceColor = hBoxPlot_D.handles.outliers(2).MarkerEdgeColor;
            hBoxPlot_D.handles.outliers(2).MarkerFaceAlpha = hBoxPlot_D.handles.outliers(2).MarkerEdgeAlpha;
            hBoxPlot_D.handles.outliers(2).MarkerFaceAlpha = alphaValCell;
            hBoxPlot_D.handles.outliers(2).MarkerEdgeColor = 'none';
            % Plot lines markign 500 fold chanes
               line_xloc = mean([mean(hBoxPlot_D.handles.scatters(1).XData) ...
                   mean(hBoxPlot_D.handles.scatters(2).XData)]);
               hold on;
               y1 = 8;
               foldChange = 400;
               plot([line_xloc line_xloc],[log10(y1), log10(y1*foldChange)],'k-')
               line_xloc = line_xloc + mean(hBoxPlot_D.handles.scatters(2).XData) - ...
                   mean(hBoxPlot_D.handles.scatters(1).XData);
               y1 = 85;
               foldChange = 10;
               plot([line_xloc line_xloc],[log10(y1), log10(y1*foldChange)],'k-')
               hold off;
        title({'Distribution of diffusivities','from track-wise MSD fits'})
        set(gca,'FontName','Helvetica','FontSize',5);  
        ylim([-0.5 4.5])
        box on;  
    % Plot the track-wise alpha, where stars are test of equal variance      
        [hBoxPlot_Alpha] = ...
            plotBoxPlotsAlpha(figureNumBoxPlots,numRows,numCols,...
                numSubplotAlpha,alpha2Plot(:,:,groupOrder2Plot),...
                CMByGroup,...
                labels,dotSizeTrack);
        title({'Distribution of power law exponents','from track-wise MSD fits'})
        set(gca,'FontName','Helvetica','FontSize',5);   
        ylim([-2.5 2.5])
        box on;
        % Make closed circles
            hBoxPlot_Alpha.handles.scatters(1).MarkerFaceColor = hBoxPlot_Alpha.handles.scatters(1).MarkerEdgeColor;
            hBoxPlot_Alpha.handles.scatters(1).MarkerFaceAlpha = hBoxPlot_Alpha.handles.scatters(1).MarkerEdgeAlpha;
            hBoxPlot_Alpha.handles.scatters(1).MarkerFaceAlpha = alphaValTrack;
            hBoxPlot_Alpha.handles.scatters(1).MarkerEdgeColor = 'none';
            hBoxPlot_Alpha.handles.outliers(1).MarkerFaceColor = hBoxPlot_Alpha.handles.outliers(1).MarkerEdgeColor;
            hBoxPlot_Alpha.handles.outliers(1).MarkerFaceAlpha = hBoxPlot_Alpha.handles.outliers(1).MarkerEdgeAlpha;
            hBoxPlot_Alpha.handles.outliers(1).MarkerFaceAlpha = alphaValTrack;
            hBoxPlot_Alpha.handles.outliers(1).MarkerEdgeColor = 'none';
            hBoxPlot_Alpha.handles.scatters(2).MarkerFaceColor = hBoxPlot_Alpha.handles.scatters(2).MarkerEdgeColor;
            hBoxPlot_Alpha.handles.scatters(2).MarkerFaceAlpha = hBoxPlot_Alpha.handles.scatters(2).MarkerEdgeAlpha;
            hBoxPlot_Alpha.handles.scatters(2).MarkerFaceAlpha = alphaValCell;
            hBoxPlot_Alpha.handles.scatters(2).MarkerEdgeColor = 'none';
            hBoxPlot_Alpha.handles.outliers(2).MarkerFaceColor = hBoxPlot_Alpha.handles.outliers(2).MarkerEdgeColor;
            hBoxPlot_Alpha.handles.outliers(2).MarkerFaceAlpha = hBoxPlot_Alpha.handles.outliers(2).MarkerEdgeAlpha;
            hBoxPlot_Alpha.handles.outliers(2).MarkerFaceAlpha = alphaValCell;
            hBoxPlot_Alpha.handles.outliers(2).MarkerEdgeColor = 'none';
        
    % Save the figure as a png
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure2\Fig2cd_Controls_BoxPlots'];
        % Choose the figure size and position
            figHandle = figure(figureNumBoxPlots);
            figHandle.Position =  [100   573   347  122];
        % Save the file as a pdf
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])
