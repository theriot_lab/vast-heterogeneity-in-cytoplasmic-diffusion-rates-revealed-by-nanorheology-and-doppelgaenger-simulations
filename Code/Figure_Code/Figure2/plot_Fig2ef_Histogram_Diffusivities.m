%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system 
    % Close all figures
        close all;
        close all hidden; % ANOVA tables
    % Clear all variables
        clear all; 
    
% Choose the datasets to plot
    experimentalDatasetFilePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
        '\All_Controls_Combined_Analyzed.mat'];
    Experimental_Results = load(experimentalDatasetFilePath);

% Reset the conditions to be all one condition
    Experimental_Results.conditionIDByCell = ...
        ones(size(Experimental_Results.conditionIDByCell));
    Experimental_Results.conditionIDByTrack = ...
        ones(size(Experimental_Results.conditionIDByTrack));
    Experimental_Results.numConditions = 1;

% Perform the MSD analysis

    % Perform the time-averaged MSD vs time
        [Experimental_Results] = ...
            performMSD_AverageByTrackCellAndCondition(Experimental_Results);
    
    % Fit the MSD data
        [Experimental_Results] = ...
            performMSDFit_AverageByTrackCellAndCondition(Experimental_Results);


% Choose the color for the bars
    experimentColor = [0.1 0.1 0.9]; 

% Plot a histogram of the Diffusivity

    % Pull up the figure
        figureNumHistD = 1;
        figure(figureNumHistD)
    % Plot the histogram in real space
        subplot(1,2,1)
        h1_Cell = histogram(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell,15,...
            'Normalization','pdf','FaceColor',[0 0 0]);
        hold on;
        h1_Track = histogram(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack,100,...
            'Normalization','pdf','FaceColor',experimentColor);
        hold off;
        % Clean up the plot
            xlim([0 2]*10^(3))
            ylim([0 3.5]*10^(-3))
            xlabel('Apparent diffusivity at 100 ms (nm^2/ms)')
            ylabel('Probability')
            legend(sprintf('<D_{app, 100 ms}: \n %1.0f \\pm %1.0f nm^2/ms>_{cell}',...
                nanmean(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell),...
                nanstd(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell)),...
                sprintf('<D_{app, 100 ms}: \n %1.0f \\pm %1.0f nm^2/ms>_{track}',...
                nanmean(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack),...
                nanstd(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack)))
            set(gca,'FontName','Helvetica','FontSize',6); 
    % Plot the histogram in log space
        subplot(1,2,2)
        h2_Cell = histogram(log10(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell),15,...
            'Normalization','pdf','FaceColor',[0 0 0]);
        hold on;
        h2_Track = histogram(log10(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack),100,...
            'Normalization','pdf','FaceColor',experimentColor);
        hold off;
        % Clean up the plot
            xlim([-2.75 log10(5)]+3)
            ylim([0 2.5])
            xlabel('log_{10}(Apparent diffusivity at 100 ms (nm^2/ms))')
            ylabel('Probability')
            set(gca,'FontName','Helvetica','FontSize',6); 

    % Save the figure 
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure2\Fig2ef_Hist_D'];
        % Choose the figure size and position
            figHandle = figure(figureNumHistD);
            figHandle.Position =  [100   489   333   261];
            figHandle.Position =  [100   545   530   210];
        % Turn off the legend
            subplot(1,2,1)
            legend(gca,'off');
        % Save the file as a pdf
            exportgraphics(gcf,[destinationTrack '.pdf'],...
                'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])  


% Plot the inset
    figure(figureNumHistD)
    subplot(1,2,1)
    xlim([0 5]*10^3)
    ylim([0 3.5]*10^(-3))
    subplot(1,2,2)
    xlim([-2.75 log10(5)]+3)
    ylim([0 2.5])

% Save the inset
    % Choose the filename and path for the figure
        destinationTrack = ['Garner_Molines_et_al\'...
            'Figure_Panels\Figure2\Fig2ef_Hist_D_Inset'];
    % Choose the figure size and position
        figHandle = figure(figureNumHistD);
        figHandle.Position =  [100   489   333   261];
        figHandle.Position =  [100   545   530   210];
    % Turn off the legend
        subplot(1,2,1)
        legend(gca,'off');
    % Save the file as a pdf
        exportgraphics(gcf,[destinationTrack '.pdf'],...
            'ContentType','vector')
    % Save the figure in matlab figure format
        savefig([destinationTrack '.fig'])  


% Calculate the coefficient of variation
CV_D_ByTrack = nanstd(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack)/...
    nanmean(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack)
CV_D_ByCell = nanstd(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell)/...
    nanmean(Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell)
