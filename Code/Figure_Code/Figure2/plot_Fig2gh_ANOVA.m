%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system 
    % Close all figures
        close all;
        close all hidden; % ANOVA tables
    % Clear all variables
        clear all; 

% Load the data into a summary table
    % Choose the data file path
        filePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
        '\All_Controls_Combined_Analyzed.mat'];
    % Load the ANOVA results    
        load(filePath,'ANOVAResults')
    % Save the results in a summary table                
        ANOVAComparison.ExpVsSim(1) = {'Experiments'};
        ANOVAComparison.SumSqLnD(:,1) = cell2mat(ANOVAResults.tbl_ln_D(2:end,2));
        ANOVAComparison.SumSqAlpha(:,1) = cell2mat(ANOVAResults.tbl_Alpha(2:end,2));
        ANOVAComparison.Labels = ANOVAResults.tbl_Alpha(2:end,1);


% Normalize data to the total variance 
    ANOVAComparison.SumSqLnD_Norm2Total = bsxfun(@rdivide,...
        ANOVAComparison.SumSqLnD,ANOVAComparison.SumSqLnD(6,:));
    ANOVAComparison.SumSqAlpha_Norm2Total = bsxfun(@rdivide,...
        ANOVAComparison.SumSqAlpha,ANOVAComparison.SumSqAlpha(6,:));

% Plot the results
    % Plot the ANOVA on fits of diffusivity
        % Choose where to plot the data
            figureNumANOVAExpmts = 2;
            numRows = 2;
            numCols = 1;
            numSubplotD = 1;
        % Choose the bar information
            barHeights = ANOVAComparison.SumSqLnD_Norm2Total([1:4 6],1)';
            barNames = ANOVAComparison.Labels([1:4 6]);
            barColors = [[0 0.5 1]*0.55;
                [1 0 0]*0.55;
                [0 1 0]*0.25;
                [1 0 1]*0.25;
                [1 1 1]*0.55];
       % Make the plot
            makeMultiColorBarPlotsWithLabelsAndErrors(figureNumANOVAExpmts,numRows,...
                numCols,numSubplotD,barHeights,barNames,barColors)
        % Clean up the plot
            ylim([0 1.25])
            ylabel({'Fraction of total variability','Sum Sq. ln(Diffusivity)'})
            title('ANOVA on trackwise MSD fit parameters')
            set(gca,'FontSize',6)
    % Plot the ANOVA on fits of alpha
        % Choose where to plot the data
            numSubplotAlpha = 2;
        % Update the bar information
            barHeights = ANOVAComparison.SumSqAlpha_Norm2Total([1:4 6],1)';
       % Make the plot
            makeMultiColorBarPlotsWithLabelsAndErrors(figureNumANOVAExpmts,numRows,...
                numCols,numSubplotAlpha,barHeights,barNames,barColors)
        % Clean up the plot
            ylim([0 1.25])
            ylabel({'Fraction of variability explained','Sum Sq. \alpha'})
            set(gca,'FontSize',6)        
    
% Save the figure
    % Choose the filename and path for the figure
        destinationTrack = ['Garner_Molines_et_al\'...
            'Figure_Panels\Figure2\Fig2gh_ANOVA'];
    % Choose the figure size and position
        figHandle = figure(figureNumANOVAExpmts);
        figHandle.Position = [529   293   343   456];
    % Save the file as a pdf
        exportgraphics(gcf,[destinationTrack '.pdf'],...
            'ContentType','vector')
    % Save the figure in matlab figure format
        savefig([destinationTrack '.fig'])  