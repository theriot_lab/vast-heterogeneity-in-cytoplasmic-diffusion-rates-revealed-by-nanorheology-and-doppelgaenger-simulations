%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    close all;
    clear all;    
    
% Load the data into a summary table

    % Determine the number of simulation replicates
        % Choose the data file path
            % Choose one file in the folder
                simFolderPath = ['Garner_Molines_et_al\'...
                'Simulation_Data\CellAndSpaceVar_Replicates_20220720\'];
                groupNames = {'Experiment', 'Model #1', 'Model #2', 'Model #3', 'Model #4'};
                folderInfo = dir(simFolderPath);
                fileName = folderInfo(3).name;
                exampleFilePath = [simFolderPath fileName];
            % Load the parameter information    
                load(exampleFilePath,'fracCellVar2SimNums')
                load(exampleFilePath,'fracSpaceVar2SimNums')
                load(exampleFilePath,'viscosityDomainSizeInNM2SimNums')
                load(exampleFilePath,'replicateNums')
                load(exampleFilePath,'ParamScanName')
                load(exampleFilePath,'dateNum')
            % Determine the number of replicates
                numReplicates = length(unique(replicateNums));

    % Add the experimental track data to the structure
        % Choose the data file path
            filePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
                '\All_Controls_Combined_Analyzed.mat'];
        % Load the ANOVA results    
            load(filePath,'ANOVAResults')
        % Save the results in a summary table                
            ANOVAComparison.ExpVsSim(1) = {'Experiments'};
            ANOVAComparison.SumSqLnD(:,1,:) = ...
                repmat(cell2mat(ANOVAResults.tbl_ln_D(2:end,2)),[1,1,numReplicates]);
            ANOVAComparison.Labels = ANOVAResults.tbl_Alpha(2:end,1);

    % Add the simulation data to the structure
    
    for numReplicate = 1:numReplicates

    % Add the simulation without variability data to the structure
        % Choose the data file path
            % Find which simulation we want
                sumNum = find((fracCellVar2SimNums==1)&(fracSpaceVar2SimNums==1)&...
                    (viscosityDomainSizeInNM2SimNums==1)&(replicateNums==numReplicate));
            % Determine the filename
                fileName = sprintf('Results_ExpStatsAndLength_%s_%s_%i.mat',...
                    ParamScanName,dateNum,sumNum);
                filePath = [simFolderPath fileName]; 
        % Load the ANOVA results    
            load(filePath,'ANOVAResults')
        % Save the results in a summary table                
            ANOVAComparison.ExpVsSim(2) = {'Simulation No Var by track'};
            ANOVAComparison.SumSqLnD(:,2,numReplicate) = cell2mat(ANOVAResults.tbl_ln_D(2:end,2));

    % Add the simulation with only spatial variabilty data to the structure
        % Choose the data file path
            % Find which simulation we want
                sumNum = find((fracCellVar2SimNums==1)&(fracSpaceVar2SimNums==2)&...
                    (viscosityDomainSizeInNM2SimNums==1)&(replicateNums==numReplicate));
            % Determine the filename
                fileName = sprintf('Results_ExpStatsAndLength_%s_%s_%i.mat',...
                    ParamScanName,dateNum,sumNum);
                filePath = [simFolderPath fileName]; 
        % Load the ANOVA results    
            load(filePath,'ANOVAResults')
        % Save the results in a summary table                
            ANOVAComparison.ExpVsSim(3) = {'Simulation Spatial Var by track'};
            ANOVAComparison.SumSqLnD(:,3,numReplicate) = cell2mat(ANOVAResults.tbl_ln_D(2:end,2));

    % Add the simulation with only cell var data to the structure
        % Choose the data file path
            % Find which simulation we want
                sumNum = find((fracCellVar2SimNums==2)&(fracSpaceVar2SimNums==1)&...
                    (viscosityDomainSizeInNM2SimNums==1)&(replicateNums==numReplicate));
            % Determine the filename
                fileName = sprintf('Results_ExpStatsAndLength_%s_%s_%i.mat',...
                    ParamScanName,dateNum,sumNum);
                filePath = [simFolderPath fileName]; 
        % Load the ANOVA results    
            load(filePath,'ANOVAResults')
        % Save the results in a summary table                
            ANOVAComparison.ExpVsSim(4) = {'Simulation Cell Var by track'};
            ANOVAComparison.SumSqLnD(:,4,numReplicate) = cell2mat(ANOVAResults.tbl_ln_D(2:end,2));

    % Add the simulation with both variabilty data to the structure
        % Choose the data file path
            % Find which simulation we want
                sumNum = find((fracCellVar2SimNums==2)&(fracSpaceVar2SimNums==2)&...
                    (viscosityDomainSizeInNM2SimNums==1)&(replicateNums==numReplicate));
            % Determine the filename
                fileName = sprintf('Results_ExpStatsAndLength_%s_%s_%i.mat',...
                    ParamScanName,dateNum,sumNum);
                filePath = [simFolderPath fileName]; 
        % Load the ANOVA results    
            load(filePath,'ANOVAResults')
        % Save the results in a summary table                
            ANOVAComparison.ExpVsSim(5) = {'Simulation Both Var by track'};
            ANOVAComparison.SumSqLnD(:,5,numReplicate) = cell2mat(ANOVAResults.tbl_ln_D(2:end,2));

     end

% Normalize the results to the experimental data
    ANOVAComparison.SumSqLnD_Norm2Expmt = bsxfun(@rdivide,...
        ANOVAComparison.SumSqLnD,ANOVAComparison.SumSqLnD(:,1));

% Plot the results
    % Choose the figure number
        figureNumANOVAExpmtVsSim = 1;
        figure(figureNumANOVAExpmtVsSim)
    % Make the ANOVA plot for the diffusivities
        % Plot the bars
            b = bar(mean(ANOVAComparison.SumSqLnD_Norm2Expmt([1:2 6],:,:),3)','FaceColor','flat');
            hold on;
        % Get the x coordinate of the bars
            x = nan(3, 5);
            for i = 1:3
                x(i,:) = b(i).XEndPoints;
            end
        % Plot the errorbars at the x-coordinates
            errorbar(x',mean(ANOVAComparison.SumSqLnD_Norm2Expmt([1:2 6],:,:),3)',...
                std(ANOVAComparison.SumSqLnD_Norm2Expmt([1:2 6],:,:),[],3)',...
                'k','linestyle','none'); 
        % Plot a line at 100%   
            xl = get(gca,'xlim');
            p1 = plot(xl,[1 1],'--','Color',[1 1 1]*0.75);
        % Turn off the default labels
            uistack(p1,'bottom')
            p1.Annotation.LegendInformation.IconDisplayStyle = 'off';
            hold off;        
        % Adjust the colors of the bars
            b(1).CData(:,:) = repmat([0 0.5 1]*0.55,size(b(1).CData,1),1);
            b(2).CData(:,:) = repmat([1 0 0]*0.55,size(b(2).CData,1),1);
            b(3).CData(:,:) = repmat([1 1 1]*0.55,size(b(3).CData,1),1);
        % Choose the axes limits and label the graph
            ylim([0 2])
            ylabel({'Fraction of experimental','variability explained'})
            title({'ANOVA on track-wise fit parameters','ln(Apparent diffusivity)'})
            set(gca,'xticklabel', groupNames)
            set(gca,'FontSize',6)
            legend(ANOVAComparison.Labels([1:2 6]),'Location','Northwest')
        % Label the bar heights
        for n = 1:length(b)
            xtips2 = b(n).XEndPoints;
            ytips2 = b(n).YEndPoints+0.1;
            labels2 = {};
            for numPts = 1:length(xtips2)
            labels2(numPts) = {sprintf('%1.2f',b(n).YData(numPts))};
            end
            text(xtips2,ytips2,labels2,'HorizontalAlignment','center',...
                'VerticalAlignment','bottom','FontSize',5)
        end

% Save the figure
    % Choose the filename and path for the figure
        destinationTrack = ['Garner_Molines_et_al\'...
        'Figure_Panels\Figure3\Fig3f_ExpmtVsSims_ANOVA'];
    % Choose the figure size and position
        figHandle = figure(figureNumANOVAExpmtVsSim);
        figHandle.Position =  [100   301   423   224];
    % Save the file as a pdf
        exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
    % Save the figure in matlab figure format
        savefig([destinationTrack '.fig'])

