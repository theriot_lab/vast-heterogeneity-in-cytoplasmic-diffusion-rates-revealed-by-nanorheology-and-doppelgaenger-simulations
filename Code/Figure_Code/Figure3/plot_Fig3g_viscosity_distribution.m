%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
close all;
clear all;

% Load the simulation data
    % Choose the folder path containng the simulation results
        simulationsFolderPath = ['Garner_Molines_et_al\'...
                'Simulation_Data\CellAndSpaceVar_Replicates_20220720\'];
    % Choose the parameter indices for each simulation we want to pull out
        % fracCellVar2SimNum fracSpaceVar2SimNum viscosityDomainSizeInNM2SimNum replicateNum
             simIdx2PullOut = [2 2 1 1];
    % Load an example file to get some information about the dataset
        % Choose example file
            folderInfo = dir(simulationsFolderPath);
            fileName = folderInfo(3).name;
            exampleFilePath = [simulationsFolderPath fileName];
        % Load the parameter information    
            load(exampleFilePath,'fracCellVar2SimNums')
            load(exampleFilePath,'fracSpaceVar2SimNums')
            load(exampleFilePath,'viscosityDomainSizeInNM2SimNums')
            load(exampleFilePath,'replicateNums')
            load(exampleFilePath,'ParamScanName')
            load(exampleFilePath,'dateNum')              
        % Find which simulation we want
            sumNum = find((fracCellVar2SimNums==simIdx2PullOut(1))&...
                (fracSpaceVar2SimNums==simIdx2PullOut(2))&...
                (viscosityDomainSizeInNM2SimNums==simIdx2PullOut(3))&...
                (replicateNums==simIdx2PullOut(4)));
            % Determine the filename
                fileName = sprintf('Results_ExpStatsAndLength_%s_%s_%i.mat',...
                    ParamScanName,dateNum,sumNum);
                filePath = [simulationsFolderPath fileName]; 
        % Load the relevant information
            % Load the dataset name 
                load(filePath)

% Pull out the mean and sd of the average cellular viscosity   
    mu_Viscosity_Cell = etaValWater*etaValMult;
    sigma_Viscosity_Cell = fracCellVar2Sim*mu_Viscosity_Cell;
            
% Plot the distribution of intracellular viscosities in log base 10 for 
% several example average cell viscosities 
% (to see orders of magnitude of variation about the viscosity of water) 

    % Plot the cellular viscosity distribution
        figureNumViscosity = 2;
        figure(figureNumViscosity)
        % Choose the x values of viscosity (base 10)
            xVals = -7:0.01:-2.5;
        % Create the distribution
            % Mean of lognormal visocity distribution (base 10)
                mu = log10(mu_Viscosity_Cell);
            % SD of lognormal visocity distribution (base 10)
                sigma = sigma_Viscosity_Cell/(mu_Viscosity_Cell*log(10));
            % Resultant lognormal distribution (log base 10)    
                yVals = (1/(sigma*sqrt(2*pi)))*...
                    exp(-0.5*((xVals-mu)./sigma).^2);
        % Plot the distribution
            plot(xVals-log10(etaValWater),yVals,'--','LineWidth',1,...
                'Color',[0.5 0.5 0.5 0.3]);  
            hold on;
    
    % Determine the average cell viscosity 3 sigma above and below the mean
        distribution_eta_Cell_3Sigma_Low_0Pt15th = ...
            exp(log(mu_Viscosity_Cell) - ...
            (3*sigma_Viscosity_Cell/mu_Viscosity_Cell));
        distribution_eta_Cell_3Sigma_High_99Pt85th = ...
            exp(log(mu_Viscosity_Cell) + ...
            (3*sigma_Viscosity_Cell/mu_Viscosity_Cell));
    
    % Loop through the average, 10th percentile, and 90th percentile cells and
    % plot the spatial viscosity distribution 
    for mu_Viscosity_Space = [distribution_eta_Cell_3Sigma_Low_0Pt15th ...
            mu_Viscosity_Cell distribution_eta_Cell_3Sigma_High_99Pt85th]  

        % Load the SD of the intracellular viscosity distribution
            sigma_Viscosity_Space = fracSpaceVar2Sim*mu_Viscosity_Space;    
        % Create the distribution
            % Mean of lognormal visocity distribution (base 10)
                mu = log10(mu_Viscosity_Space);
            % SD of lognormal visocity distribution (base 10)
                sigma = sigma_Viscosity_Space/(mu_Viscosity_Space*log(10));
            % Resultant lognormal distribution (log base 10)    
                yVals = (1/(sigma*sqrt(2*pi)))*...
                    exp(-0.5*((xVals-mu)./sigma).^2);
        % Plot the distribution
            plot(xVals-log10(etaValWater),yVals,'-','LineWidth',2);  
    
    end
    
    % Label the plot
        % Add log lines
            plot([log10(10) log10(100)], [2 2],'k-')
            plot([log10(1) log10(400)], [2.5 2.5],'k--')
        hold off;
        ylim([0 3.5])
        xlim([-1 4])
        xlabel('log_{10}(Viscosity / Viscosity of water)')
        ylabel('Probability density')
        title('Distribution of intracellular cell viscosity')
        legend({'Distribution of cell averages',...
            'Example Cell #1 - \mu_s = \mu_c - 3\sigma_c',...
            'Example Cell #2 - \mu_s = \mu_c',...
            'Example Cell #3 - \mu_s = \mu_c + 3\sigma_c'})
        set(gca,'FontName','Helvetica','FontSize',6);
        
    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure3\Fig3g_ViscotiyDistribution'];
        % Choose the figure size and position
            figHandle = figure(figureNumViscosity);
            figHandle.Position =  [100   545   225   150];
        % Save the file as a pdf 
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector') 
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])
        