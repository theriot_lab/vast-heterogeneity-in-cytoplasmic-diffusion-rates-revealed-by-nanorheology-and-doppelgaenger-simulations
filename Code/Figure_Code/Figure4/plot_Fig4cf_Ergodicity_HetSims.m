%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    % Close all figures
        close all;
        close all hidden; % ANOVA tables
    % Clear all variables
        clear all; 
    
% Choose the datasets to plot

    % Load the simulation data
        % Choose the folder path containng the simulation results
            simulationsFolderPath = ['Garner_Molines_et_al\Simulation_Data\'...
                'ParameterScan_CellVar_vs_SpaceVar_vs_BoxSize_ToggleOnOff_Replicates_ErgAndMeanOpt\'];
        % Choose the parameter indices for each simulation we want to pull out
            % cols: fracCellVar2SimNum fracSpaceVar2SimNum viscosityDomainSizeInNM2SimNum replicateNum
            % rows: no var; spatial var; cell var; both 
                simIdx2PullOut = [2 2 1 3];
        % Load an example file to get some information about the dataset
            % Choose example file
                folderInfo = dir(simulationsFolderPath);
                fileName = folderInfo(3).name;
                exampleFilePath = [simulationsFolderPath fileName];
            % Load the parameter information    
                load(exampleFilePath,'fracCellVar2SimNums')
                load(exampleFilePath,'fracSpaceVar2SimNums')
                load(exampleFilePath,'viscosityDomainSizeInNM2SimNums')
                load(exampleFilePath,'replicateNums')
                load(exampleFilePath,'ParamScanName')
                load(exampleFilePath,'dateNum') 
            % Find which simulation we want
                sumNum = find((fracCellVar2SimNums==simIdx2PullOut(1))&...
                    (fracSpaceVar2SimNums==simIdx2PullOut(2))&...
                    (viscosityDomainSizeInNM2SimNums==simIdx2PullOut(3))&...
                    (replicateNums==simIdx2PullOut(4)));
                % Determine the filename
                    fileName = sprintf('Results_ExpStatsAndLength_%s_%s_%i.mat',...
                        ParamScanName,dateNum,sumNum);
                    filePath = [simulationsFolderPath fileName]; 

    % Re-analyze the data

        % Load the dataset
            Simulation_Results = load(filePath);

        % Reset the conditions to be all one condition
            Simulation_Results.conditionIDByCell = ...
                ones(size(Simulation_Results.conditionIDByCell));
            Simulation_Results.conditionIDByTrack = ...
                ones(size(Simulation_Results.conditionIDByTrack));
            Simulation_Results.numConditions = 1;
                
        % Perform the MSD analysis
        
            % Perform the time-averaged MSD vs time
                [Simulation_Results] = ...
                    performMSD_EnsembleAverageByTrackCellAndCondition(Simulation_Results);
            
            % Fit the MSD data
                [Simulation_Results] = ...
                    performMSDFit_EnsembleAverage(Simulation_Results);
 
% Save the MSD vs time data
    % Pull out the time points to plot
        timeInMS = Simulation_Results.MSDResults.time_interval_for_MSD_ms;
    % Time used to calculate the apparent diffusivity
        timeIntervalForDApp_ms = Simulation_Results.MSDFitResults.timeIntervalForDApp_ms;
    % Pull out the MSDs
        % Ensemble average
            MSD_EnsAvg_mean = ...
                Simulation_Results.MSDResults.MSD_nm2_EnsAvg';
            MSD_EnsAvg_sd = ...
                Simulation_Results.MSDResults.SDSD_nm2_EnsAvg';
            MSD_EnsAvg_se = ...
                Simulation_Results.MSDResults.SESD_nm2_EnsAvg';
        % Ensemble average fits
           alpha_EnsAvg = Simulation_Results.MSDFitResults.alphaVal_FitEnsAvg;
           D_EnsAvg = Simulation_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitEnsAvg;
        % Time-ensemble average
            MSD_TimeEnsAvg_mean = ...
                Simulation_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(1,:);
            MSD_TimeEnsAvg_sd = ...
                Simulation_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.stdmean(1,:);
            MSD_TimeEnsAvg_se = ...
                Simulation_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.semean(1,:);
        % Time-ensemble average fits
           alpha_TimeEnsAvg = Simulation_Results.MSDFitResults.alphaVal_FitMeanByCondition;
           D_TimeEnsAvg = Simulation_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCondition;


%% Plot the time-ensemble-average next to the 95% CI of the ensemble-average

% Calculate the 95% confidence intervals for the ensemble-averaged MSD

    % Choose the dimension across which to calculate the MSD
        dim2CalcEnsAvgMSD = 3;
    % Choose the dimension across which to sum the MSD
        dim2SumMSD = 2;
    % Calculate the squared displacement (relative to the first timepoint)
    % for each particle and time point
        [SD] = calculateSDFromOrigin(Simulation_Results.trackPositions_nm,dim2CalcEnsAvgMSD,dim2SumMSD);
    % Preallocate space to store the MSD and 95% CIs
        MSD_Ens = nan([1,size(SD,2)]);
        MSD_CI = nan([2,size(SD,2)]);
    % Loop through each timepoint and run the analysis on the non-nan values
    for n = 1:size(SD,2)
    % Find the timepoint to calculate the confidence intervals
        CI_Idx = find(~isnan(SD(:,n))&(SD(:,n)>0));
        if length(CI_Idx)>100
            % Calculate the 95% confidence intervals about the mean using
            % bootstrapping
                [ci,bootstat] = bootci(500,{@mean,SD(CI_Idx,n)},'Alpha',0.05,'Type','per');
                MSD_CI(:,n) = ci;
            % Calculate the mean
                MSD_Ens(:,n) = mean(SD(CI_Idx,n));
        end
    end

% Plot the MSDs on top of each other

    % Pull up the figure
        figureNumMSD_ErgodicityTest = 1;
        figure(figureNumMSD_ErgodicityTest)

    % Plot the time-ensemble average
        % Choose the data color
            experimentColor = [0.1 0.1 0.1];
        % Plot the mean
            plot(timeInMS,MSD_TimeEnsAvg_mean,'','Color',experimentColor,'LineWidth',1)       
            hold on;       

    % Plot the ensemble average
        % Choose the data color
            experimentColor = [0.99 0.01 0.01];
        % Pull out the non-nan values
            idx2fit = ~isnan(MSD_Ens); 
        % Plot the standard deviation as shaded regions
            % Pull out the x-values
                timeVals = [timeInMS(idx2fit), fliplr(timeInMS(idx2fit))];
            % Pull out the y-values
                upperVals = MSD_CI(1,idx2fit);
                lowerVals = MSD_CI(2,idx2fit);
                shaddedRegion = [upperVals, fliplr(lowerVals)];
            % Plot the filled region
                fill(timeVals, shaddedRegion,experimentColor(1:3),'FaceAlpha',0.31,'EdgeAlpha',0);        
        % Plot the mean
            plot(timeInMS(idx2fit),MSD_Ens(idx2fit),'','Color',experimentColor,'LineWidth',1)  

    % Plot the relevant power law regimes
        timeInMS2Plot = [12 30];
        plot(timeInMS2Plot,1.3*10^3.*timeInMS2Plot.^1,'k--')        
        plot(timeInMS2Plot,4.5*10^3.*timeInMS2Plot.^0.8,'k-.')
        timeInMS2Plot = [50 150];
        plot(timeInMS2Plot,2.2*10^3.*timeInMS2Plot.^0.85,'k-')  
    % Clean up the plot
        xl = xlim();
        plot([xl(1) xl(2)],[0 0],'k')
        hold off;
        set(gca, 'YScale', 'log')
        set(gca, 'XScale', 'log')
        yticks([10^3 10^4 10^5 10^6])
        xticks([10^1 10^2 10^3])
        xlim([10^1 200])
        ylim([1.5*10^4 3*10^5])
        xlabel('Time offset (ms)')
        ylabel('MSD (nm^2)')
        title('TA vs TEA MSD')
        h4 = legend({'Time-ensemble average', sprintf(...
            'Best fit to power law \n D_{app} = %2.2g \\mu^2/s; \\alpha = %0.2f',...
            D_TimeEnsAvg/10^3,alpha_TimeEnsAvg),...
            'Ensemble average','95% CI',sprintf(...
            'Best fit to power law \n D_{app} = %2.2g \\mu^2/s; \\alpha = %0.2f',...
            D_EnsAvg/10^3,alpha_EnsAvg),...
            'Power law: \alpha = 1','Power law: \alpha = 0.8','Power law: \alpha = 0.85'},...
            'Location','Southeast','FontSize',5);
        set(gca,'FontName','Helvetica','FontSize',5);  
        
        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure4\Fig4c_HetSimsMSDAndErgodicity'];
            % Choose the figure size and position
                figHandle = figure(figureNumMSD_ErgodicityTest);
                figHandle.Position =  [100   500   145   135];
            % Save the file as a pdf
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])   


%% Calculate the nonergodicity

    % Calculate the percent difference between the MSDs for each time
    % offset
        percentDiff = (MSD_EnsAvg_mean-MSD_TimeEnsAvg_mean)./...
            MSD_TimeEnsAvg_mean;
    % Fit the data
        % Only plot the non-nan values less than 200 ms
            percentDiff2Fit = percentDiff((timeInMS<200)&(timeInMS>0));
            timeInMS2Fit = timeInMS((timeInMS<200)&(timeInMS>0));
        % Set up fittype and options
            ft = fittype( 'a*exp(-b*x)+c', 'independent', 'x', 'dependent', 'y' );
            opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
            opts.Algorithm = 'Levenberg-Marquardt';
            opts.Display = 'Off';
            opts.Robust = 'LAR';
            opts.StartPoint = [0.996463474528354 0.782663618937426 0.341247885874676];
            opts.Upper = [1 1 0.2];
            opts.Lower = [0 0 -0.2];
        % Fit model to data.
            [fitresult, gof] = fit( timeInMS2Fit', percentDiff2Fit', ft, opts );
        % Save the fit results
            fitresults = [fitresult.a,fitresult.b,fitresult.c, gof.sse];
    % Plot the figure
        figureNum_ErgodicityFit = 2;
        figure(figureNum_ErgodicityFit)
        % Plot the data
            plot(timeInMS,percentDiff*100,'-o','LineWidth',1,'color',[0.8500 0.3250 0.0980],...
                'MarkerFaceColor',[0.8500 0.3250 0.0980],'MarkerEdgeColor','none','MarkerSize',3)
            hold on;
        % Plot the fit result
            h = plot( timeInMS2Fit, fitresult(timeInMS2Fit)*100,'k--','LineWidth',1);
            hold off;
        % Clean up the plot
            ylabel({'Percent difference','EA vs TEA MSD'})
            ylim([0 0.5]*100)
            xlim([0 200])
            xlabel('Time offset (ms)')
            legend({'Data',sprintf('Best Fit \n %2.2f* \n exp(-%2.2f*x) \n +%2.2f',fitresult.a,fitresult.b,fitresult.c)})
            set(gca,'FontName','Helvetica','FontSize',5);  

        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure4\Fig4f_HetSimsErgodicity'];
            % Choose the figure size and position
                figHandle = figure(figureNum_ErgodicityFit);
                figHandle.Position =  [100   300   145   110];
            % Save the file as a pdf
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])   
