%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    % Close all figures
        close all;
        close all hidden; % ANOVA tables
    % Clear all variables
        clear all; 
        
% Choose the datasets to plot

    % Load the simulation data
        simulationsFolderPath = ['Garner_Molines_et_al\Simulation_Data\'...
            'ParameterScan_OptVarAndMean_BoxSize_Coarse_Fig4g\'];

    % Load example data
        % Pull out the Sim Idx
            simIdx2PullOut = [1 1 1 1 1];
        % Load an example file to get some information about the dataset
            % Choose example file
                folderInfo = dir(simulationsFolderPath);
                fileName = folderInfo(3).name;
                exampleFilePath = [simulationsFolderPath fileName];
        % Load the parameter information    
            load(exampleFilePath,'viscosityDomainSizeInNM2SimNums')
            load(exampleFilePath,'replicateNums')
            load(exampleFilePath,'fracCellVar2SimVals')
            load(exampleFilePath,'viscosityDomainSizeInNM2SimVals')            
            load(exampleFilePath,'numReplicates')

    % Choose the parameter indices for each simulation we want to pull out
        % cols: fracCellVar2SimNum fracSpaceVar2SimNum viscosityDomainSizeInNM2SimNum replicateNum
        % rows: no var; spatial var; cell var; both 
             simIdx2PullOutTotal  = [repmat(1:length(fracCellVar2SimVals),[3 numReplicates]);...
                repelem(1:numReplicates,length(fracCellVar2SimVals))];

% Load the experimental data    
   % Choose the path to the experimental data
        experimentalDatasetFilePath = ['C:\Users\Rikki Garner\Documents\PhD\'...
            'Research\Collaborations\Molines_Chang_Collaboration\'...
            'Experimental_Data_and_Analysis_Code\Data\'...
            'All_Controls_Combined_Analyzed.mat'];    
    % Load the results
        Experimental_Results = load(experimentalDatasetFilePath);

    % Reset the conditions to be all one condition
        Experimental_Results.conditionIDByCell = ...
            ones(size(Experimental_Results.conditionIDByCell));
        Experimental_Results.conditionIDByTrack = ...
            ones(size(Experimental_Results.conditionIDByTrack));
        Experimental_Results.numConditions = 1;

  
    % Perform the time-averaged MSD vs time
        [Experimental_Results] = ...
            performMSD_EnsembleAverageByTrackCellAndCondition(Experimental_Results);        

    % Pull out the time points to plot
        timeInMS = Experimental_Results.MSDResults.time_interval_for_MSD_ms;

    % Calculate the experimental non-ergodicity    
        % Pull out the MSDs
            % Ensemble average
                MSD_EnsAvg_mean = ...
                    Experimental_Results.MSDResults.MSD_nm2_EnsAvg';
            % Time-ensemble average
                MSD_TimeEnsAvg_mean = ...
                    Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(1,:);
        % Calculate the percent difference between the MSDs for each time
        % offset
            percentDiff = (MSD_EnsAvg_mean-MSD_TimeEnsAvg_mean)./...
                MSD_TimeEnsAvg_mean;
        % Fit the data
            % Only plot the non-nan values less than 200 ms
                percentDiff2Fit = percentDiff((timeInMS<200)&(timeInMS>0));
                timeInMS2Fit = timeInMS((timeInMS<200)&(timeInMS>0));
            % Set up fittype and options
                ft = fittype( 'a*exp(-b*x)+c', 'independent', 'x', 'dependent', 'y' );
                opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
                opts.Algorithm = 'Trust-Region';
                opts.Display = 'Off';
                opts.Robust = 'LAR';
                opts.StartPoint = [0.996463474528354 0.782663618937426 0.341247885874676];
                opts.Upper = [1 1 0.2];
                opts.Lower = [0 0 -0.2];
            % Fit model to data.
                [fitresult, gof] = fit( timeInMS2Fit', percentDiff2Fit', ft, opts );
            % Save the fit results
                fitresults = [fitresult.a,fitresult.b,fitresult.c, gof.sse];


%% Calculate ergodicitiy metric

% Calculate the number of different spatial domains simulated
    numBoxSizes = max(viscosityDomainSizeInNM2SimNums);
% Pull out the time points to plot
    numTimePoints = length(timeInMS);

% Pre-allocate space to store the percent difference between the TEA and EA
% MSD
    percentDiff_All = nan([numBoxSizes,numTimePoints,numReplicates]);

% Pre-allocate space to store the fit results for the non-ergodicity
% metric
     fitresults_All = nan([numBoxSizes,4,numReplicates]);


for boxSizeNum = 1:numBoxSizes

    % Print the box size
        boxSizeNum

    for repNum = 1:numReplicates

        % Pull out the Sim Idx
            simIdx2PullOut = [boxSizeNum boxSizeNum boxSizeNum boxSizeNum repNum];
        % Load an example file to get some information about the dataset
            % Choose example file
                folderInfo = dir(simulationsFolderPath);
                fileName = folderInfo(3).name;
                exampleFilePath = [simulationsFolderPath fileName];
            % Load the parameter information    
                load(exampleFilePath,'fracCellVar2SimNums')
                load(exampleFilePath,'fracSpaceVar2SimNums')
                load(exampleFilePath,'viscosityDomainSizeInNM2SimNums')
                load(exampleFilePath,'meanViscosity2SimNums')
                load(exampleFilePath,'replicateNums')
                load(exampleFilePath,'ParamScanName')
                load(exampleFilePath,'dateNum') 
            % Find which simulation we want
                sumNum = find((fracCellVar2SimNums==simIdx2PullOut(1))&...
                    (fracSpaceVar2SimNums==simIdx2PullOut(2))&...
                    (viscosityDomainSizeInNM2SimNums==simIdx2PullOut(3))&...
                    (meanViscosity2SimNums==simIdx2PullOut(4))&...
                    (replicateNums==simIdx2PullOut(5)));
                % Determine the filename
                    fileName = sprintf('Results_ExpStatsAndLength_%s_%s_%i.mat',...
                        ParamScanName,dateNum,sumNum);
                    filePath = [simulationsFolderPath fileName]; 

       % Re-analyze the data

            % Load the dataset
                Simulation_Results = load(filePath);
    
            % Reset the conditions to be all one condition
                Simulation_Results.conditionIDByCell = ...
                    ones(size(Simulation_Results.conditionIDByCell));
                Simulation_Results.conditionIDByTrack = ...
                    ones(size(Simulation_Results.conditionIDByTrack));
                Simulation_Results.numConditions = 1;
            
            % Perform the time-averaged and ensemble-average MSD vs time
                [Simulation_Results] = ...
                    performMSD_EnsembleAverageByTrackCellAndCondition(Simulation_Results);
    
    
        % Pull out the MSD data
            % Pull out the time points to plot
                timeInMS = Simulation_Results.MSDResults.time_interval_for_MSD_ms;
            % Time used to calculate the apparent diffusivity
                timeIntervalForDApp_ms = Simulation_Results.MSDFitResults.timeIntervalForDApp_ms;
            % Pull out the MSDs
                % Ensemble average
                    MSD_EnsAvg_mean = ...
                        Simulation_Results.MSDResults.MSD_nm2_EnsAvg';
                    MSD_EnsAvg_se = ...
                        Simulation_Results.MSDResults.SESD_nm2_EnsAvg';
                % Time-ensemble average
                    MSD_TimeEnsAvg_mean = ...
                        Simulation_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(1,:);
                    MSD_TimeEnsAvg_se = ...
                        Simulation_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.semean(1,:);
        
                
        % Store the percent difference between the TEA and EA MSD 
            % Calculate the difference
                diff = (MSD_EnsAvg_mean-MSD_TimeEnsAvg_mean);
            % Calculate the standard error of the differnce
                seDiff = propagate_error_addition_noCovariance(...
                    MSD_EnsAvg_mean,MSD_EnsAvg_se,...
                    MSD_TimeEnsAvg_mean,MSD_TimeEnsAvg_se);
            % Calculate the percent difference
                percentDiff = diff./MSD_TimeEnsAvg_mean;
            % Calculate the standard error in the percent difference
                sePercentDiff = propagate_error_division_noCovariance(...
                    diff,seDiff,MSD_TimeEnsAvg_mean,MSD_TimeEnsAvg_se);
            % Record the percent difference
                percentDiff_All(boxSizeNum,:,repNum) = percentDiff;

        % Fit the data
            % Only plot the non-nan values less than 200 ms
                percentDiff2Fit = percentDiff((timeInMS<200)&(timeInMS>0));
                sePercentDiff2Fit = sePercentDiff((timeInMS<200)&(timeInMS>0));
                timeInMS2Fit = timeInMS((timeInMS<200)&(timeInMS>0));
            % Set up fittype and options
                ft = fittype( 'a*exp(-b*x)+c', 'independent', 'x', 'dependent', 'y' );
                opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
                opts.Algorithm = 'Trust-Region';
                opts.Display = 'Off';
                opts.Robust = 'LAR';
                opts.StartPoint = [0.996463474528354 0.782663618937426 0.341247885874676];
                opts.Weights = (1./sePercentDiff2Fit);
                opts.Upper = [1 1 0.2];
                opts.Lower = [0 0 -0.2];
            % Fit model to data
                [fitresultTemp, gofTemp] = fit(timeInMS2Fit',percentDiff2Fit',ft,opts);
            % Save the fit results
                fitresults_All(boxSizeNum,:,repNum) = [fitresultTemp.a,fitresultTemp.b,fitresultTemp.c, gofTemp.sse];

    end
end

%% Calculate the summary statistics
    % Choose the dimension to average over
        dim2AverageOver = 3;
    % Choose the conditions for each replicate
        conditionIDByReplicate = ones([numReplicates, 1]);
    % Calculate the summary statistics 
        [ErgodicityResults] = ...
            calculateSummaryStatsByConditionErgodicity(percentDiff_All,...
            dim2AverageOver,conditionIDByReplicate);
        [EgodicityFitResults] = ...
            calculateSummaryStatsByConditionErgodicity(fitresults_All,...
            dim2AverageOver,conditionIDByReplicate);

%% Plot the results

% Pull up the figure
    figure(10)
% Choose the color of the plot
    experimentColor = [0.1 0.1 0.9 0.04];
    simulationColor = [0.8500 0.3250 0.0980];

% Loop through each spatial domain and plot each replicate and the mean +- se
for boxSizeNum = 1:numBoxSizes

    % Pull up the subplot
        subplot(1,numBoxSizes,boxSizeNum)
    % Plot the value for each replicate
    for repNum = 1:numReplicates

        % Plot the mean percent difference in the non-erodicity metric
            plot(timeInMS, squeeze(percentDiff_All(boxSizeNum,:,repNum))*100,...
                '-','Color',[simulationColor 0.1]);
            hold on;
    end

    % Plot the experimental best fit result
        plot(timeInMS2Fit, fitresult(timeInMS2Fit)*100,'--',...
            'Color',experimentColor(1:3),'LineWidth',1);
    % Plot the best fit across all simulations
        plot( timeInMS2Fit, ((EgodicityFitResults.median(boxSizeNum,1).*...
            exp(-EgodicityFitResults.median(boxSizeNum,2).*timeInMS2Fit)) + ...
            EgodicityFitResults.median(boxSizeNum,3))*100,'--',...
            'Color',simulationColor(1:3),'LineWidth',1);
    % Clean up the plot
        hold off;
        xlabel('Time offset (ms)')
        ylabel({'Percent difference','EA vs TEA MSD'})
        title(sprintf('%d nm domain size',viscosityDomainSizeInNM2SimVals(boxSizeNum)))
        ylim([0 0.5]*100)
        xlim([0 200])
        set(gca,'FontName','Helvetica','FontSize',5);  

end

        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure4\Fig4g_HetSimsErgodicity_BoxSize'];
            % Choose the figure size and position
                figHandle = figure(10);
                figHandle.Position =  [100   615   660   105];
            % Save the file as a pdf
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save as a png                               
                print(gcf,destinationTrack,'-dpng',['-r' num2str(600)])
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])    
