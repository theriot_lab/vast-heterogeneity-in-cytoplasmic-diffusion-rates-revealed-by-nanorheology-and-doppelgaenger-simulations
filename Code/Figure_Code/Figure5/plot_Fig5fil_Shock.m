%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022

% Clear the system
    clear all;
    close all;
    
% Choose and load the comparisons to be analyzed and plotted    

    % Choose the file path to the data containing the combined Simulational and simulation
    % results
        filePath = ['Garner_Molines_et_al\'...
            'Experimental_Data\Analyzed_Data\'...
            'All_Conditions_sorbitol_20210726_Analyzed.mat'];

    % Load the data
        load(filePath)

    % Load and print the treatment names
        groupNames = uniqueTreatments;
        groupIDsByTrack = treatmentIDByTrack;
        groupIDsByCell = treatmentIDByCell;

    % Choose the group names for plotting (by looking at the names)
        groupNames2Plot = {'1.5M', '1M','0M'};

    % Choose the control treatment groups (by looking at the names)
        controlGroupNum = 3;
        perturbationGroupNums = [2 1];

    % Count the number of perturbations
        numPerturbations = length(perturbationGroupNums);
        numGroups = numPerturbations + 1;

% Calculate p values for test of equality of means/variances by track

    % Preallocate space to store the p values and stats
        pD_sigma_ByTrack = nan(numPerturbations,1);
        statsD_sigma_ByTrack = cell([numPerturbations,1]);
        pD_mu_ByTrack = nan(numPerturbations,1);
        tblD_mu_ByTrack = cell([numPerturbations,1]);
        statsD_mu_ByTrack = cell([numPerturbations,1]);
        pD_sigma_ByCell = nan(numPerturbations,1);
        statsD_sigma_ByCell = cell([numPerturbations,1]);
        pD_mu_ByCell = nan(numPerturbations,1);
        tblD_mu_ByCell = cell([numPerturbations,1]);
        statsD_mu_ByCell = cell([numPerturbations,1]);

    for perturbationGroupNum = 1:numPerturbations

        % Perform the analysis on the track-wise data

            % Pull out the data to analyze
                tracks2Analyze = ((groupIDsByTrack==controlGroupNum)|...
                    groupIDsByTrack==perturbationGroupNums(perturbationGroupNum));
                log10D2Analyze = log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack(tracks2Analyze));
                groupIDs2Analyze = groupIDsByTrack(tracks2Analyze);
    
            % Perform a nonparametric test of equal variance
                % Diffusivity
                    [pD_sigma_ByTrack(perturbationGroupNum),...
                        statsD_sigma_ByTrack{perturbationGroupNum}] = ...
                            vartestn(log10D2Analyze,groupIDs2Analyze,...
                            'Display','on','TestType','LeveneAbsolute');       
                
            % Perform a nonparametric test of equal medians % distributions
                % Diffusivity
                    [pD_mu_ByTrack(perturbationGroupNum),...
                        tblD_mu_ByTrack{perturbationGroupNum},...
                        statsD_mu_ByTrack{perturbationGroupNum}] = ...
                            ranksum(log10D2Analyze(groupIDs2Analyze==controlGroupNum),...
                                log10D2Analyze(groupIDs2Analyze==...
                                perturbationGroupNums(perturbationGroupNum)));       
          
        % Perform the analysis on the cell-wise data

            % Pull out the data to analyze
                tracks2Analyze = ((groupIDsByCell==controlGroupNum)|...
                    groupIDsByCell==perturbationGroupNums(perturbationGroupNum));
                log10D2Analyze = log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell(tracks2Analyze));
                groupIDs2Analyze = groupIDsByCell(tracks2Analyze);
    
            % Perform a nonparametric test of equal variance
                % Diffusivity
                    [pD_sigma_ByCell(perturbationGroupNum),...
                        statsD_sigma_ByCell{perturbationGroupNum}] = ...
                            vartestn(log10D2Analyze,groupIDs2Analyze,...
                            'Display','on','TestType','LeveneAbsolute');       
      
            % Perform a nonparametric test of equal medians % distributions
                % Diffusivity
                    [pD_mu_ByCell(perturbationGroupNum),...
                        tblD_mu_ByCell{perturbationGroupNum},...
                        statsD_mu_ByCell{perturbationGroupNum}] = ...
                            ranksum(log10D2Analyze(groupIDs2Analyze==controlGroupNum),...
                                log10D2Analyze(groupIDs2Analyze==...
                                perturbationGroupNums(perturbationGroupNum)));       
          
    end
   
    close all;
    close all hidden; % Close stubborn tables and graphs


%% Make box plots with significance stars

    % Reformat the data for the box plots
        % Determine the maximum number of tracks on each group
            uniqueGroupIDs = unique(groupIDsByTrack); 
            numTracksPerGroup = histc(groupIDsByTrack,uniqueGroupIDs);
            numCellsPerGroup = histc(groupIDsByCell,uniqueGroupIDs);
        % Pre-allocate space to store the data
            % Reformatted matrices
                D2Plot_ByTrack = nan(max(numTracksPerGroup),1,numGroups);
                D2Plot_ByCell = nan(max(numCellsPerGroup),1,numGroups);
            % Means and variances 
                D2Plot_ByTrack_numPoints = nan(1,numGroups);           
                D2Plot_ByTrack_median = nan(1,numGroups);
                D2Plot_ByTrack_sdmedian = nan(1,numGroups);
                D2Plot_ByCell_numPoints = nan(1,numGroups);  
                D2Plot_ByCell_median = nan(1,numGroups);
                D2Plot_ByCell_sdmedian = nan(1,numGroups);
    
    % Load the track data into a single matrix
    for groupNum = 1:numGroups

        % Track-wise data
            % Pull out the data to plot
                tracks2Analyze = (groupIDsByTrack==groupNum);
                log10D2Analyze = log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack(tracks2Analyze));
            % Fill in the matrix
                D2Plot_ByTrack(1:numTracksPerGroup(groupNum),1,groupNum) = log10D2Analyze;
            % Calculate summary statistics
                % Count the number of tracks
                    D2Plot_ByTrack_numPoints(groupNum) = sum(~isnan(log10D2Analyze));
                % Record summary statistics for the log10(diffusivity)
                    % Calculate the median
                        D2Plot_ByTrack_median(groupNum) = nanmedian(log10D2Analyze);
                    % Calculate the standard deviation of the median
                        D2Plot_ByTrack_sdmedian(groupNum) = sqrt(nansum((log10D2Analyze - ...
                            D2Plot_ByTrack_median(groupNum)).^2,1)./D2Plot_ByTrack_numPoints(groupNum));
          
        % Cell-wise data
            % Pull out the data to plot
                tracks2Analyze = (groupIDsByCell==groupNum);
                log10D2Analyze = log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell(tracks2Analyze));
            % Fill in the matrix
                D2Plot_ByCell(1:numCellsPerGroup(groupNum),1,groupNum) = log10D2Analyze;
            % Calculate summary statistics
                % Count the number of tracks
                    D2Plot_ByCell_numPoints(groupNum) = sum(~isnan(log10D2Analyze));
                % Record summary statistics for the log10(diffusivity)
                    % Calculate the median
                        D2Plot_ByCell_median(groupNum) = nanmedian(log10D2Analyze);
                    % Calculate the standard deviation of the median
                        D2Plot_ByCell_sdmedian(groupNum) = sqrt(nansum((log10D2Analyze - ...
                            D2Plot_ByCell_median(groupNum)).^2,1)./D2Plot_ByCell_numPoints(groupNum));
    end   

    % Choose the plotting parameters
        % Choose the plot locations
            figureNumBoxPlotsVariances = 1;
            numCols = 2;
            numRows = 1;
            numSubplotDByTrack = 1;
            numSubplotDByCell = 2;
        % Subset the temperature data only
            groupOrder2Plot = [controlGroupNum perturbationGroupNums];
            groupNames(groupOrder2Plot)
            comparisonOrder2Plot = 1:numPerturbations;
        % Choose which color to plot the conditions    
            CMByGroup = jet(numGroups)*0.75;
            CMByGroup(1,:) = [0.37 0.42 1]; 
            CMByGroup(2,:) = CMByGroup(3,:)*0.75;
            CMByGroup(3,:) = [0 0.9 0]*0.5;
        % Choose the dot size
            dotSizeTrack = 5;
            dotSizeCell = 5;
        % Choose the alpha values
            alphaValsTracks = 0.1*ones(size(groupOrder2Plot));
            alphaValsCells = 0.9*ones(size(groupOrder2Plot));

    % Calculate summary stats
        % Convert the median diffusivities to real space, and then calculate the percent change
            PercentChangeInLogDMean_ByTrack = ...
                (10.^D2Plot_ByTrack_median(perturbationGroupNums)-...
                10.^D2Plot_ByTrack_median(controlGroupNum)).*100./...
                10.^D2Plot_ByTrack_median(controlGroupNum)

    % Make box plots with significance stars for the test of equal variance 

        % Plot the track-wise diffusivity, where stars are test of equal variance    
            [hBoxPlot_DByTrack, hSigStars_DByTrack] = ...
                plotBoxPlotsWithSigStarsDiffusivity(figureNumBoxPlotsVariances,numRows,numCols,...
                    numSubplotDByTrack,D2Plot_ByTrack(:,:,groupOrder2Plot),...
                    pD_sigma_ByTrack(comparisonOrder2Plot),CMByGroup,...
                    groupNames2Plot(groupOrder2Plot),dotSizeTrack,alphaValsTracks);
            box on;
            yticks(0:5)
            title({'Distribution of diffusivities','from track-wise MSD fits'})
            set(gca,'Xlim',[0.6 1.4])
            set(gca,'FontName','Helvetica','FontSize',5);  

        % Plot the cell-wise diffusivity, where stars are test of equal variance    
            [hBoxPlot_DByCell, hSigStars_DByCell] = ...
                plotBoxPlotsWithSigStarsDiffusivity(figureNumBoxPlotsVariances,numRows,numCols,...
                    numSubplotDByCell,D2Plot_ByCell(:,:,groupOrder2Plot),...
                    pD_sigma_ByCell(comparisonOrder2Plot),CMByGroup,...
                    groupNames2Plot(groupOrder2Plot),dotSizeCell,alphaValsCells);
            box on;
            yticks(0:5)
            title({'Distribution of diffusivities','from cell-wise MSD fits'})
            set(gca,'Xlim',[0.6 1.4])
            set(gca,'FontName','Helvetica','FontSize',5);   
                
        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure5\Fig5il_Shock_BoxPlots_StarsAreVariance'];
            % Choose the figure size and position
                figHandle = figure(figureNumBoxPlotsVariances);
                figHandle.Position =  [100   430   310   145];
            % Save the file as a pdf
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])           

 
%% Make a plot of the means 
    figure(3)
    % Make the bar plots
        medianD = 10.^nanmedian(squeeze(D2Plot_ByTrack(:,:,groupOrder2Plot)),1);
        numD = sum(~isnan(squeeze(D2Plot_ByTrack(:,:,groupOrder2Plot))),1);
        stdD = sqrt(nansum((squeeze(D2Plot_ByTrack(:,:,groupOrder2Plot)) - medianD).^2,1)./numD);
        seD = stdD./sqrt(numD);
        b = bar(medianD);
        b.FaceColor = 'flat';
        b.CData = CMByGroup;
        hold on;
        errorbar(medianD,seD,'k.')
        set(gca,'XTickLabel',groupNames2Plot(groupOrder2Plot))
        yticks(0:100:500)
        % Choose the x axes limits
            spaceBetweenBars = mean(diff(b.XData));
            xlim([(min(b.XData) - (spaceBetweenBars*3/4)) ...
                (max(b.XData) + (spaceBetweenBars*3/4))]);
        title({'Track-averaged diffusivity'})
        ylabel({'Diffusivity (D, nm^2/ms)','Median \pm SE Median'})
        ylim([0 450])
        set(gca,'FontName','Helvetica','FontSize',5);     
    % Plot the significance stars
        % Calculate the locations for the bars and sig stars    
            [groups2Compare] = [ones(numPerturbations,1), ((1:numPerturbations)'+1)];
        % Plot the sig stars
            hSigStars = sigstar(num2cell(groups2Compare,2),pD_mu_ByTrack);
            hold off;
    
    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\Figure5\Fig5f_Shock_BarPlots'];
        % Choose the figure size and position
            figHandle = figure(3);
            figHandle.Position =  [100   640   165   110];
        % Save the file as a pdf
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])


