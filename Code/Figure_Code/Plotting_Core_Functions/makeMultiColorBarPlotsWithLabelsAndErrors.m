function [] = makeMultiColorBarPlotsWithLabelsAndErrors(figureNum,numRows,...
    numCols,subplotNum,barHeights,barNames,barColors)

    % Open the figure and subplot
        figure(figureNum)
        subplot(numRows,numCols,subplotNum)
    % Make the plot    
        bD = bar(1,barHeights);
    % Color the bars
        numBars = length(barHeights);
        for numBar = 1:numBars
            bD(numBar).FaceColor = barColors(numBar,:);
        end
    % Label the bars
        % Get group centers
            xCnt = get(bD(1),'XData') + cell2mat(get(bD,'XOffset')); % XOffset is undocumented!
        % Set individual ticks
            set(gca, 'XTick', sort(xCnt(:)), 'XTickLabel', barNames)
    % Label the bar heights
        for n = 1:length(bD)
            xtips2 = bD(n).XEndPoints;
            ytips2 = bD(n).YEndPoints;
            labels2 = sprintf('%1.2f',bD(n).YData);
            text(xtips2,ytips2,labels2,'HorizontalAlignment','center',...
                'VerticalAlignment','bottom','FontSize',5)
        end
