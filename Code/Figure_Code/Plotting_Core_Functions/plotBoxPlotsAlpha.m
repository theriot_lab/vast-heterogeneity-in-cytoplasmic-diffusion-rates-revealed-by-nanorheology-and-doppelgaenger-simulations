
function [hBoxPlot] = plotBoxPlotsAlpha(figureNum,numRows,...
    numCols,numSubplot,dataVals,CMByGroup,labelByGroup,dotSize)

    % Open the figure
        figure(figureNum)
        subplot(numRows,numCols,numSubplot,'replace')

    % Make the box plots
        hBoxPlot = iosr.statistics.boxPlot({''},dataVals,'showScatter',true,...
            'scattercolor',num2cell(CMByGroup,2),...
            'scattermarker','o','scatterSize',dotSize,'showMean',true,...
            'meanMarker','*','meanColor','k','medianColor','k','symbolMarker',...
            'o','outlierSize',dotSize,...
            'groupLabels',labelByGroup,'symbolColor',...
            num2cell(CMByGroup,2),'style','hierarchy');
    % Set the y-limit before adding stars
        ylim([-3 3])

    % Update group labels
        for numObj = 1:(length(hBoxPlot.handles.groupsTxt)-1)
          %  set(hBoxPlot.handles.groupsTxt(numObj),'Rotation',10)
            set(hBoxPlot.handles.groupsTxt(numObj),'FontSize',5)
            set(hBoxPlot.handles.groupsTxt(numObj),'String',labelByGroup(numObj))
            pos = get(hBoxPlot.handles.groupsTxt(numObj),'Position');
            pos(2) = -3.35;
            set(hBoxPlot.handles.groupsTxt(numObj),'Position',pos);
        end

    % Clean up the plot        
        title({'Distribution of power law exponents','from track-wise fits'})
        ylabel({'Power law exponent','(\alpha, unitless)'})
        ylim([-1.1 2.1])
        set(gca,'FontName','Helvetica','FontSize',5); 