
function [hBoxPlot] = plotBoxPlotsDiffusivity(figureNum,numRows,...
    numCols,numSubplot,dataVals,CMByGroup,labelByGroup,dotSize)

% DO NOT UPDATE YLIM AFTER CALLING THIS FUNCTION

    % Open the figure
        figure(figureNum)
        subplot(numRows,numCols,numSubplot,'replace')

    % Make the box plots
        hBoxPlot = iosr.statistics.boxPlot({''},dataVals,'showScatter',true,...
            'scattercolor',num2cell(CMByGroup,2),...
            'scattermarker','o','scatterSize',dotSize,'scatterAlpha',0.01,'showMean',true,...
            'meanMarker','*','meanColor','k','medianColor','k','symbolMarker',...
            'o','outlierSize',dotSize,...
            'groupLabels',labelByGroup,'symbolColor',...
            num2cell(CMByGroup,2),'style','hierarchy');
    % Set the y-limit before adding stars
        ylim(gca, [-1 5.5])  

    % Update group labels
        for numObj = 1:(length(hBoxPlot.handles.groupsTxt)-1)
           % set(hBoxPlot.handles.groupsTxt(numObj),'Rotation',10)
            set(hBoxPlot.handles.groupsTxt(numObj),'FontSize',5)
            set(hBoxPlot.handles.groupsTxt(numObj),'String',labelByGroup(numObj))
            pos = get(hBoxPlot.handles.groupsTxt(numObj),'Position');
            pos(2) = -3.35;
            set(hBoxPlot.handles.groupsTxt(numObj),'Position',pos);
        end

    % Clean up the plots
        title({'Distribution of diffusivities'})
        ylabel({'log_{10}(Diffusivity)','(D_{app, 100ms}, nm^2/ms)'})
        set(gca,'FontName','Helvetica','FontSize',5);    
        ylim([-1 4.2])
