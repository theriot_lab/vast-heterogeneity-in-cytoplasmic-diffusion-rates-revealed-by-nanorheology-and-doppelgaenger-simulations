
function [hBoxPlot, hSigStars] = plotBoxPlotsWithSigStarsDiffusivityByTrack(figureNum,numRows,...
    numCols,numSubplot,dataVals,pVals,CMByGroup,labelByGroup,dotSize,alphaVals)

% DO NOT UPDATE YLIM AFTER CALLING THIS FUNCTION

    % Open the figure
        figure(figureNum)
        subplot(numRows,numCols,numSubplot,'replace')

    % Make the box plots
        hBoxPlot = iosr.statistics.boxPlot({''},dataVals,'showScatter',true,...
            'scattercolor',num2cell(CMByGroup,2),...
            'scattermarker','o','scatterSize',dotSize,'showMean',true,...
            'meanMarker','*','meanColor','k','medianColor','k','symbolMarker',...
            'o','outlierSize',dotSize,...
            'groupLabels',labelByGroup,'symbolColor',...
            num2cell(CMByGroup,2),'style','hierarchy');
        % Make them closed circles
        for n = 1:length(hBoxPlot.handles.scatters)
            if isprop(hBoxPlot.handles.scatters(n),'XData')
            hBoxPlot.handles.scatters(n).MarkerFaceColor = hBoxPlot.handles.scatters(n).MarkerEdgeColor;
            hBoxPlot.handles.scatters(n).MarkerFaceAlpha = hBoxPlot.handles.scatters(n).MarkerEdgeAlpha;
            hBoxPlot.handles.scatters(n).MarkerFaceAlpha = alphaVals(n);
            hBoxPlot.handles.scatters(n).MarkerEdgeColor = 'none';
            end
        end   
        for n = 1:length(hBoxPlot.handles.outliers)
            if isprop(hBoxPlot.handles.outliers(n),'XData')
            hBoxPlot.handles.outliers(n).MarkerFaceColor = hBoxPlot.handles.outliers(n).MarkerEdgeColor;
            hBoxPlot.handles.outliers(n).MarkerFaceAlpha = hBoxPlot.handles.outliers(n).MarkerEdgeAlpha;
            hBoxPlot.handles.outliers(n).MarkerFaceAlpha = alphaVals(n);
            hBoxPlot.handles.outliers(n).MarkerEdgeColor = 'none';
            end
        end   
    % Set the y-limit before adding stars
        ylim([0 4.5])

    % Plot the significance stars
        % Calculate the locations for the bars and sig stars
            % Calculate the number of groups
                numGroups = size(dataVals,3);
            % Calculate the number of comparisons
                numComparisons = numGroups-1;     
            % Create a matrix of every comparison made
                [groups2Compare] = [ones(numGroups-1,1), (2:numGroups)'];
            % Calculate the locations
                if numComparisons == 5
                    locations = num2cell(((groups2Compare-4)./8)+1.05,2);
                elseif numComparisons == 4
                    locations = num2cell(((groups2Compare-3)./7)+1,2);
                elseif numComparisons == 3
                    locations = num2cell(((groups2Compare-3)./5)+1.1,2);
                elseif numComparisons == 2
                    locations = num2cell(((groups2Compare-2)./4)+1,2);
                elseif numComparisons == 1
                    locations = num2cell(((groups2Compare-1.5).*2./5.5)+1,2);
                end
        % Plot the sig stars
            hSigStars = sigstar(locations,pVals);

    % Update group labels
        for numObj = 1:(length(hBoxPlot.handles.groupsTxt)-1)
            % set(hBoxPlot.handles.groupsTxt(numObj),'Rotation',10)
            set(hBoxPlot.handles.groupsTxt(numObj),'FontSize',5)
            set(hBoxPlot.handles.groupsTxt(numObj),'String',labelByGroup(numObj))
            pos = get(hBoxPlot.handles.groupsTxt(numObj),'Position');
            pos(2) = -1;
            set(hBoxPlot.handles.groupsTxt(numObj),'Position',pos);
        end

    % Clean up the plot
        title({'Distribution of diffusivities'})
        ylabel({'log_{10}(Diffusivity)','(D_{app, 100ms}, nm^2/ms)'})
        set(gca,'FontName','Helvetica','FontSize',5);    
