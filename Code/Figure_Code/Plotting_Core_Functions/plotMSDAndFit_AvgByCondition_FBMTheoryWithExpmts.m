% This script plots ensemble-averaged, time-averaged MSD vs Time for each 
% dimension separately in addition to the total, and is run by the function
% analyzeGEMTracks_OneExampleFile.m
% This function was written and developed by Rikki M. Garner and was last
% updated on 2021/06/15.

function plotMSDAndFit_AvgByCondition_FBMTheoryWithExpmts(dataStructureExpmts,dataStructureSims,datasetInfo,numRows,numCols,...
    figureNumMSD,numSubplotMean)

% Unpack the structure              
    v2struct(dataStructureSims);

% Choose the legendText
    legendTextMean = {};
    legendTextMedian = {};

for conditionNum = 1:length(datasetInfo.condition2plot)   
    
    % Pull out the condition to plot
        conditionNum2Plot = datasetInfo.condition2plot(conditionNum);
    
    figure(figureNumMSD)
    % Plot the mean and se of the mean
        subplot(numRows,numCols,numSubplotMean)
        % Experiments
            % Plot the mean
                plot(dataStructureExpmts.MSDResults.time_interval_for_MSD_ms,...
                    dataStructureExpmts.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(conditionNum2Plot,:),...
                    '','Color','m','LineWidth',1)
                hold on;
            % Plot the SE of the mean as error bars
                EB = errorbar(dataStructureExpmts.MSDResults.time_interval_for_MSD_ms,...
                    dataStructureExpmts.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(conditionNum2Plot,:)',...
                    dataStructureExpmts.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.semean(conditionNum2Plot,:)',...
                    'r','LineStyle', 'none','Color','m');
                    set([EB.Bar, EB.Line], 'ColorType', 'truecoloralpha', ...
                        'ColorData', [EB.Line.ColorData(1:3); 255*0.3])
                    set(EB.Cap, 'EdgeColorType', 'truecoloralpha', ...
                        'EdgeColorData', [EB.Cap.EdgeColorData(1:3); 255*0.3])  
                    set(EB,'HandleVisibility', 'off')
                    EB.Annotation.LegendInformation.IconDisplayStyle = 'off';
        % Simulations
            % Plot the mean
                plot(MSDResults.time_interval_for_MSD_ms,...
                    MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(conditionNum2Plot,:),...
                    '','Color',datasetInfo.CMByCondition(conditionNum,:))
                hold on;
            % Plot the SE of the mean as error bars
                EB = errorbar(MSDResults.time_interval_for_MSD_ms,...
                    MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(conditionNum2Plot,:)',...
                    MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.semean(conditionNum2Plot,:)',...
                    'r','LineStyle', 'none','Color',datasetInfo.CMByCondition(conditionNum,:));
                    set([EB.Bar, EB.Line], 'ColorType', 'truecoloralpha', ...
                        'ColorData', [EB.Line.ColorData(1:3); 255*0.3])
                    set(EB.Cap, 'EdgeColorType', 'truecoloralpha', ...
                        'EdgeColorData', [EB.Cap.EdgeColorData(1:3); 255*0.3])  
                    set(EB,'HandleVisibility', 'off')
                    EB.Annotation.LegendInformation.IconDisplayStyle = 'off';
%         % Plot the best fit
%             loglog(MSDFitResults.time_interval_for_MSD_ms,...
%                 MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCondition(conditionNum2Plot).*...
%                 (2*2*MSDFitResults.timeIntervalForDApp_ms.^...
%                 (1-MSDFitResults.alphaVal_FitMeanByCondition(conditionNum2Plot)))*...
%                 MSDFitResults.time_interval_for_MSD_ms.^...
%                 MSDFitResults.alphaVal_FitMeanByCondition(conditionNum2Plot),...
%                 '--','Color',datasetInfo.CMByCondition(conditionNum,:));
         % Plot the theory
            loglog(MSDFitResults.time_interval_for_MSD_ms,...
                2*numDimensions*D*MSDFitResults.time_interval_for_MSD_ms.^alphaVal,...
                'k-.')%,'Color',datasetInfo.CMByCondition(conditionNum,:));
               
    % Update the legend text
        legendTextMean = [legendTextMean {['Expmt. data']}];
        legendTextMean = [legendTextMean {[sprintf('FBM Sim. data \n (with expmt. stats.)')]}];
%         legendTextMean = [legendTextMean ...
%             {sprintf('Best fit to power law MSD = %2.2g\\tau^{%0.2f}',...
%             MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCondition(conditionNum2Plot),...
%             MSDFitResults.alphaVal_FitMeanByCondition(conditionNum2Plot))}];
        legendTextMean = [legendTextMean ...
            {sprintf('FBM analytical theory \n D_{app} = %2.2g \\mu^2/s; \n \\alpha = %0.2f',...
            D/(10^3*Experimental_Results.MSDFitResults.timeIntervalForDApp_ms.^...
        (1-Experimental_Results.MSDFitResults.alphaVal_FitMeanByCondition(conditionNum2Plot))),alphaVal)}]; 

end
    
% Clean up the mean plot
    figure(figureNumMSD)
    subplot(numRows,numCols,numSubplotMean)
    xl = xlim();
    plot([xl(1) xl(2)],[0 0],'k')
    hold off;
    xlabel('Time offset (ms)')
    ylabel('MSD (nm^2)')
    title({'Time-averaged, ensemble(track)-averaged MSD', datasetInfo.datasetName})
    title({sprintf('Mean-squared \n displacement (MSD)'),'(time-avg, track-avg)',datasetInfo.datasetName})
    set(gca,'FontName','Helvetica','FontSize',6); 
    legend(legendTextMean,'Location','Northwest','FontSize',5)
  
% Link the axes
    figure(figureNumMSD)
    % Define the axes
        % All plots
            rows = 1:numRows;
            cols = 1:numCols;
            numPlots=0;
            hAxMSD=[];
            for  rowNum = rows
                for colNum = cols
                    numPlots=numPlots+1;
                    subplotNum = colNum + (numCols*(rowNum-1));
                    hAxMSD(numPlots) = subplot(numRows,numCols,subplotNum);
                end
            end
    % Perform the link    
        link1 = linkprop(hAxMSD,{'XScale','XLim','YScale','YLim'});
    % Update the axes
        subplot(numRows,numCols,1)
%         ylim([min(MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(...
%             MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(:)>0))...
%             max(MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(:)*2)])
        xlim([10^1 10^3])
        ylim([10^4 2*10^6])
        set(hAxMSD, 'YScale', 'log')
        set(hAxMSD, 'XScale', 'log')

    
    