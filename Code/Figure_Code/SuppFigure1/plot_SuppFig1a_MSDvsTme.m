%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    % Close all figures
        close all;
        close all hidden; % ANOVA tables
    % Clear all variables
        clear all; 
    
% Choose the dataset to plot
    experimentalDatasetFilePath = ['Garner_Molines_et_al\Experimental_Data\'...
        'Analyzed_Data\All_Controls_Combined_Analyzed.mat'];
    Experimental_Results = load(experimentalDatasetFilePath);

% Choose whether to save the images
    doSaveImages = true;  
   
% Choose where to make the plot
    % Choose the figure number
        figureNumMSDByCondition = 10;
    % Choose the condition numbers
        conditionNum2Plot = 1;
    % Choose the data color
        alphaVal = 0.3;
        experimentColor = [0.1 0.1 0.9 alphaVal]; 

% Reset the conditions to be all one condition
    Experimental_Results.conditionIDByCell = ...
        ones(size(Experimental_Results.conditionIDByCell));
    Experimental_Results.conditionIDByTrack = ...
        ones(size(Experimental_Results.conditionIDByTrack));
    Experimental_Results.numConditions = 1;

% Perform the MSD analysis

    % Perform the time-averaged MSD vs time
        [Experimental_Results] = ...
            performMSD_AverageByTrackCellAndCondition(Experimental_Results);
    
    % Fit the MSD data
        [Experimental_Results] = ...
            performMSDFit_AverageByTrackCellAndCondition(Experimental_Results);
               
% Make the MSD plot
    figure(figureNumMSDByCondition)
    % Plot the X MSD for the experimental results
        % Plot the mean
            plot(Experimental_Results.MSDResults.time_interval_for_MSD_ms,...
                squeeze(Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition_dims.mean(:,1)),...
                '','Color',[0 1 0 alphaVal],'LineWidth',1)
            hold on;
        % Plot the SE of the mean as error bars
            EB = errorbar(Experimental_Results.MSDResults.time_interval_for_MSD_ms,...
                squeeze(Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition_dims.mean(:,1))',...
                squeeze(Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition_dims.semean(:,1))',...
                'r','LineStyle', 'none','Color','g');
            set([EB.Bar, EB.Line], 'ColorType', 'truecoloralpha', ...
                'ColorData', [EB.Line.ColorData(1:3); 255*alphaVal])
            set(EB.Cap, 'EdgeColorType', 'truecoloralpha', ...
                'EdgeColorData', [EB.Cap.EdgeColorData(1:3); 255*alphaVal])  
            set(EB,'HandleVisibility', 'off')
    % Plot the Y MSD for the experimental results
        % Plot the mean
            plot(Experimental_Results.MSDResults.time_interval_for_MSD_ms,...
                squeeze(Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition_dims.mean(:,2)),...
                '','Color',[1 0 1 alphaVal],'LineWidth',1)
            hold on;
        % Plot the SE of the mean as error bars
            EB = errorbar(Experimental_Results.MSDResults.time_interval_for_MSD_ms,...
                squeeze(Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition_dims.mean(:,2))',...
                squeeze(Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition_dims.semean(:,2))',...
                'r','LineStyle', 'none','Color','m');
            set([EB.Bar, EB.Line], 'ColorType', 'truecoloralpha', ...
                'ColorData', [EB.Line.ColorData(1:3); 255*alphaVal])
            set(EB.Cap, 'EdgeColorType', 'truecoloralpha', ...
                'EdgeColorData', [EB.Cap.EdgeColorData(1:3); 255*alphaVal])  
            set(EB,'HandleVisibility', 'off')
    % Plot the totla MSD
        % Plot the mean
            plot(Experimental_Results.MSDResults.time_interval_for_MSD_ms,...
                Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(conditionNum2Plot,:),...
                '','Color',[experimentColor],'LineWidth',1)
            hold on;
        % Plot the SE of the mean as error bars
            EB = errorbar(Experimental_Results.MSDResults.time_interval_for_MSD_ms,...
                Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.mean(conditionNum2Plot,:)',...
                Experimental_Results.MSDResults.MSD_nm2_TimeAvgByTrack_EnsAvgByCondition.semean(conditionNum2Plot,:)',...
                'r','LineStyle', 'none','Color',experimentColor);
            set([EB.Bar, EB.Line], 'ColorType', 'truecoloralpha', ...
                'ColorData', [EB.Line.ColorData(1:3); 255*0.3])
            set(EB.Cap, 'EdgeColorType', 'truecoloralpha', ...
                'EdgeColorData', [EB.Cap.EdgeColorData(1:3); 255*0.3])  

        % Clean up the plot
            xl = xlim();
            plot([xl(1) xl(2)],[0 0],'k')
            hold off;
            xlim([10^1 10^3])
            ylim([10^3 2*10^6])
            set(gca, 'YScale', 'log')
            set(gca, 'XScale', 'log')
            yticks([10^3 10^4 10^5 10^6])
            xticks([10^1 10^2 10^3])
            xlabel('Time offset (ms)')
            ylabel('MSD (nm^2)')
            title(sprintf('Mean-squared \n displacement (MSD)'))
            set(gca,'FontName','Helvetica','FontSize',6); 
            legend({'X - long axis','Y - short axis','Total'},...
                'Location','Southeast','FontSize',5)   
        
if doSaveImages
    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\SuppFigure1\SuppFig1a_MSDByOrientation'];
        % Choose the figure size and position
            figHandle = figure(figureNumMSDByCondition);
            figHandle.Position =  [100   615  190   160];
        % Turn off the legend
       %     legend(gca,'off');
        % Save the figure in PDF format         
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])
end


