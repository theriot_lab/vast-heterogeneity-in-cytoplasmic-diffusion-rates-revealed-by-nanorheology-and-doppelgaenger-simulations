% This script calculates particle positions diffusing under fractional
% Brownian motion. Written and developed by Rikki Garner to generate the
% figures in Garner and Molines et al. 2022

%% Clear the system and reset the random number generator

% Close all figures
    close all;
% Clear all variables
    clear all; 
% Give the random number generator a random seed
% (Random number generators are actually deterministic based on 
% the initial seed given to the random number generator. The option 
% 'shuffle' sets the seed to the current time, which is pseudorandom
% unless you always start the simulation at the same exact time)
    rng('shuffle');
    
            
    % Choose the dataset name
        datasetName = 'Fractional Brownian Motion';
    % Choose the corresponding parameter values       
        conditionParameterVals = [1];     
        conditionNameforSprintf = 'Replicate %d';  
    % Choose the plotting parameters
        % Choose the condition number for each folder
            condition2plot = 1:length(conditionParameterVals);
        % Choose which color to plot the conditions    
            CMByCondition = jet(length(condition2plot))*0.75;
        % Choose whether to plot or save results
            doSaveResults = false;
            doPlotFitDistByTrack = false;
            doPlotMSDByConditionWithFBM = true;
            doPlotVelAutocorrDByConditionWithFBM = true;            
            doSaveImages = true;
    % Load the dataset info into a structure
        simulationDatasetInfo = v2struct(); 
        
    % Choose a filename to save the results
        % Pull out the date and convert to string for folder naming convention
            str = date();
            dateNum = datestr(str,'yyyy/mm/dd');
            dateNum = dateNum(dateNum~='/');
        % Choose the parent folder path to create the new folder in
            savefolderPath = ['Garner_Molines_et_al\'...
                'Simulation_Data\FractionalBrownianMotion\'];
        % Create the new folder name and path, assuming it doesn't already
        % exist, otherwise append a number to the end of the date until we have
        % a new folder
            saveFileName = sprintf('Results_FBM_ExpStats_%s.mat',dateNum);
            saveFilePath = [savefolderPath saveFileName];
            if isfile(saveFilePath)
                n=1;
                while isfile(saveFilePath)
                    saveFileName = sprintf('Results_FBM_ExpStats_%s_%i.mat',dateNum,n);
                    saveFilePath = [savefolderPath saveFileName];
                    n=n+1;
                end
            end
        
%% Choose the experimental dataset

    % Write the path to the experimental dataset
        experimentalDatasetFilePath = ['Garner_Molines_et_al\Experimental_Data\'...
            'Analyzed_Data\All_Controls_Combined_Analyzed.mat'];
        Experimental_Results = load(experimentalDatasetFilePath);

    % Load the experimental dataset
        % Load the tracks
            load(experimentalDatasetFilePath,'trackPositions_nm');
            experimentalTracks = trackPositions_nm;
            load(experimentalDatasetFilePath,'DataOrganizedByCell');    
            experimentalDataOrganizedByCell = DataOrganizedByCell;            
            DataOrganizedByCell = struct('Length',{experimentalDataOrganizedByCell(:).Length});
            load(experimentalDatasetFilePath,'numTracks');
            load(experimentalDatasetFilePath,'numDimensions');
            load(experimentalDatasetFilePath,'dimensionNames')
            load(experimentalDatasetFilePath,'numTimepoints');
            load(experimentalDatasetFilePath,'timePoints_ms');
            load(experimentalDatasetFilePath,'numCells');
            load(experimentalDatasetFilePath,'numConditions');
            load(experimentalDatasetFilePath,'uniqueConditions');
        % Load the condition and cell IDs
            load(experimentalDatasetFilePath,'conditionIDByTrack');
            load(experimentalDatasetFilePath,'conditionIDByCell');
            load(experimentalDatasetFilePath,'cellIDByTrack');
        % Load the timestep and pixel size
            load(experimentalDatasetFilePath,'msPerTimeStep');

    % Reset the conditions to be all one condition
        Experimental_Results.conditionIDByCell = ...
            ones(size(Experimental_Results.conditionIDByCell));
        Experimental_Results.conditionIDByTrack = ...
            ones(size(Experimental_Results.conditionIDByTrack));
        Experimental_Results.numConditions = 1;
        
    % Perform the MSD analysis
    
        % Perform the time-averaged MSD vs time
            [Experimental_Results] = ...
                performMSD_AverageByTrackCellAndCondition(Experimental_Results);
        
        % Fit the MSD data
            [Experimental_Results] = ...
                performMSDFit_AverageByTrackCellAndCondition(Experimental_Results);
           
    % Perform the velocity autocorrelation        
        [Experimental_Results] = ...
            performVelocityAutocorrelation_AverageByTrackCellAndCondition(Experimental_Results);

%% Simulate fractional Brownian motion using the Cholesky decomposition method
% of the covariance matrix.

% Choose the time points to simulate
    % Choose the time step
        dtInMS = msPerTimeStep;
    % Choose the total time to simulate
        totalTime2SimInMS = numTimepoints*msPerTimeStep;
    % Calculate the timepoints    
        timeInMS = 0:dtInMS:totalTime2SimInMS; 
    % Calculate the number of timepoints
        numTimePoints = length(timeInMS);       
        
% Choose the number of particles
    numParticles = numTracks;

% Choose with experimnetal condition to plot
    conditionNum2Plot = 1;
        
% Choose the anomylous diffusion exponent
    alphaVal = Experimental_Results.MSDFitResults.alphaVal_FitMeanByCondition(conditionNum2Plot);
    
% Choose the diffusion coefficient (in each dimension, in units of nm^2/ms^alpha?)
    D = Experimental_Results.MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCondition(conditionNum2Plot).*...
        (Experimental_Results.MSDFitResults.timeIntervalForDApp_ms.^...
        (1-Experimental_Results.MSDFitResults.alphaVal_FitMeanByCondition(conditionNum2Plot)));

% Simulate the x-dimension

    % Form the variance covariance matrix VCV = (R(t_i,t_j),i,j=1,....n) where 
    % R(t,s) = (s^(alphaVal) + t^(alphaVal) - |t-s|^(alphaVal))/2
    % (A variance-covariance matrix is a square matrix that contains the 
    % variances and covariances associated with several variables. The 
    % diagonal elements of the matrix contain the variances of the variables 
    % and the off-diagonal elements contain the covariances between all 
    % possible pairs of variables.)
        % Create a matrix of initial times
            t = repmat(timeInMS',[1,numTimePoints]);
        % Create a matrix of final times
            s = repmat(timeInMS,[numTimePoints,1]);
        % Create the variance covariance matrix Gamma
            VCV = 2*D*(s.^(alphaVal)+t.^(alphaVal)-...
                abs(t-s).^(alphaVal))/2;

    % Compute the square root matrix of the variance-covariance matrix, 
    % essentially the equivalent "standard deviation" (Note: this 
    % is NOT the same as taking the square root of each matrix element.)  
        STD = sqrtm(VCV);

    % Compute a distribution of normally distributed random numbers with 
    % mean=0 and std=1
        randomV = randn(numTimePoints,numParticles);

    % Convolve the square root matrix with the random number vector to
    % determine the path
        X_Pos_Traj_NM = STD*randomV;
        

% Simulate the x-dimension

    % Form the variance covariance matrix VCV = (R(t_i,t_j),i,j=1,....n) where 
    % R(t,s) = (s^(alphaVal) + t^(alphaVal) - |t-s|^(alphaVal))/2
    % (A variance-covariance matrix is a square matrix that contains the 
    % variances and covariances associated with several variables. The 
    % diagonal elements of the matrix contain the variances of the variables 
    % and the off-diagonal elements contain the covariances between all 
    % possible pairs of variables.)
        % Create a matrix of initial times
            t = repmat(timeInMS',[1,numTimePoints]);
        % Create a matrix of final times
            s = repmat(timeInMS,[numTimePoints,1]);
        % Create the variance covariance matrix Gamma
            VCV = 2*D*(s.^(alphaVal)+t.^(alphaVal)-...
                abs(t-s).^(alphaVal))/2;

    % Compute the square root matrix of the variance-covariance matrix, 
    % essentially the equivalent "standard deviation" (Note: this 
    % is NOT the same as taking the square root of each matrix element.)  
        STD = sqrtm(VCV);

    % Compute a distribution of normally distributed random numbers with 
    % mean=0 and std=1
        randomV = randn(numTimePoints,numParticles);

    % Convolve the square root matrix with the random number vector to
    % determine the path
        Y_Pos_Traj_NM = STD*randomV;
    
%% Quickly analyze the results

% Determine some basic information about the dataset 

    % Reformat the data for inputting into the velocity autocorrelation pipeline       
        % Reformat the position data for all dimensions into a single matrix of
        % size (numTrack x numDimensions x numTimePoints)
            trackPositions_nm = nan([size(X_Pos_Traj_NM,2) 2 size(X_Pos_Traj_NM,1)]);
            trackPositions_nm(:,1,:) = X_Pos_Traj_NM';            
            trackPositions_nm(:,2,:) = Y_Pos_Traj_NM';
        % Re-name the timestep to be consistent with experimental data
        % analysis
            msPerTimeStep = dtInMS;
        % Name the dimensions
            dimensionNames = {'X','Y'}; 
        
    % Calculate the size of the data
        % Calculate the number of tracks
            numTracks = size(trackPositions_nm,1);
        % Calculate the number of dimensions
            numDimensions = size(trackPositions_nm,2);
        % Calculate the number of timepoints
            numTimepoints = size(trackPositions_nm,3);

    % Create the associated vector of time points in ms
        timePointsInMS = (0:(numTimepoints-1)).*msPerTimeStep;      

% Remove any time points not collected in the experimental data
    trackPositions_nm(isnan(experimentalTracks)) = nan;
        
% Save the results        
    cellIDByTrack = ones(numTracks,1);
    numCells=1;
    conditionIDByTrack = ones(numTracks,1) ;
    numConditions=1;
    conditionIDByCell=1;
    uniqueConditions = {'1'};
    Simulation_Results = v2struct();
        
% Perform the MSD analysis

    % Perform the time-averaged MSD vs time
        [Simulation_Results] = ...
            performMSD_AverageByTrackCellAndCondition(Simulation_Results);
    
    % Fit the MSD data
        [Simulation_Results] = ...
            performMSDFit_AverageByTrackCellAndCondition(Simulation_Results);
       
% Perform the velocity autocorrelation        
    [Simulation_Results] = ...
        performVelocityAutocorrelation_AverageByTrackCellAndCondition(Simulation_Results);

% Save the file
if Simulation_Results.doSaveResults
    save(Simulation_Results.saveFilePath,'-struct',...
        'Simulation_Results')
end
    
%% Plot the velocity autocorrelation results for FBM and the experiment on the same plot

simulationDatasetInfo = Simulation_Results.simulationDatasetInfo;

if Simulation_Results.doPlotVelAutocorrDByConditionWithFBM


    % Update the condition color
        simulationDatasetInfo.CMByCondition(1,:) = [0 1 1]*0.9;
    
    % Plot the Vacorr and fits by condition
        % Choose where to make the plot
            % Choose the figure number
                figureNumVelAutocorr = 10;
            % Choose the number of axes in the plot
                numRows = 1;
                numCols = 2;
            % Choose the subplots for this dataset
                numSubplotMean = 1;
                numSubplotNormMean = 2;
        % Make the plot
            plotVelocityAutocorrelation_AvgByCondition_FBMTheoryWithExpmts(...
                Experimental_Results,Simulation_Results,...
                simulationDatasetInfo,numRows,numCols,figureNumVelAutocorr,...
                numSubplotMean,numSubplotNormMean)
            drawnow     
            
    if Simulation_Results.doSaveImages
        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                    'Figure_Panels\SuppFigure1\SuppFig1c_VelAutocorr_FBMvsExpmt'];
            % Choose the figure size and position
                figHandle = figure(figureNumVelAutocorr);
                figHandle.Position =  [100   553  421   197];
%             % Turn off the legend
%                 subplot(numRows,numCols,numSubplotMean)
%                 legend(gca,'off');
%                 subplot(numRows,numCols,numSubplotNormMean)
%                 legend(gca,'off');
            % Save the figure in PDF format         
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])      
    end

    % Find the inset
         figure(figureNumVelAutocorr)
         subplot(numRows,numCols,numSubplotMean)
         legend(gca,'off');
         xlim([0 50])
         yl = get(gca,'ylim');
         ylim([yl(1) -yl(1)])
         subplot(numRows,numCols,numSubplotNormMean)
         legend(gca,'off');
         xlim([0 50])
         ylim([-0.065 0.065])

    if Simulation_Results.doSaveImages
        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                    'Figure_Panels\SuppFigure1\SuppFig1c_VelAutocorr_FBMvsExpmt_Inset'];
            % Choose the figure size and position
                figHandle = figure(figureNumVelAutocorr);
                figHandle.Position =  [100   553  421   197];
%             % Turn off the legend
%                 subplot(numRows,numCols,numSubplotMean)
%                 legend(gca,'off');
%                 subplot(numRows,numCols,numSubplotNormMean)
%                 legend(gca,'off');
            % Save the figure in PDF format         
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])         
    end
end    

if Simulation_Results.doPlotMSDByConditionWithFBM
    %% Plot the MSD and fits by condition
        % Choose where to make the plot
            % Choose the figure number
                figureNumMSD = 1;
            % Choose the number of axes in the plot
                numRows = 1;
                numCols = 1;
            % Choose the subplots for this dataset
                numSubplotMean = 1;
                numSubplotMedian = 1;
        % Make the plot
            plotMSDAndFit_AvgByCondition_FBMTheoryWithExpmts(Experimental_Results,...
                Simulation_Results,simulationDatasetInfo,...
                numRows,numCols,figureNumMSD,numSubplotMean)
            drawnow 
        % Clean up the plot
            xlim([10^1 10^3])
            ylim([10^3 2*10^6])
            set(gca, 'YScale', 'log')
            set(gca, 'XScale', 'log')
            yticks([10^3 10^4 10^5 10^6])
            xticks([10^1 10^2 10^3])
            xlabel('Time offset (ms)')
            ylabel('MSD (nm^2)')
            title(sprintf('Mean-squared \n displacement (MSD)'))
            set(gca,'FontName','Helvetica','FontSize',6); 
            %legend({'X - long axis','Y - short axis','Total'},...
                %'Location','Southeast','FontSize',5)   
            
    if Simulation_Results.doSaveImages

        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                    'Figure_Panels\SuppFigure1\SuppFig1b_MSD_FBMvsExpmt'];
            % Choose the figure size and position
                figHandle = figure(figureNumMSD);
                figHandle.Position =  [100   615  190   160];
            % Save the figure in PDF format         
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])           

    end
    
end    
