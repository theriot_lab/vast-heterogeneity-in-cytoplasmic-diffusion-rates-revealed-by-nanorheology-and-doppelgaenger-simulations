%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    % Close all figures
        close all;
    % Clear all variables
        clear all; 
    
% Choose the dataset to plot
    experimentalDatasetFilePath = ['Garner_Molines_et_al\Experimental_Data\'...
        'Analyzed_Data\All_Controls_Combined_Analyzed.mat'];
    Experimental_Results = load(experimentalDatasetFilePath);
  
% Choose the plot colors
    experimentColor = [0.1 0.1 0.9 0.1];  
    alphaByTrack = 0.1;
    alphaByCell = 0.8;
    
% Choose the figure numbers
    figureNumDvsAlpha = 1;
    figureNumInstrinsicVsExtrinsic = 2;
    figureNumDvsNumTracks = 3;
    figureNumCVvsNumTracks = 4;
    figureNumCVvsD = 5;

% Reset the conditions to be all one condition
    Experimental_Results.conditionIDByCell = ...
        ones(size(Experimental_Results.conditionIDByCell));
    Experimental_Results.conditionIDByTrack = ...
        ones(size(Experimental_Results.conditionIDByTrack));
    Experimental_Results.numConditions = 1;

% Perform the MSD analysis

    % Perform the time-averaged MSD vs time
        [Experimental_Results] = ...
            performMSD_AverageByTrackCellAndCondition(Experimental_Results);
    
    % Fit the MSD data
        [Experimental_Results] = ...
            performMSDFit_AverageByTrackCellAndCondition(Experimental_Results);

    % Set MSDFitResults as its own variable
        MSDFitResults = Experimental_Results.MSDFitResults;    

% Calculate the mean, standard deviation, and coefficient of variation 
% (SD/Mean) of diffusivities, averaged by cell
    % Pre-allocate space to store the CVs
        MEAN = nan(Experimental_Results.numCells,1);
        SD = nan(Experimental_Results.numCells,1);
        CV = nan(Experimental_Results.numCells,1);
        numTracksPerCell = nan(Experimental_Results.numCells,1);
    % Loop through each cell and calculate the CV
    for cellNum = 1:Experimental_Results.numCells
        trackIDX = (Experimental_Results.cellIDByTrack==cellNum);
        numTracksPerCell(cellNum) = nansum(trackIDX);
        MEAN(cellNum) = nanmean(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack(trackIDX));
        SD(cellNum) = nanstd(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack(trackIDX));
        CV(cellNum)  = SD(cellNum)./MEAN(cellNum);            
    end

%% Plot D vs alpha

    % Make the plot       
        figure(figureNumDvsAlpha)
        % Plot the track-wise data
        scatter(MSDFitResults.alphaVal_FitMeanByTrack,...
            log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack), 6,...
            'o','MarkerFaceColor',experimentColor(1:3),...
            'MarkerEdgeColor','none',...
            'MarkerFaceAlpha',alphaByTrack)   
        hold on;
        % Plot the cell-wise data
        scatter(MSDFitResults.alphaVal_FitMeanByCell,...
            log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell), 6,...
            'o','MarkerFaceColor',experimentColor(1:3)*0.6,...
            'MarkerEdgeColor','none',...
            'MarkerFaceAlpha',alphaByCell)  
        hold off;
    % Clean up the plot
        box on;
        xlim([-2.5 2.5])
        ylim([-0.5 4.5])
        xticks(-2:2)
        yticks([0 1 2 3 4])
        title({'Fit parameters','across all tracks'})
        xlabel('Power law exponent (\alpha, unitless)')
        ylabel({'log_{10}(Diffusivity)','(D_{app, 100ms}, nm^2/ms)'})
        set(gca,'FontName','Helvetica','FontSize',6);      

    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\SuppFigure2\SuppFig2a_D_vs_alpha'];
        % Choose the figure size and position
            figHandle = figure(figureNumDvsAlpha);
            figHandle.Position =  [100   615  160   125];
        % Save the figure in PDF format         
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])

%% Make scatter plot comparing particle diffusivity between different
% particles in the same cell

% Pull out the diffusivity values
    log10D2Analyze = ...
        log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack);

% For each particle, choose a random partner in the same cell for comparison
% (without replacement)
    % Initialize the vector containing the set of combinations
        combos = [];
    % Pull out the unique cells represented in the dataset
        uniqueCellNums = unique(Experimental_Results.cellIDByTrack);
    % For each cell, pull out the particles detected in that cell and pair
    % them randomly
    for cellNum = uniqueCellNums'
        % Find the particles in this cell
            particles = find(Experimental_Results.cellIDByTrack==cellNum);
        % Determine all possible pairs of particles, without replacement
            b = nchoosek(particles',2);
        % For each particle, choose only the first pair in this list
            [b1Vals ia ic] = unique(b(:,1),'first');
            b = b(ia,:);
        % Add this pair to the vector
            combos = [combos ; b];
    end

% Calculate the spearman correlation coefficient
    [R_DP1VsDP2 P_DP1VsDP2] = corr(log10D2Analyze(combos(:,1)),...
        log10D2Analyze(combos(:,2)),'Type','Spearman');

% Plot the comparisons on a scatter plot
    figure(figureNumInstrinsicVsExtrinsic)
    scatter(log10D2Analyze(combos(:,1)),log10D2Analyze(combos(:,2)),6,'o',...
        'MarkerFaceColor',[0.1 0.1 0.9],'MarkerEdgeColor',[0.1 0.1 0.9],...
        'MarkerFaceAlpha',alphaByTrack,'MarkerEdgeAlpha',alphaByTrack)
    hold on;
    %axis equal
    xlim([-0.5 4.5])
    ylim([-0.5 4.5])
    xl = get(gca,'xlim');
    yl = get(gca,'ylim');
    minVal = min(xl(1),yl(1));
    maxVal = max(xl(2),yl(2));
    %plot([minVal maxVal],[polyval(p,minVal) polyval(p,maxVal)],'k--')
    plot([minVal maxVal],[minVal maxVal],'k--')
    hold off;
    legend({sprintf('Data, r = %2.2f \n p = %2.2d',...
        R_DP1VsDP2,P_DP1VsDP2)','D_1=D_2'},'Location','NorthWest')
    xlabel({'Particle 1: log_{10}(Diffusivity)','(D_{app, 100ms}, nm^2/ms)'})
    ylabel({'Particle 2: log_{10}(Diffusivity)','(D_{app, 100ms}, nm^2/ms)'})
    set(gca,'FontName','Helvetica','FontSize',6);   
    box on;

% Save the figure
    % Choose the filename and path for the figure
        destinationTrack = ['Garner_Molines_et_al\'...
            'Figure_Panels\SuppFigure2\SuppFig2b_IntrinsicVsExtrinsic'];
    % Choose the figure size and position
        figHandle = figure(figureNumInstrinsicVsExtrinsic);
        figHandle.Position =  [100   615  150   130];
%         % Turn off the legend
%             legend(gca,'off');
    % Save the figure in PDF format         
        exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
    % Save the figure in matlab figure format
        savefig([destinationTrack '.fig'])   

%% Make the scatter plot of the number of tracks vs the mean diffusivity 
% across all tracks in each cell

    % Calculate the spearman correlation coefficient
        [R_NumTracksVsMean P_NumTracksVsMean] = ...
            corr(numTracksPerCell,MEAN,'Type','Spearman');
    
    % Make the plot
        figure(figureNumDvsNumTracks)
        scatter(numTracksPerCell,MEAN,6,'o','MarkerFaceColor',experimentColor(1:3)*0.6,...
            'MarkerEdgeColor','none')
        box on;
        ylabel('Mean diffusivity (nm^2/ms)')
        xlabel('Number of tracks in the cell')
        set(gca,'FontName','Helvetica','FontSize',6);    
        legend({sprintf('Data, r = %2.2f \n p = %2.2d',...
            R_NumTracksVsMean,P_NumTracksVsMean)'},'Location','NorthWest')
        set(gca,'FontName','Helvetica','FontSize',6);    
    
    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                    'Figure_Panels\SuppFigure2\SuppFig2c_NumParticlesVsMean_DByCell'];
        % Choose the figure size and position
            figHandle = figure(figureNumDvsNumTracks);
            figHandle.Position =  [100   615  150   130];
        % Save the figure in PDF format         
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])


%% Make the scatter plot of the number of tracks vs the coefficient of 
% variation in diffusivity across all tracks in each cell

    % Calculate the spearman correlation coefficient
        [R_NumTracksVsCV P_NumTracksVsCV] = ...
            corr(numTracksPerCell,CV,'Type','Spearman');
    
    % Make the plot
        figure(figureNumCVvsNumTracks)
        scatter(numTracksPerCell,CV,6,'o','MarkerFaceColor',experimentColor(1:3)*0.6,...
            'MarkerEdgeColor','none')
        box on;
        ylabel(sprintf('Coefficient of variation \n (unitless)'))
        xlabel('Number of tracks in the cell')
        legend({sprintf('Data, r = %2.2f \n p = %2.2d',...
            R_NumTracksVsCV,P_NumTracksVsCV)'},'Location','NorthWest')
        set(gca,'FontName','Helvetica','FontSize',6);    
    
    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                    'Figure_Panels\SuppFigure2\SuppFig2d_NumParticlesVsCV_DByCell'];
        % Choose the figure size and position
            figHandle = figure(figureNumCVvsNumTracks);
            figHandle.Position =  [100   615  150   130];
        % Save the figure in PDF format         
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])

            
%% Make the scatter plot of the coefficient of variation vs the mean 
% diffusivitiy in each cell

    % Calculate the spearman correlation coefficient
        [R_MeanVsCV P_MeanVsCV] = ...
            corr(numTracksPerCell,CV,'Type','Spearman');  

    % Make the plot
        figure(figureNumCVvsD)
        scatter(MEAN',CV',6,'o','MarkerFaceColor',experimentColor(1:3)*0.6,...
            'MarkerEdgeColor','none')
        box on;
        xlabel('Mean diffusivity (nm^2/ms)')
        ylabel(sprintf('Coefficient of variation \n (unitless)'))
        legend({sprintf('Data, r = %2.2f \n p = %2.2d',...
            R_MeanVsCV,P_MeanVsCV)'},'Location','NorthWest')
        set(gca,'FontName','Helvetica','FontSize',6);    

    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                    'Figure_Panels\SuppFigure2\SuppFig2e_CVvsMean_DByCell'];
        % Choose the figure size and position
            figHandle = figure(figureNumCVvsD);
            figHandle.Position =  [100   615  150   130];
        % Save the figure in PDF format         
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])


