%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    close all;
    clear all;    

% Choose and load the comparisons to be analyzed and plotted     
    % Load the simulation data
        simulationsFolderPath = ['Garner_Molines_et_al\Simulation_Data\'...
            'ParameterScan_OptVarAndMean_BoxSize_Coarse_Fig4g\'];
    % Choose the parameter indices for each simulation we want to pull out
        % cols: fracCellVar2SimNum fracSpaceVar2SimNum viscosityDomainSizeInNM2SimNum viscosityNum replicateNum
        % rows: no var; spatial var; cell var; both 
            simIdx2PullOut = [1 1 1 1 4; 2 2 2 2 5; 3 3 3 3 5; 4 4 4 4 5; 5 5 5 5 12];
    % Load an example file to get some information about the dataset
        % Choose example file
            folderInfo = dir(simulationsFolderPath);
            fileName = folderInfo(3).name;
            exampleFilePath = [simulationsFolderPath fileName];
        % Load the parameter information    
            load(exampleFilePath,'fracCellVar2SimNums')
            load(exampleFilePath,'fracCellVar2SimVals')
            load(exampleFilePath,'fracSpaceVar2SimNums')
            load(exampleFilePath,'fracSpaceVar2SimVals')
            load(exampleFilePath,'meanViscosity2SimNums')
            load(exampleFilePath,'meanViscosity2SimVals')
            load(exampleFilePath,'viscosityDomainSizeInNM2SimNums')
            load(exampleFilePath,'viscosityDomainSizeInNM2SimVals')
            load(exampleFilePath,'replicateNums')
            load(exampleFilePath,'ParamScanName')
            load(exampleFilePath,'dateNum')  

%% Make a plot of the medians     
    % Choose the group names for plotting (by looking at the names)
        groupNames2Plot = {sprintf('%d',viscosityDomainSizeInNM2SimVals(1)),...
            sprintf('%d',viscosityDomainSizeInNM2SimVals(2)),...
            sprintf('%d',viscosityDomainSizeInNM2SimVals(3)),...
            sprintf('%d',viscosityDomainSizeInNM2SimVals(4)),...
            sprintf('%d',viscosityDomainSizeInNM2SimVals(5))};
    % Choose the plot color
        CMByGroup = repmat([0.8500 0.3250 0.0980],[5, 1]);
    % Pull up the figure
        figure(3)
    % Plot the mean viscosity
        subplot(1,3,1)
        % Make the bar plots
            b = bar(meanViscosity2SimVals);
            b.FaceColor = 'flat';
            b.CData = CMByGroup;
        % Choose the axes limits and label the graph
            set(gca,'XTickLabel',groupNames2Plot)
            yticks(0:10:50)
            % Choose the x axes limits
                spaceBetweenBars = mean(diff(b.XData));
                xlim([(min(b.XData) - (spaceBetweenBars*3/4)) ...
                    (max(b.XData) + (spaceBetweenBars*3/4))]);
            ylabel({'Mean viscosity','Relative to water'})
            xlabel('Spatial domain size (nm)')
            ylim([0 55])
            set(gca,'FontName','Helvetica','FontSize',6);     
    % Plot the cellular variation in viscosity
        subplot(1,3,2)
        % Make the bar plots
            b = bar(fracCellVar2SimVals);
            b.FaceColor = 'flat';
            b.CData = CMByGroup;
        % Choose the axes limits and label the graph
            set(gca,'XTickLabel',groupNames2Plot)
            yticks(0:0.1:0.5)
            % Choose the x axes limits
                spaceBetweenBars = mean(diff(b.XData));
                xlim([(min(b.XData) - (spaceBetweenBars*3/4)) ...
                    (max(b.XData) + (spaceBetweenBars*3/4))]);
            ylabel({'CV of cellular variation'})
            xlabel('Spatial domain size (nm)')
            ylim([0 0.45])
            set(gca,'FontName','Helvetica','FontSize',6);     
    % Plot the spatial variation in viscosity
        subplot(1,3,3)
        % Make the bar plots
            b = bar(fracSpaceVar2SimVals);
            b.FaceColor = 'flat';
            b.CData = CMByGroup;
        % Choose the axes limits and label the graph
            set(gca,'XTickLabel',groupNames2Plot)
            yticks(0:0.2:1.2)
            % Choose the x axes limits
                spaceBetweenBars = mean(diff(b.XData));
                xlim([(min(b.XData) - (spaceBetweenBars*3/4)) ...
                    (max(b.XData) + (spaceBetweenBars*3/4))]);
            ylabel({'CV of spatial variation'})
            xlabel('Spatial domain size (nm)')
            ylim([0 1.25])
            set(gca,'FontName','Helvetica','FontSize',6);     
    
   %% Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\SuppFigure3\SuppFig3a_'...
                'BarPlotsInputParams_BoxSize_Coarse'];
        % Choose the figure size and position
            figHandle = figure(3);
            figHandle.Position =  [450   1630   455   130];
        % Save the file as a pdf
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])
