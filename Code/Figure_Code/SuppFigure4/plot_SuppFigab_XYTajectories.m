close all;
clear all;

% Load the data
    % Choose the data file path
        filePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
            'All_Controls_Combined_Analyzed.mat'];
    % Load the data
        load(filePath)

% Choose the figure number
    figureNumPlotFastAndSlowTracks = 1;
    figureNumPlotLongestTracks = 2;

% Choose the color
    xColor = [0.01 1  0.01]*0.5;
    yColor = [1 0.01 1]*0.5;

% Calculate the mean and std of the step size across all tracks     
    % Pull out the trajectories and calculate the step size in each dimension
        % Pull out the track positions
            x = squeeze(trackPositions_nm(:,1,:))';
            y = squeeze(trackPositions_nm(:,2,:))';
        % Calculate the step sizes
            dx = diff(x,1,1);
            dy = diff(y,1,1);
        % Concatenate step sizes for all tracks into a single vector
            dx = dx(:);
            dy = dy(:);
        % Pull out the track positions
            x = x(1:(end-1),:);
            y = y(1:(end-1),:);
    % Add these step sizes        
        stepSizesInNM = sqrt(dx.^2 + dy.^2);
     % Calculate the mean and sd of this distribution
        meanStepSizeInNM = round(nanmean(stepSizesInNM));
        stdStepSizeInNM = round(nanstd(stepSizesInNM));

% Plot the longest tracks

% Pull out the longest tracks
    trackLengthInTimeSteps = sum(~isnan(squeeze(sum(trackPositions_nm,2))),2);
    % Find the top 5
        num2Choose = 12;
        [maxTLVals,maxTLValIdx] = maxk(trackLengthInTimeSteps,num2Choose);

    % Plot the fast tracks colored by step size
        % Initialize a counter variable
            trackCounter = 0;
        % Loop through each track and plot it
        for trackNum = maxTLValIdx'

            % Update the counter
                trackCounter = trackCounter + 1
        
            % Prepare the data
                % Calculate the step size in each dimension
                    % Pull out the track positions and subtract the
                    % centroid
                        x = squeeze(trackPositions_nm(trackNum,1,:))'./10^3;
                        x = x - nanmean(x);
                        y = squeeze(trackPositions_nm(trackNum,2,:))'./10^3;
                        y = y - nanmean(y);
        
            % Optionally plot all tracks from a single cell together
                % Plot the tracks in real distances   
                    figure(figureNumPlotLongestTracks)
                    subplot(3,ceil(num2Choose/3),trackCounter)
                    % Plot the x- and y-values
                        plot(timePoints_ms/1000,x,'linew',1,'Color',xColor);
                        hold on;
                        plot(timePoints_ms/1000,y,'linew',1','Color',yColor);
                    % Clean up the plot
                        ylim([-1 1]*2)
                        xlim([0 3])
                        xlabel('Time (s)')
                        ylabel({'Particle position (\mu m)','relative to track centroid'})
                        title({'Particle Trajectory'})    
                        if trackCounter==num2Choose
                            legend({'X-position','Y-position'})
                        end
                        set(gca,'FontName','Helvetica','FontSize',6);  
                        drawnow  
        end

    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\SuppFigure4\SuppFig4ab_Ergod_LongTracks'];
        % Choose the figure size and position
            figHandle = figure(figureNumPlotLongestTracks);
            figHandle.Position =  [36   313  712   457];
            % Save the file as a pdf
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save as a png                               
                print(gcf,destinationTrack,'-dpng',['-r' num2str(600)])
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])     

