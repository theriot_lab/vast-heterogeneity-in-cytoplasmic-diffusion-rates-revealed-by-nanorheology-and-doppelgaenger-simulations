%% Clear the system and reset the random number generator

% Close all figures
    close all;
    close all hidden; % ANOVA tables
% Clear all variables
    clear all; 
    
% Choose the datasets to plot
    
    % Write the path to the experimental dataset
        experimentalDatasetFilePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
            'All_Controls_Combined_Analyzed.mat'];
        Experimental_Results = load(experimentalDatasetFilePath);

% Reset the conditions to be all one condition
    Experimental_Results.conditionIDByCell = ...
        ones(size(Experimental_Results.conditionIDByCell));
    Experimental_Results.conditionIDByTrack = ...
        ones(size(Experimental_Results.conditionIDByTrack));
    Experimental_Results.numConditions = 1;
        
% Perform the MSD analysis

    % Perform the time-averaged MSD vs time
        [Experimental_Results] = ...
            performMSD_EnsembleAverageByTrackCellAndCondition(Experimental_Results);
    
    % Fit the MSD data
        [Experimental_Results] = ...
            performMSDFit_EnsembleAverage(Experimental_Results);


%% Plot alpha vs track length

% Pull up the figure
    figureNumMSD_ErgodicityTest = 4;
    figure(figureNumMSD_ErgodicityTest)
% Choose the data color
    experimentColor = [0.1 0.1 0.9 0.1];
% Calculate the track lengths
    trackLengthInTimeSteps = ...
        sum(~isnan(squeeze(sum(Experimental_Results.trackPositions_nm,2))),2);
    trackLengthInMS = ...
       trackLengthInTimeSteps*Experimental_Results.msPerTimeStep;
% Plot a scatter of track lengh vs alpha
    scatter(trackLengthInMS,...
        Experimental_Results.MSDFitResults.alphaVal_FitMeanByTrack,7,...
        'MarkerFaceColor',experimentColor(1:3),...
        'MarkerFaceAlpha',experimentColor(4),...
        'MarkerEdgeColor',experimentColor(1:3),...
        'MarkerEdgeAlpha',0)
     hold on;
% Calculate the mean alpha for each track length
    % Calculate the unique track lengths
        uniqueTrackLengths = unique(trackLengthInTimeSteps);
    % Calculate the number of instances for each
        numCounts = groupcounts(trackLengthInTimeSteps,uniqueTrackLengths);
    % Only keep the time steps with more than three data points per
    % group
        uniqueTrackLengths = uniqueTrackLengths(numCounts>3);
        uniqueTrackLengthsInMS = uniqueTrackLengths*Experimental_Results.msPerTimeStep;
    % Pre-allocate apce to store the mean alpha
        meanAlphaByTrackLength = nan(size(uniqueTrackLengths));
        sdAlphaByTrackLength = nan(size(uniqueTrackLengths));
        seAlphaByTrackLength = nan(size(uniqueTrackLengths));
        numTracksByTrackLength = nan(size(uniqueTrackLengths));
    % Pre-allocate space to store the means and se
    for numLength = 1:length(uniqueTrackLengths)

        % Pull out the tracks with this track length
            tracks2Plot = (trackLengthInTimeSteps==uniqueTrackLengths(numLength));
        % Calculate the average alpha for these tracks
            meanAlphaByTrackLength(numLength) = ...
                nanmean(Experimental_Results.MSDFitResults.alphaVal_FitMeanByTrack(tracks2Plot));
            sdAlphaByTrackLength(numLength) = ...
                nanstd(Experimental_Results.MSDFitResults.alphaVal_FitMeanByTrack(tracks2Plot));
            numTracksByTrackLength(numLength) = ...
                sum(~isnan(Experimental_Results.MSDFitResults.alphaVal_FitMeanByTrack(tracks2Plot)));
            seAlphaByTrackLength(numLength) = ...
                sdAlphaByTrackLength(numLength)/sqrt(numTracksByTrackLength(numLength));
    end
% Choose the data color
    experimentColor = [0.1 0.1 0.1 1];
% Plot the mean alpha vs track length
    plot(uniqueTrackLengthsInMS,meanAlphaByTrackLength,'color',experimentColor,'LineWidth',1)
% Plot the SE of the mean as error bars
    EB = errorbar(uniqueTrackLengthsInMS,meanAlphaByTrackLength',seAlphaByTrackLength, ...
        'r','LineStyle', 'none','Color',experimentColor);
    set([EB.Bar, EB.Line], 'ColorType', 'truecoloralpha', ...
        'ColorData', [EB.Line.ColorData(1:3); 255*0.75],'LineWidth',1)
    set(EB.Cap, 'EdgeColorType', 'truecoloralpha', ...
        'EdgeColorData', [EB.Cap.EdgeColorData(1:3); 255*0.75],'LineWidth',1)  
% Plot alpha = 1
    xlim([100 3000])
    xl = get(gca,'XLim');
    plot([10 xl(2)], [1 1],'k--','LineWidth',1)
    hold off;
 % Clean up the plots
    ylim([-1 2])
    yticks(-1:0.5:2)
    set(gca,'XScale','Log')
    xlabel('Track length (ms)')
    ylabel({'Fitted power law exponent','(unitless)'})
    title(sprintf('Track-wise fits'))
    set(gca,'FontName','Helvetica','FontSize',6); 
    box on;


    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\SuppFigure4\SuppFig4c_AlphaVsTrackLength'];
        % Choose the figure size and position
            figHandle = figure(figureNumMSD_ErgodicityTest);
            figHandle.Position =  [488   623  290   140];
            % Save the file as a pdf
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save as a png                               
                print(gcf,destinationTrack,'-dpng',['-r' num2str(600)])
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])   
 