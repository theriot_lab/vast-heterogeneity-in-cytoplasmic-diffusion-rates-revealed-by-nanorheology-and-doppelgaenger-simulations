%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    % Close all figures
        close all;
    % Clear all variables
        clear all; 
    
% Choose the dataset to plot
    experimentalDatasetFilePath = ['Garner_Molines_et_al\Experimental_Data\'...
        'Analyzed_Data\All_Controls_Combined_Analyzed.mat'];
    Experimental_Results = load(experimentalDatasetFilePath);
  
% Choose the plot colors
    experimentColor = [0.1 0.1 0.9 0.1];  
    alphaByTrack = 0.1;
    alphaByCell = 0.8;
    
% Choose the figure numbers
    figureNumDVsCellLength = 1;
    figureNumAlphaVsCellLength = 2;
    figureNumDVsLongAxisPosition = 3;
    figureNumAlphaVsLongAxisPosition = 4;

% Reset the conditions to be all one condition
    Experimental_Results.conditionIDByCell = ...
        ones(size(Experimental_Results.conditionIDByCell));
    Experimental_Results.conditionIDByTrack = ...
        ones(size(Experimental_Results.conditionIDByTrack));
    Experimental_Results.numConditions = 1;

% Perform the MSD analysis

    % Perform the time-averaged MSD vs time
        [Experimental_Results] = ...
            performMSD_AverageByTrackCellAndCondition(Experimental_Results);
    
    % Fit the MSD data
        [Experimental_Results] = ...
            performMSDFit_AverageByTrackCellAndCondition(Experimental_Results);

    % Set structure elements as their own variable
        DataOrganizedByCell = Experimental_Results.DataOrganizedByCell;
        MSDFitResults = Experimental_Results.MSDFitResults;   
        cellIDByTrack = Experimental_Results.cellIDByTrack;
        trackPositions_nm = Experimental_Results.trackPositions_nm;
  
 %% Plot D vs cell length

    % Make the plot       
        figure(figureNumDVsCellLength)
        % Plot the track-wise data
        scatter([DataOrganizedByCell(cellIDByTrack).Length],...
            log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack),6,...
            'o','MarkerFaceColor',experimentColor(1:3),...
            'MarkerEdgeColor','none',...
            'MarkerFaceAlpha',alphaByTrack) 
        hold on;
        % Plot the cell-wise data
        scatter([DataOrganizedByCell.Length],...
            log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell),6,...
            'o','MarkerFaceColor',experimentColor(1:3)*0.6,...
            'MarkerEdgeColor','none',...
            'MarkerFaceAlpha',alphaByCell) 
        hold off;
    % Clean up the plot
        box on;
        xlim([5 15])
        ylim([-0.5 4.5])
        xticks(6:2:14)
        yticks([0 1 2 3 4])
        title({'Fit parameters','across all tracks'})
        xlabel('Cell length (\mu m)')
        ylabel({'log_{10}(Diffusivity)','(D_{app, 100ms}, nm^2/ms)'})
        set(gca,'FontName','Helvetica','FontSize',6);                
    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\SuppFigure5\SuppFig5a_D_vs_Length'];
        % Choose the figure size and position
            figHandle = figure(figureNumDVsCellLength);
            figHandle.Position =  [100   615  175   135];
        % Save the figure in PDF format         
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])

 %% Plot alpha vs cell length

    % Make the plot       
        figure(figureNumAlphaVsCellLength)
        % Plot the track-wise data
        scatter([DataOrganizedByCell(cellIDByTrack).Length],...
            MSDFitResults.alphaVal_FitMeanByTrack,6,...
            'o','MarkerFaceColor',experimentColor(1:3),...
            'MarkerEdgeColor','none',...
            'MarkerFaceAlpha',alphaByTrack) 
        hold on;
        % Plot the cell-wise data
        scatter([DataOrganizedByCell.Length],...
            MSDFitResults.alphaVal_FitMeanByCell,6,...
            'o','MarkerFaceColor',experimentColor(1:3)*0.6,...
            'MarkerEdgeColor','none',...
            'MarkerFaceAlpha',alphaByCell) 
        hold off;
    % Clean up the plot
        box on;
        xlim([5 15])
        ylim([-2.5 2.5])
        xticks(6:2:14)
        yticks(-2:2)
        title({'Fit parameters','across all tracks'})
        xlabel('Cell length (\mu m)')
        ylabel({'Power law exponent','(\alpha, unitless)'})
        set(gca,'FontName','Helvetica','FontSize',6);                
    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\SuppFigure5\SuppFig5b_alpha_vs_Length'];
        % Choose the figure size and position
            figHandle = figure(figureNumAlphaVsCellLength);
            figHandle.Position =  [100   615  150   130];
        % Save the figure in PDF format         
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])


%% Plot D and alpha as a function of average spatial localization for each particle

% Find the mean x-position of each track
    mean_Long_axis_pos_um = squeeze(nanmean(trackPositions_nm(:,1,:),3))/10^3;
% Find which cells could be accurately determine for left/right asymmetry
    newPolePosByCell = [DataOrganizedByCell.NewPolePos];
    nonNanPolePosByCell = (newPolePosByCell~="N/A");
    numNonNanCells = sum(nonNanPolePosByCell);
    nonNanPolePosByTrack = nonNanPolePosByCell(cellIDByTrack)';
    numNonNanTracks = sum(nonNanPolePosByTrack);
% Choose which tracks to plot
    tracks2Plot = find((cellIDByTrack>=16)&(mean_Long_axis_pos_um>-5)&...
        (mean_Long_axis_pos_um<5)&nonNanPolePosByTrack);

% Plot D vs Long axis position
    % Make the plot       
        figure(figureNumDVsLongAxisPosition)
        scatter(mean_Long_axis_pos_um(tracks2Plot),...
            log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack(tracks2Plot)),6,...
            'o','MarkerFaceColor',experimentColor(1:3),...
            'MarkerEdgeColor','none',...
            'MarkerFaceAlpha',alphaByTrack) 
    % Clean up the plot
        box on;
        xlim([-5 5])
        ylim([-0.5 4.5])
        xticks(-5:5)
        yticks([0 1 2 3 4])
        title({'Fit parameters','across all tracks'})
        xlabel({'Mean position along Long axis (\mu m)','New pole to right'})
        ylabel({'log_{10}(Diffusivity)','(D_{app, 100ms}, nm^2/ms)'})
        legend(sprintf('N_{cells} = %1.0f, N_{tracks} = %1.0f',numNonNanCells,numNonNanTracks))
        set(gca,'FontName','Helvetica','FontSize',6);                
    % Save the figure 
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\SuppFigure5\SuppFig5c_D_vs_LongAxisPosition'];
        % Choose the figure size and position
            figHandle = figure(figureNumDVsLongAxisPosition);
            figHandle.Position =  [100   615  175   135];
        % Save the figure in PDF format         
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])   
  
% Plot alpha vs position along Long axis
    % Make the plot       
        figure(figureNumAlphaVsLongAxisPosition)
        scatter(mean_Long_axis_pos_um(tracks2Plot),...
            MSDFitResults.alphaVal_FitMeanByTrack(tracks2Plot),6,...
            'o','MarkerFaceColor',experimentColor(1:3),...
            'MarkerEdgeColor','none',...
            'MarkerFaceAlpha',alphaByTrack) 
    % Clean up the plot
        box on;
        xlim([-5 5])
        ylim([-2.5 2.5])
        xticks(-5:5)
        yticks(-2:2)
        title({'Fit parameters','across all tracks'})
        xlabel({'Mean position along Long axis (\mu m)','New pole to right'})
        ylabel({'Power law exponent','(\alpha, unitless)'})
        set(gca,'FontName','Helvetica','FontSize',6);                
    % Save the figure 
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'Figure_Panels\SuppFigure5\SuppFig5d_alpha_vs_LongAxisPosition'];
        % Choose the figure size and position
            figHandle = figure(figureNumAlphaVsLongAxisPosition);
            figHandle.Position =  [100   615  150   130];
        % Save the figure in PDF format         
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])   
