%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [velocity_autocorrelation_nm2_per_ms2] = calculatePredictedFLMVelocityAutocorrelation_Continuous(timelag_for_velocity_autocorr_ms,kT,gammaVal,alphaVal)

% This function calculates the predicted velocity autocorrelation 
% *assuming instantaneous (continuous, not discrete) velocity measurements* 
% for time lags specified in timelag_for_velocity_autocorr_ms for a
% particle with viscous drag coefficient gammaVal, thermal energy kT, and
% an anomolous MSD scaling exponent of alphaVal undergoing fractional
% Lengevin motion

% Inputs: 
    % timelag_for_velocity_autocorr_ms = A vector containing the time
        % offsets in ms used to calculate the autocorrelation
    % kT = A scalar storing the thermal energy k_B T in pN nm
    % gammaVal = A scalar storing the viscous drag coefficient of the
        % particle in pN ms / nm
    % alphaVal = A scalar storing the MSD vs time power law exponent
        % (unitless)
% Outputs: 
    % velocity_autocorrelation_nm2_per_ms2 = A vector of predicted velocity
        % autocorrelation values for the time lags specified in 
        % timelag_for_velocity_autocorr_ms

velocity_autocorrelation_nm2_per_ms2 = -(3*kT/gammaVal)*...
    (sin(alphaVal*pi)/(pi*(2-alphaVal)))*...
    timelag_for_velocity_autocorr_ms.^(alphaVal-2);

end