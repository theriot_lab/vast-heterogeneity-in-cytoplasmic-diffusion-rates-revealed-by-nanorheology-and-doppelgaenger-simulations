%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [velocity_autocorrelation_nm2_per_ms2] = calculatePredictedFLMVelocityAutocorrelation_Discrete(timelag_for_velocity_autocorr_ms,kT,gammaVal,alphaVal,temporalDiscretizationForVelocity_ms)

% This function calculates the predicted velocity autocorrelation 
% *assuming discrete, possibly overlapping, velocity measurements* 
% for time lags specified in timelag_for_velocity_autocorr_ms for a
% particle with viscous drag coefficient gammaVal, thermal energy kT, and
% an anomolous MSD scaling exponent of alphaVal undergoing fractional
% Lengevin motion

% Inputs: 
    % timelag_for_velocity_autocorr_ms = A vector containing the time
        % offsets in ms used to calculate the autocorrelation
    % kT = A scalar storing the thermal energy k_B T in pN nm
    % gammaVal = A scalar storing the viscous drag coefficient of the
        % particle in pN ms / nm
    % alphaVal = A scalar storing the MSD vs time power law exponent
        % (unitless)
    % temporalDiscretizationForVelocity_ms
% Outputs: 
    % velocity_autocorrelation_nm2_per_ms2 = A vector of predicted velocity
        % autocorrelation values for the time lags specified in 
        % timelag_for_velocity_autocorr_ms

% Start by calculating the velocity autocorrelation assuming
% instantaneous velocity measurements        
    velocity_autocorrelation_nm2_per_ms2_instantaneous = ...
        calculatePredictedFLMVelocityAutocorrelation_Continuous(...
        timelag_for_velocity_autocorr_ms,kT,gammaVal,alphaVal);
        
% Calculate the inverse time lag in frames         
    inverseTimeLag_frames = temporalDiscretizationForVelocity_ms./...
        timelag_for_velocity_autocorr_ms;
        
% Correct the velocity autocorrelation to account for the fact that the
% velocity measurements are made from discrete data 
    % (for time lag >= velocity discretiztion)
        velocity_autocorrelation_nm2_per_ms2 = ...
            velocity_autocorrelation_nm2_per_ms2_instantaneous.*...
            ((2-(1-inverseTimeLag_frames).^alphaVal-(1+inverseTimeLag_frames).^alphaVal)./...
            (inverseTimeLag_frames.^2.*alphaVal*(1-alphaVal)));
    % (for time lag == 0)
        velocity_autocorrelation_nm2_per_ms2(isinf(inverseTimeLag_frames))  = (3*kT/gammaVal)*...
        (sin(alphaVal*pi)/(pi*(1-(alphaVal/2))*(1-alphaVal)*alphaVal))*...
        (temporalDiscretizationForVelocity_ms).^(alphaVal-2);
    % (for time lag < velocity discretiztion)
        timeLagsSmallerThanVelDisc = (((inverseTimeLag_frames>1))&((inverseTimeLag_frames<Inf)));
        if any(timeLagsSmallerThanVelDisc)
            velocity_autocorrelation_nm2_per_ms2(timeLagsSmallerThanVelDisc) = ...
                (velocity_autocorrelation_nm2_per_ms2_instantaneous(timeLagsSmallerThanVelDisc).*...
                ((2+(inverseTimeLag_frames(timeLagsSmallerThanVelDisc)-1).^alphaVal-...
                (inverseTimeLag_frames(timeLagsSmallerThanVelDisc)+1).^alphaVal)./...
                (inverseTimeLag_frames(timeLagsSmallerThanVelDisc).^2.*alphaVal*(1-alphaVal)))) + ...
                ((3*kT/gammaVal)*(sin(alphaVal*pi)/(pi*(1-(alphaVal/2))*(1-alphaVal)*alphaVal))*...
                ((1/temporalDiscretizationForVelocity_ms).^2)*...
                (temporalDiscretizationForVelocity_ms-...
                timelag_for_velocity_autocorr_ms(timeLagsSmallerThanVelDisc)).^alphaVal);
        end

end