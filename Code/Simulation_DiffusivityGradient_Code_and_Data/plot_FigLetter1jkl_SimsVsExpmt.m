%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% Clear the system
    close all;
    clear all;    

% Choose and load the comparisons to be analyzed and plotted     

    % Load the experimental data

        % Choose the file path to the data containing the results
            experimentalFilePath = ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
            'All_Controls_Combined_Analyzed.mat']; 
            Experimental_Results = load(experimentalFilePath);

        % Redo the analysis    
            % Reset the conditions to be all one condition
                Experimental_Results.conditionIDByCell = ...
                    ones(size(Experimental_Results.conditionIDByCell));
                Experimental_Results.conditionIDByTrack = ...
                    ones(size(Experimental_Results.conditionIDByTrack));
                Experimental_Results.numConditions = 1;    
            % Perform the MSD analysis        
                % Perform the time-averaged MSD vs time
                    [Experimental_Results] = ...
                        performMSD_AverageByTrackCellAndCondition(Experimental_Results);            
                % Fit the MSD data
                    [Experimental_Results] = ...
                        performMSDFit_AverageByTrackCellAndCondition(Experimental_Results);
            % Rename the MSD fitting results    
                MSDFitResultsNew = Experimental_Results.MSDFitResults;

        % Load the condition information    
            conditionIDByTrack = Experimental_Results.conditionIDByTrack;
            conditionIDByCell = Experimental_Results.conditionIDByCell;            

        % Add the data to the structure
            groupNames = {'Experiment'};
            groupIDsByTrack = ones(size(conditionIDByTrack));
            groupIDsByCell = ones(size(conditionIDByCell));
            MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack = ...
                MSDFitResultsNew.D_app_Val_nm2_per_ms_FitMeanByTrack;
            MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell = ...
                MSDFitResultsNew.D_app_Val_nm2_per_ms_FitMeanByCell;

    % Load the simulation data
        % Choose the folder path containng the simulation results
            simulationsFolderPath = ['Garner_Molines_et_al\'...
                'DiffusivityGradient_Code\CellAndSpaceVar_ToggleGrad_20240612_2\'];
        % Choose the parameter indices for each simulation we want to pull out
            % cols: fracCellVar2SimNum fracSpaceVar2SimNum
            % viscosityDomainSizeInNM2SimNum replicateNum ToggleGradNum
            % rows: original; interpolated; interpolate + diffusivity gradient 
                 simIdx2PullOut = [1 1 1 3 1; 1 1 1 2 2; 1 1 1 3 3];
        % Load an example file to get some information about the dataset
            % Choose example file
                folderInfo = dir(simulationsFolderPath);
                fileName = folderInfo(3).name;
                exampleFilePath = [simulationsFolderPath fileName];
            % Load the parameter information    
                load(exampleFilePath,'fracCellVar2SimNums')
                load(exampleFilePath,'fracSpaceVar2SimNums')
                load(exampleFilePath,'viscosityDomainSizeInNM2SimNums')
                load(exampleFilePath,'ToggleGradNums')
                load(exampleFilePath,'replicateNums')
                load(exampleFilePath,'ParamScanName')
                load(exampleFilePath,'dateNum')  
        for simNum = 1:size(simIdx2PullOut,1)
            % Find which simulation we want
                sumNum = find((fracCellVar2SimNums==simIdx2PullOut(simNum,1))&...
                    (fracSpaceVar2SimNums==simIdx2PullOut(simNum,2))&...
                    (viscosityDomainSizeInNM2SimNums==simIdx2PullOut(simNum,3))&...
                    (replicateNums==simIdx2PullOut(simNum,4))&...
                    (ToggleGradNums==simIdx2PullOut(simNum,5)));
                % Determine the filename
                    fileName = sprintf('Results_ExpStatsAndLength_%s_%s_%i.mat',...
                        ParamScanName,dateNum,sumNum);
                    filePath = [simulationsFolderPath fileName]; 
            % Load the relevant information
                % Load the dataset name                    
                    load(filePath,'datasetName')
               % Re-analyze the data
                    Simulation_Results = load(filePath);        
                    % Reset the conditions to be all one condition
                        Simulation_Results.conditionIDByCell = ...
                            ones(size(Simulation_Results.conditionIDByCell));
                        Simulation_Results.conditionIDByTrack = ...
                            ones(size(Simulation_Results.conditionIDByTrack));
                        Simulation_Results.numConditions = 1;
                    % Perform the time-averaged MSD vs time
                        [Simulation_Results] = ...
                            performMSD_AverageByTrackCellAndCondition(Simulation_Results);                        
                    % Fit the MSD data
                        [Simulation_Results] = ...
                            performMSDFit_AverageByTrackCellAndCondition(Simulation_Results);
                    % Keep the MSD fitting results as a new variable   
                        MSDFitResultsNew = Simulation_Results.MSDFitResults;
                    % Save the file
                       % parsave(Simulation_Results)
                    % Clear the variable
                        clear Simulation_Results
            % Add the data to the structure
                groupNames = [groupNames, datasetName];
                groupIDsByTrack = [groupIDsByTrack; (simNum+1)*ones(size(conditionIDByTrack))];
                groupIDsByCell = [groupIDsByCell; (simNum+1)*ones(size(conditionIDByCell))];
                MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack = ...
                    [MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack;...
                    MSDFitResultsNew.D_app_Val_nm2_per_ms_FitMeanByTrack];
                MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell = ...
                    [MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell; ...
                    MSDFitResultsNew.D_app_Val_nm2_per_ms_FitMeanByCell];
        end

    % Choose the group names for plotting (by looking at the names)
        groupNames2Plot = {'Expmt', 'Original', 'Smoothed','Smoothed+Diffusivity Gradient'};

    % Choose the control treatment groups (by looking at the names)
        groupNames
        controlGroupNum = 1;
        perturbationGroupNums = [2 3 4];

    % Count the number of perturbations
        numPerturbations = length(perturbationGroupNums);
        numGroups = numPerturbations + 1;

% Calculate p values for test of equality of means/variances by track

    % Preallocate space to store the p values and stats
        pD_sigma_ByTrack = nan(numPerturbations,1);
        statsD_sigma_ByTrack = cell([numPerturbations,1]);
        pD_mu_ByTrack = nan(numPerturbations,1);
        tblD_mu_ByTrack = cell([numPerturbations,1]);
        statsD_mu_ByTrack = cell([numPerturbations,1]);
        pD_sigma_ByCell = nan(numPerturbations,1);
        statsD_sigma_ByCell = cell([numPerturbations,1]);
        pD_mu_ByCell = nan(numPerturbations,1);
        tblD_mu_ByCell = cell([numPerturbations,1]);
        statsD_mu_ByCell = cell([numPerturbations,1]);

    for perturbationGroupNum = 1:numPerturbations

        % Perform the analysis on the track-wise data

            % Pull out the data to analyze
                tracks2Analyze = ((groupIDsByTrack==controlGroupNum)|...
                    groupIDsByTrack==perturbationGroupNums(perturbationGroupNum));
                log10D2Analyze = log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack(tracks2Analyze));
                groupIDs2Analyze = groupIDsByTrack(tracks2Analyze);
    
            % Perform a nonparametric test of equal variance
                % Diffusivity
                    [pD_sigma_ByTrack(perturbationGroupNum),...
                        statsD_sigma_ByTrack{perturbationGroupNum}] = ...
                            vartestn(log10D2Analyze,groupIDs2Analyze,...
                            'Display','on','TestType','LeveneAbsolute');       
    
            % Perform a nonparametric test of equal medians % distributions
                % Diffusivity
                    [pD_mu_ByTrack(perturbationGroupNum),...
                        tblD_mu_ByTrack{perturbationGroupNum},...
                        statsD_mu_ByTrack{perturbationGroupNum}] = ...
                            ranksum(log10D2Analyze(groupIDs2Analyze==controlGroupNum),...
                                log10D2Analyze(groupIDs2Analyze==...
                                perturbationGroupNums(perturbationGroupNum)));         

        % Perform the analysis on the cell-wise data

            % Pull out the data to analyze
                tracks2Analyze = ((groupIDsByCell==controlGroupNum)|...
                    groupIDsByCell==perturbationGroupNums(perturbationGroupNum));
                log10D2Analyze = log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell(tracks2Analyze));
                groupIDs2Analyze = groupIDsByCell(tracks2Analyze);
    
            % Perform a nonparametric test of equal variance
                % Diffusivity
                    [pD_sigma_ByCell(perturbationGroupNum),...
                        statsD_sigma_ByCell{perturbationGroupNum}] = ...
                            vartestn(log10D2Analyze,groupIDs2Analyze,...
                            'Display','on','TestType','LeveneAbsolute');           
    
            % Perform a nonparametric test of equal medians % distributions
                % Diffusivity
                    [pD_mu_ByCell(perturbationGroupNum),...
                        tblD_mu_ByCell{perturbationGroupNum},...
                        statsD_mu_ByCell{perturbationGroupNum}] = ...
                            ranksum(log10D2Analyze(groupIDs2Analyze==controlGroupNum),...
                                log10D2Analyze(groupIDs2Analyze==...
                                perturbationGroupNums(perturbationGroupNum)));                       
                
    end
   
    close all;
    close all hidden; % Close stubborn tables and graphs


%% Make box plots with significance stars

    % Reformat the data for the box plots
        % Determine the maximum number of tracks on each group
            uniqueGroupIDs = unique(groupIDsByTrack); 
            numTracksPerGroup = histc(groupIDsByTrack,uniqueGroupIDs);
            numCellsPerGroup = histc(groupIDsByCell,uniqueGroupIDs);
        % Pre-allocate space to store the data
            % Reformatted matrices
                D2Plot_ByTrack = nan(max(numTracksPerGroup),1,numGroups);
                D2Plot_ByCell = nan(max(numCellsPerGroup),1,numGroups);
            % Means and variances            
                D2Plot_ByTrack_mean = nan(1,numGroups);
                D2Plot_ByTrack_var = nan(1,numGroups);
                D2Plot_ByCell_mean = nan(1,numGroups);
                D2Plot_ByCell_var = nan(1,numGroups);
    
    % Load the track data into a single matrix
    for groupNum = 1:numGroups

        % Cell-wise data
            % Pull out the data to plot
                tracks2Analyze = (groupIDsByTrack==groupNum);
                log10D2Analyze = log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByTrack(tracks2Analyze));       
            % Fill in the matrix
                D2Plot_ByTrack(1:numTracksPerGroup(groupNum),1,groupNum) = log10D2Analyze;
            % Record the mean diffusivity
                D2Plot_ByTrack_mean(groupNum) = nanmean(log10D2Analyze);
                D2Plot_ByTrack_var(groupNum) = nanstd(log10D2Analyze)^2;

        % Track-wise data
            % Pull out the data to plot
                tracks2Analyze = (groupIDsByCell==groupNum);
                log10D2Analyze = log10(MSDFitResults.D_app_Val_nm2_per_ms_FitMeanByCell(tracks2Analyze));
            % Fill in the matrix
                D2Plot_ByCell(1:numCellsPerGroup(groupNum),1,groupNum) = log10D2Analyze;
            % Record the mean diffusivity
                D2Plot_ByCell_mean(groupNum) = nanmean(log10D2Analyze);
                D2Plot_ByCell_var(groupNum) = nanstd(log10D2Analyze)^2;
    end       

    % Print the relative difference from the mean
        PercentChangeInLogDMean_ByTrack = ...
            (D2Plot_ByTrack_mean(perturbationGroupNums)-...
            D2Plot_ByTrack_mean(controlGroupNum)).*100./...
            D2Plot_ByTrack_mean(controlGroupNum)
        PercentChangeInLogDVar_ByTrack = ...
            (D2Plot_ByTrack_var(perturbationGroupNums)-...
            D2Plot_ByTrack_var(controlGroupNum)).*100./...
            D2Plot_ByTrack_var(controlGroupNum)
        PercentChangeInLogDMean_ByCell = ...
            (D2Plot_ByCell_mean(perturbationGroupNums)-...
            D2Plot_ByCell_mean(controlGroupNum)).*100./...
            D2Plot_ByCell_mean(controlGroupNum)
        PercentChangeInLogDVar_ByCell = ...
            (D2Plot_ByCell_var(perturbationGroupNums)-...
            D2Plot_ByCell_var(controlGroupNum)).*100./...
            D2Plot_ByCell_var(controlGroupNum)

    % Choose the plotting parameters
        % Choose the plot locations
            figureNumBoxPlotsVariances = 1;
            figureNumBoxPlotsMeans = 2;
            numCols = 1;
            numRows = 2;
            numSubplotDByTrack = 1;
            numSubplotDByCell = 2;
        % Subset the temperature data only
            groupOrder2Plot = [controlGroupNum perturbationGroupNums];
            groupNames(groupOrder2Plot)
            comparisonOrder2Plot = 1:numPerturbations;
        % Choose which color to plot the conditions    
            CMByGroup = jet(numGroups)*0.75;
            CMByGroup(1,:) = [0.1 0.1 0.9]; 
            CMByGroup(1,:) = [0.37 0.42 1]; 
            CMByGroup(2:end,:) = repmat([0 1 0]*0.75,[3, 1]);
        % Choose the dot size
            dotSizeTrack = 5;
            dotSizeCell = 5;
        % Choose the alpha values
            alphaValsTracks = 0.1*ones(size(groupOrder2Plot));
            alphaValsCells = 0.9*ones(size(groupOrder2Plot));

    % Make box plots with significance stars for the test of equal variance 

        % Plot the track-wise diffusivity, where stars are test of equal variance    
            [hBoxPlot_DByTrack, hSigStars_DByTrack] = ...
                plotBoxPlotsWithSigStarsDiffusivityByTrack(figureNumBoxPlotsVariances,numRows,numCols,...
                    numSubplotDByTrack,D2Plot_ByTrack(:,:,groupOrder2Plot),...
                    pD_sigma_ByTrack(comparisonOrder2Plot),CMByGroup,...
                    groupNames2Plot(groupOrder2Plot),dotSizeTrack,alphaValsTracks);
            box on;
            for numObj = 1:(length(hBoxPlot_DByTrack.handles.groupsTxt)-1)
                set(hBoxPlot_DByTrack.handles.groupsTxt(numObj),'Rotation',-10)
            end
            yticks(0:5)
            title({'Distribution of diffusivities','from track-wise MSD fits'})
            set(gca,'Xlim',[0.6 1.4])
            set(gca,'FontName','Helvetica','FontSize',5);  
        % Plot the cell-wise diffusivity, where stars are test of equal variance    
            [hBoxPlot_DByCell, hSigStars_DByCell] = ...
                plotBoxPlotsWithSigStarsDiffusivityByCell(figureNumBoxPlotsVariances,numRows,numCols,...
                    numSubplotDByCell,D2Plot_ByCell(:,:,groupOrder2Plot),...
                    pD_sigma_ByCell(comparisonOrder2Plot),CMByGroup,...
                    groupNames2Plot(groupOrder2Plot),dotSizeCell,alphaValsCells);
            box on;
            for numObj = 1:(length(hBoxPlot_DByCell.handles.groupsTxt)-1)
                set(hBoxPlot_DByCell.handles.groupsTxt(numObj),'Rotation',-10)
            end
            yticks(0:5)
            title({'Distribution of diffusivities','from cell-wise MSD fits'})
            set(gca,'Xlim',[0.6 1.4])
            set(gca,'FontName','Helvetica','FontSize',5); 
                
        % Save the figure
            % Choose the filename and path for the figure
                destinationTrack = ['Garner_Molines_et_al\'...
                'DiffusivityGradient_Code\Fig1ki_ExpmtVsSim_BoxPlots_StarsAreVariance'];
            % Choose the figure size and position
                figHandle = figure(figureNumBoxPlotsVariances);
                figHandle.Position =  [100   435   250   310];
            % Save the file as a pdf
                exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
            % Save the figure in matlab figure format
                savefig([destinationTrack '.fig'])           


%% Make a plot of the medians 
    figure(3)
        % Calculate the median and standard error
            medianD = nanmedian(10.^squeeze(D2Plot_ByTrack(:,:,groupOrder2Plot)),1);
            numD = sum(~isnan(10.^squeeze(D2Plot_ByTrack(:,:,groupOrder2Plot))),1);
            stdD = sqrt(nansum((10.^squeeze(D2Plot_ByTrack(:,:,groupOrder2Plot)) - medianD).^2,1)./numD);
            seD = stdD./sqrt(numD);
        % Make the bar plots
            b = bar(medianD);
            b.FaceColor = 'flat';
            b.CData = CMByGroup;
            hold on;
            errorbar(medianD,seD,'k.')
        % Choose the axes limits and label the graph
            set(gca,'XTickLabel',groupNames2Plot(groupOrder2Plot))
            yticks(0:100:500)
            % Choose the x axes limits
                spaceBetweenBars = mean(diff(b.XData));
                xlim([(min(b.XData) - (spaceBetweenBars*3/4)) ...
                    (max(b.XData) + (spaceBetweenBars*3/4))]);
            title({'Track-averaged diffusivity'})
            ylabel({'Diffusivity (D, nm^2/ms)','Median \pm SE Median'})
            ylim([0 450])
            set(gca,'FontName','Helvetica','FontSize',5);     
    % Plot the significance stars
        % Calculate the locations for the bars and sig stars    
            [groups2Compare] = [ones(numPerturbations,1), ((1:numPerturbations)'+1)];
        % Plot the sig stars
            hSigStars = sigstar(num2cell(groups2Compare,2),pD_mu_ByTrack);
            hold off;
    
    % Save the figure
        % Choose the filename and path for the figure
            destinationTrack = ['Garner_Molines_et_al\'...
                'DiffusivityGradient_Code\Fig1j_ExpmtVsSim_BarPlots'];
        % Choose the figure size and position
            figHandle = figure(3);
            figHandle.Position =  [200   500   215   140];
        % Save the file as a pdf
            exportgraphics(gcf,[destinationTrack '.pdf'],'ContentType','vector')
        % Save the figure in matlab figure format
            savefig([destinationTrack '.fig'])
