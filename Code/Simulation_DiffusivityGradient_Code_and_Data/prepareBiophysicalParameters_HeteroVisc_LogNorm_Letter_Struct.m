%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [biophysicalParams] = prepareBiophysicalParameters_HeteroVisc_LogNorm_Letter_Struct(simulationDatasetInfo,Experiment_Measurement_Info,cellNum,treatmentNum)

%% Choose the biophysical parameters

    % Choose the cell length across each dimension in nm
        % If boundary is the cell
            cellSize_nm = [Experiment_Measurement_Info.DataOrganizedByCell(cellNum).Length*10^3 3000];   
        % If boundary is pore size of cytoplasm    
             %cellSize_nm = 2*[1000 1000];  
             
    % Choose the viscosity domain size in nm
        viscosiyDomainSize_nm =  simulationDatasetInfo.viscosityDomainSizeInNM2Sim;

    % Choose the domain size for the viscosity gradient
        domainSize_nm_grad = 10;
        
    % Choose the radius of the particles in nm
        particleRadius = 20;
        
    % Choose the properties governing thermal energy in pN nm
        % Boltzman constant in pN nm / K
            k_B = 0.0138;
        % Temperature in C
            T_C = 30;
        % Calcuate the temperature in K
            T_K = 273.15 + T_C;
        % Calculate kT in pN nm
            kT = k_B*T_K;
            
    % Choose the properties governing viscous drag on the particle 
        % Calculate the (dynamic) viscosity of water at this temperature in
        % pN ms / nm^2
            etaValWater = 2.414*10^(-8)*10^(247.8/(T_K-140));
        % Choose the fold increase in viscosity of the cytoplasm relative to
        % water
            etaValMult = simulationDatasetInfo.treatmentParameterVals(treatmentNum);
        % Select a cell-averaged viscosity randomly from a log-normal 
        % distribution   
            etaVal = etaValWater*etaValMult*exp(simulationDatasetInfo.fracCellVar2Sim*randn);                
        % Don't allow the viscosity to drop below that of water
            etaVal(etaVal<etaValWater) = etaValWater;
        % Calculate the Stokes drag for this particle size in pN ms / nm
            gammaVal = 6*pi*etaVal*particleRadius;    
        % Select the individual viscosity and Stokes drag for each domain 
            % Viscosity by domain
                % Pre-allocate space
                    etaValsByBox = nan(ceil(cellSize_nm/viscosiyDomainSize_nm));
                % Select each viscosity values from a log-normal
                % distribution
                    etaValsByBox(:) = etaVal.*exp(simulationDatasetInfo.fracSpaceVar2Sim*...
                        randn(length(etaValsByBox(:)),1));                
                % Don't allow the viscosity to drop below that of water
                    etaValsByBox(etaValsByBox<etaValWater) = etaValWater;
            % Stokes drag by domain in pN ms / nm       
                gammaValsByBox = 6*pi*etaValsByBox*particleRadius;
                
    % Calculate the diffusion coefficient in nm^2/ms
        DVal = kT/gammaVal;
        DValsByBox = kT./gammaValsByBox;

    % Calculate the spatial diffusivity gradient in nm/ms
        % Interpolate the diffusivity 
            DValsByBox_Interp = 10.^imresize(log10(DValsByBox),[ceil(cellSize_nm/domainSize_nm_grad)],'bicubic');
            gammaValsByBox_Interp = kT./DValsByBox_Interp;
        % Calculate the gradient in nm/ms
            % Compute the gradient (assumes box size 1)
                [DValsByBox_Grad_Cols,DValsByBox_Grad_Rows] = gradient(DValsByBox_Interp);
            % Correct the units            
                DValsByBox_Grad_Cols = DValsByBox_Grad_Cols./domainSize_nm_grad;
                DValsByBox_Grad_Rows = DValsByBox_Grad_Rows./domainSize_nm_grad;
        
        % figure(1)
        % subplot(2,2,1)
        % rowVals = (viscosiyDomainSize_nm/2):viscosiyDomainSize_nm:cellSize_nm(1);
        % colVals = (viscosiyDomainSize_nm/2):viscosiyDomainSize_nm:cellSize_nm(2);
        % imagesc(rowVals,colVals,DValsByBox')
        % title({'Diffusivity (nm^2/ms)', sprintf('Viscosity Domain Size %1.1d nm',viscosiyDomainSize_nm),...
        %     sprintf('Grid Domain Size %1.1d nm',viscosiyDomainSize_nm)})
        % colorbar
        % subplot(2,2,2)
        % rowVals = (domainSize_nm_grad/2):domainSize_nm_grad:cellSize_nm(1);
        % colVals = (domainSize_nm_grad/2):domainSize_nm_grad:cellSize_nm(2);
        % imagesc(rowVals,colVals,DValsByBox_Interp')
        % title({'Interpolated Diffusivity (nm^2/ms)', sprintf('Viscosity Domain Size %1.1d nm',viscosiyDomainSize_nm),...
        %     sprintf('Grid Domain Size %1.1d nm',domainSize_nm_grad)})
        % colorbar
        % subplot(2,2,3)
        % imagesc(rowVals,colVals,DValsByBox_Grad_Cols')
        % title('Interpolated Diffusivity Gradient (Y) (nm/ms)')
        % colorbar
        % subplot(2,2,4)
        % imagesc(rowVals,colVals,DValsByBox_Grad_Rows')
        % title('Interpolated Diffusivity Gradient (X)  (nm/ms)')
        % colorbar

    % Calculate the standard deviation of the Brownian force in pN
        xi = sqrt(2*kT*gammaVal/Experiment_Measurement_Info.msPerTimeStep);
        xiValsByBox = sqrt(2*kT*gammaValsByBox/Experiment_Measurement_Info.msPerTimeStep);  
        xiValsByBox_Interp = sqrt(2*kT*gammaValsByBox_Interp/Experiment_Measurement_Info.msPerTimeStep);  


% Save the results
    biophysicalParams = v2struct();
            
            