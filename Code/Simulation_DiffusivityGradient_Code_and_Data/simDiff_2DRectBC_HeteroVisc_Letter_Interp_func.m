%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [trackPositions_nm] = simDiff_2DRectBC_HeteroVisc_Letter_Interp_func(numTracks,...
    numDimensions,numTimepoints,msPerTimeStep,boundarySize_nm,...
    domainSize_nm_grad,gammaValsByBox_Interp,xiValsByBox_Interp)

    % Set up the simulation     
    
        % Preallocate space to store temporary variables
            boxByDimension_Interp_subsIdx = nan(numTracks,numDimensions);
            boxByDimension_Interp_linIdx = nan(numTracks,1);
            brownianForce_pN = nan(numTracks,numDimensions);
            velocity_nm_per_ms = nan(numTracks,numDimensions);
            gammaValsByTrack = nan(numTracks,1);
            xiValsByTrack = nan(numTracks,1);
    
        % Initialize the particle positions randomly between
        % x=0 and x=boundarySize_nm)
            trackPositions_nm = nan(numTracks,numDimensions,numTimepoints);
            trackPositions_nm(1:numTracks,1:numDimensions,1) = bsxfun(@times,...
                rand([numTracks numDimensions]), boundarySize_nm);
            
        % Create the multiplier to convert from subscript to linear indices
        % (for determining which domain each track is in)
        % (Each increase in subscript along dimension n ads s_1*s_2*...*s_(n-1) 
        % to the linear index)
            numDomainsEachDimension = size(gammaValsByBox_Interp);
            sub2LinIdxMultiplier = nan(1,numDimensions);
            sub2LinIdxMultiplier(1) = 1;
            for dimensionNum = 2:numDimensions
                sub2LinIdxMultiplier(dimensionNum) = ...
                    prod(numDomainsEachDimension(1:(dimensionNum-1)));
            end

    % Run the simulation

        for numTimepoint = 1:(numTimepoints-1)

            % Update the parameters based on which box the particles are in

                % Figure out which domain each particle is in
                    % Determine the subscript index along each dimension
                        boxByDimension_Interp_subsIdx(:) = ceil(trackPositions_nm(:,:,...
                            numTimepoint)./domainSize_nm_grad);                    
                    % Use the multiplier to convert from subcript index to linear index 
                        boxByDimension_Interp_linIdx(:) = sum((boxByDimension_Interp_subsIdx-1).*...
                            sub2LinIdxMultiplier,2)+1;    

                % Calculate the remaining simulation parameters for each 
                % track based on which domain each particle is in
                     % Pull out the viscosity for each track
                        gammaValsByTrack(:) = gammaValsByBox_Interp(boxByDimension_Interp_linIdx);
                     % Pull out the sd of the Brownian force for this track
                        xiValsByTrack(:) = xiValsByBox_Interp(boxByDimension_Interp_linIdx);

                % Assign Brownian forces, chosen from a random normal 
                % distribution with standard deviation xi for each track
                    brownianForce_pN(:) = bsxfun(@times,...
                        randn(numTracks,numDimensions),xiValsByTrack);
                    
                % Calculate the associated velocity
                    velocity_nm_per_ms(:) = bsxfun(@rdivide,...
                        brownianForce_pN,gammaValsByTrack);      

            % Update the particles' positions
                trackPositions_nm(:,:,numTimepoint+1) = ...
                    trackPositions_nm(:,:,numTimepoint) + ...
                    (velocity_nm_per_ms*msPerTimeStep);
                
                if any(~isreal(trackPositions_nm(:)))
                    beep
                    'Something bad happened'
                   break 
                end
                
            % Implement the reflective boundary conditions    
            while any(any((trackPositions_nm(:,:,numTimepoint+1)<0)|...
                    bsxfun(@gt,trackPositions_nm(:,:,numTimepoint+1),boundarySize_nm)))

                % Reflect the particles that pass the left or bottom wall (position<0)
                    trackPositions_nm(:,:,numTimepoint+1) = ...
                        trackPositions_nm(:,:,numTimepoint+1) + ...
                        ((trackPositions_nm(:,:,numTimepoint+1)<0).*...
                        (-2*trackPositions_nm(:,:,numTimepoint+1)));

                % Reflect the particles that pass the right or top wall (position>cellSize)
                    trackPositions_nm(:,:,numTimepoint+1) = ...
                        trackPositions_nm(:,:,numTimepoint+1) + ...
                        (bsxfun(@gt,trackPositions_nm(:,:,numTimepoint+1),boundarySize_nm).*...
                        (-2*bsxfun(@minus,trackPositions_nm(:,:,numTimepoint+1),boundarySize_nm)));
            end

        end
        