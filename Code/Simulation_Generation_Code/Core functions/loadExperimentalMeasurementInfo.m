%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [Experiment_Measurement_Info] = loadExperimentalMeasurementInfo(experimentalDatasetFilePath)


        % Load the experimental tracks
            load(experimentalDatasetFilePath,'trackPositions_nm');
            experimentalTracks = trackPositions_nm;
            clear trackPositions_nm
        % Load the relevant cell length information            
            load(experimentalDatasetFilePath,'DataOrganizedByCell');           
            DataOrganizedByCell = struct('Length',{DataOrganizedByCell(:).Length});
        % Load the measurement information
            load(experimentalDatasetFilePath,'numTracks');
            load(experimentalDatasetFilePath,'numDimensions');
            load(experimentalDatasetFilePath,'dimensionNames')
            load(experimentalDatasetFilePath,'numTimepoints');
            load(experimentalDatasetFilePath,'timePoints_ms');
            load(experimentalDatasetFilePath,'numCells');
        % Load the condition information (for plotting only)
            load(experimentalDatasetFilePath,'numConditions');
            load(experimentalDatasetFilePath,'uniqueConditions');
            load(experimentalDatasetFilePath,'conditionIDByTrack');
            load(experimentalDatasetFilePath,'conditionIDByCell');
        % Load the IDs
            load(experimentalDatasetFilePath,'imagingSessionIDByTrack');
            load(experimentalDatasetFilePath,'imagingSessionIDByCell');
            load(experimentalDatasetFilePath,'dayIDByTrack');
            load(experimentalDatasetFilePath,'dayIDByCell');
            load(experimentalDatasetFilePath,'localizationIDByTrack');
            load(experimentalDatasetFilePath,'localizationIDByCell');
            load(experimentalDatasetFilePath,'treatmentIDByTrack');
            load(experimentalDatasetFilePath,'treatmentIDByCell');
            load(experimentalDatasetFilePath,'cellIDByTrack');
        % Load the timestep
            load(experimentalDatasetFilePath,'msPerTimeStep');
        
    % Load the data into a structure    
        Experiment_Measurement_Info = v2struct();        