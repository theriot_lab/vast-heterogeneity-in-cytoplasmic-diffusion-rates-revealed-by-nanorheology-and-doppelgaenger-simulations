%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

function [Simulation_Results] = packageSimulationResults(globalSimulationDatasetInfo,Experiment_Measurement_Info,simulationDatasetInfo,biophysicalParams,trackPositions_nm)

    v2struct(globalSimulationDatasetInfo)
    v2struct(Experiment_Measurement_Info)
    v2struct(simulationDatasetInfo)
    v2struct(biophysicalParams)

    Simulation_Results = v2struct();
    Simulation_Results.trackPositions_nm = trackPositions_nm;