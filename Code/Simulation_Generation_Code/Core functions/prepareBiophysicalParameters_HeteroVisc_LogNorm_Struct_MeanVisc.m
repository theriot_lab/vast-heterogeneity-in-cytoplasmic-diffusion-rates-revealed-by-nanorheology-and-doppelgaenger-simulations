function [biophysicalParams] = prepareBiophysicalParameters_HeteroVisc_LogNorm_Struct_MeanVisc(simulationDatasetInfo,Experiment_Measurement_Info,cellNum,treatmentNum)

%% Choose the biophysical parameters

    % Choose the cell length across each dimension in nm
        % If boundary is the cell
            cellSize_nm = [Experiment_Measurement_Info.DataOrganizedByCell(cellNum).Length*10^3 3000];   
        % If boundary is pore size of cytoplasm    
             %cellSize_nm = 2*[1000 1000];  
             
    % Choose the viscosity domain size in nm
        viscosiyDomainSize_nm =  simulationDatasetInfo.viscosityDomainSizeInNM2Sim;
        
    % Choose the radius of the particles in nm
        particleRadius = 20;
        
    % Choose the properties governing thermal energy in pN nm
        % Boltzman constant in pN nm / K
            k_B = 0.0138;
        % Temperature in C
            T_C = 30;
        % Calcuate the temperature in K
            T_K = 273.15 + T_C;
        % Calculate kT in pN nm
            kT = k_B*T_K;
            
    % Choose the properties governing viscous drag on the particle 
        % Calculate the (dynamic) viscosity of water at this temperature in
        % pN ms / nm^2
            etaValWater = 2.414*10^(-8)*10^(247.8/(T_K-140));
        % Choose the fold increase in viscosity of the cytoplasm relative to
        % water
            etaValMult = simulationDatasetInfo.meanViscosity2Sim;
        % Select a cell-averaged viscosity randomly from a log-normal 
        % distribution   
            etaVal = etaValWater*etaValMult*exp(simulationDatasetInfo.fracCellVar2Sim*randn);                
        % Don't allow the viscosity to drop below that of water
            etaVal(etaVal<etaValWater) = etaValWater;
        % Calculate the Stokes drag for this particle size in pN ms / nm
            gammaVal = 6*pi*etaVal*particleRadius;    
        % Select the individual viscosity and Stokes drag for each domain 
            % Viscosity by domain
                % Pre-allocate space
                    etaValsByBox = nan(ceil(cellSize_nm/viscosiyDomainSize_nm));
                % Select each viscosity values from a log-normal
                % distribution
                    etaValsByBox(:) = etaVal.*exp(simulationDatasetInfo.fracSpaceVar2Sim*...
                        randn(length(etaValsByBox(:)),1));                
                % Don't allow the viscosity to drop below that of water
                    etaValsByBox(etaValsByBox<etaValWater) = etaValWater;
            % Stokes drag by domain        
                gammaValsByBox = 6*pi*etaValsByBox*particleRadius;
                
    % Calculate the diffusion coefficient
        DVal = kT/gammaVal;
        DValsByBox = kT./gammaValsByBox;

    % Calculate the standard deviation of the Brownian force in pN
        xi = sqrt(2*kT*gammaVal/Experiment_Measurement_Info.msPerTimeStep);
        xiValsByBox = sqrt(2*kT*gammaValsByBox/Experiment_Measurement_Info.msPerTimeStep);  


% Save the results
    biophysicalParams = v2struct();
            
            