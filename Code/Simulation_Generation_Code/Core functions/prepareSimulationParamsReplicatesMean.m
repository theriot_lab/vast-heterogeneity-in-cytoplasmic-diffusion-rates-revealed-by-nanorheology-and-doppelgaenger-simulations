function [simulationDatasetInfo] = prepareSimulationParamsReplicatesMean(globalSimulationDatasetInfo,paramComboNum)

    % Unpack the structure
        v2struct(globalSimulationDatasetInfo);

    % Pull out the indices for each parameter
        fracCellVar2SimNum = fracCellVar2SimNums(paramComboNum);
        fracSpaceVar2SimNum = fracSpaceVar2SimNums(paramComboNum);
        viscosityDomainSizeInNM2SimNum = viscosityDomainSizeInNM2SimNums(paramComboNum); 
        meanViscosity2SimNum = meanViscosity2SimNums(paramComboNum); 
        replicateNum = replicateNums(paramComboNum); 
            
    % Load the the parameters         
        fracCellVar2Sim = fracCellVar2SimVals(fracCellVar2SimNum);
        fracSpaceVar2Sim = fracSpaceVar2SimVals(fracSpaceVar2SimNum);
        viscosityDomainSizeInNM2Sim = viscosityDomainSizeInNM2SimVals(viscosityDomainSizeInNM2SimNum);
        meanViscosity2Sim = meanViscosity2SimVals(meanViscosity2SimNum);
            
    % Choose the simulation dataset name
        datasetName = sprintf(['Simulation Results - Exp stats and cell '...
            'length - Cell var %1.1d  - %d nm viscosity '...
            'domains var %1.1d - mean viscosity %1.1d - Rep %d'],fracCellVar2Sim,...
            viscosityDomainSizeInNM2Sim,fracSpaceVar2Sim,meanViscosity2Sim,replicateNum); 
        
    % Choose a filepath to save the results
        % Create the new file name and path
            saveFileName = sprintf('Results_ExpStatsAndLength_%s_%s_%d.mat',...
                ParamScanName,dateNum,paramComboNum);
            saveFilePath = [saveFolderPath saveFileName];        

    % Save the results
        simulationDatasetInfo = v2struct();

end