%% The following code was written by Rikki Garner to generate the figures 
% in Garner and Molines et al. 2022, doi:10.1101/2022.05.11.491518

% This code simulates 1D diffusion between two barrier with reflective 
% boundary conditions using a fixed time step Brownian dynamics implementation.

%% Clear the system and reset the random number generator

% Close all figures
    close all;
    close all hidden; % ANOVA tables
% Clear all variables
    clear all; 
% Give the random number generator a random seed
% (Random number generators are actually deterministic based on 
% the initial seed given to the random number generator. The option 
% 'shuffle' sets the seed to the current time, which is pseudorandom
% unless you always start the simulation at the same exact time)
    rng('shuffle');
    
%% Choose the simulation input parameters

    % Choose the path to the experimental dataset bring replicated
        experimentalDatasetFilePath = ...
            ['Garner_Molines_et_al\Experimental_Data\Analyzed_Data\'...
            'All_Controls_Combined_Analyzed.mat']; 

    % Choose the corresponding parameter values     
        % Choose the names for each condition   
            % (If conditions are replicates)
                load(experimentalDatasetFilePath,'numConditions')
                conditionParameterVals = 1:numConditions;   
                conditionNameforSprintf = 'Replicate %d';
                conditionNameforSprintf = conditionNameforSprintf;        
                condition2plot = 1:numConditions;       
                CMByCondition = jet(numConditions)*0.7;
                uniqueConditions={};
            % Update the condition names
                for conditionNum = 1:numConditions    
                    uniqueConditions{conditionNum} = ...
                        sprintf(conditionNameforSprintf,...
                        conditionParameterVals(conditionNum));
                end

    % Choose whether to plot or save results
        doPlotMSDByCondition = false;
        doPlotMSDByCondition_CompMeanVsMed = false;
        doPlotMSDByCell = false;
        doPlotMSDByTrack = false;
        doPlotFitDistByTrack = false;
        doPlotVelAutocorrByCondition = false;
        doSaveImages = false;
        doSaveResults = true;
            


        % Choose the parameters controling cellular viscosity heterogeneity - finer parameter scan 100 nm box size 
        % Coarse box size scan
            ParamScanName = 'SpaceCellVar_vs_BoxSize_Coarse_Opt';  
            saveFolderName = 'ParameterScan_OptVarAndMean_BoxSize_Coarse_Fig4g\';
            fracCellVar2SimVals = [0.375 0.4 0.4 0.4 0];
            fracSpaceVar2SimVals = [1.1 1.0 0.9 0.8 0.775];
            meanViscosity2SimVals = [50 39 38 40.5 41.5];
            viscosityDomainSizeInNM2SimVals = [100 300 600 1000 3000];
            numReplicates = 1; 

% Prepare folder path to save results
    % Pull out the date and convert to string for folder naming convention
        str = date();
        dateNum = datestr(str,'yyyy/mm/dd');
        dateNum = dateNum(dateNum~='/');
    % Choose the parent folder path to create the new folder in
        saveFolderPath = ['Garner_Molines_et_al\Simulation_Data\' ...
            saveFolderName(1:(end-1)) '_' dateNum];
        % If the folder already exists, append an integer
        if isfolder(saveFolderPath)
            n=1;
            saveFolderPathOriginal = saveFolderPath;
            while isfolder(saveFolderPath)
                saveFolderPath = [saveFolderPathOriginal sprintf('_%i',n)];
                n=n+1;
            end
        end
        % Add the final backslash
            saveFolderPath = [saveFolderPath '\'];
        % Make the folder
            mkdir(saveFolderPath)


% Prepare indices to loop through all combinations of the listed parameters
    % Manually enter the combinations of indices  to be simulated
        paramComboIndices = [repmat(1:length(fracCellVar2SimVals),[4 numReplicates]);...
            repelem(1:numReplicates,length(fracCellVar2SimVals))];
    % Count the number of combinations
        numParamCombos = size(paramComboIndices,2);
    % Preallocate space to store the indices
        fracCellVar2SimNums = paramComboIndices(1,:);
        fracSpaceVar2SimNums = paramComboIndices(2,:);
        viscosityDomainSizeInNM2SimNums = paramComboIndices(3,:);
        meanViscosity2SimNums = paramComboIndices(4,:);
        replicateNums = paramComboIndices(5,:);

% Package the data into a structure
    globalSimulationDatasetInfo = v2struct();
    clearvars -except globalSimulationDatasetInfo

% Load the experimental data into a structure
    [Experiment_Measurement_Info] = ...
        loadExperimentalMeasurementInfo(globalSimulationDatasetInfo.experimentalDatasetFilePath); 

% Open the parallel pool
if isempty(gcp('nocreate'))
    parpool('local', 5);
end


parfor paramComboNum = 1:globalSimulationDatasetInfo.numParamCombos

    tic

    %% Set up and run the simulation

    % Reset variables
        Simulation_Results = {};
        biophysicalParams = {};
    
    % Give the random number generator a random seed
    % (Random number generators are actually deterministic based on 
    % the initial seed given to the random number generator. The option 
    % 'shuffle' sets the seed to the current time, which is pseudorandom
    % unless you always start the simulation at the same exact time)
        rng('shuffle');

    % Load all the simulation parameters for this input parameter set    
        [simulationDatasetInfo] = ...
            prepareSimulationParamsReplicatesMean(globalSimulationDatasetInfo,paramComboNum);

    % Pre-allocate space to store the particle positions
        trackPositions_nm = nan(Experiment_Measurement_Info.numTracks,...
            Experiment_Measurement_Info.numDimensions,...
            Experiment_Measurement_Info.numTimepoints);
            
    for cellNum = 1:Experiment_Measurement_Info.numCells     
    
        % Set up the simulation
        
            % Determine the treatment number
                treatmentNum = Experiment_Measurement_Info.treatmentIDByCell(cellNum);    
        
            % Prepare the biphysical parameters
                [biophysicalParams] = ...
                    prepareBiophysicalParameters_HeteroVisc_LogNorm_Struct_MeanVisc(...
                    simulationDatasetInfo,Experiment_Measurement_Info,...
                    cellNum,treatmentNum);
    
            % Pick out the tracks being simulated
                % Give the particles their IDs   
                    trackIDs = find(Experiment_Measurement_Info.cellIDByTrack==cellNum);
                % Count the number of particles
                    numTracksInCell = sum(Experiment_Measurement_Info.cellIDByTrack==cellNum);
    
        % Run the simulation                    
            [trackPositions_nm(trackIDs,:,:)] = ...
                simDiff_2DRectBC_HeteroVisc_func(numTracksInCell,...
                    Experiment_Measurement_Info.numDimensions,...
                    Experiment_Measurement_Info.numTimepoints,...
                    Experiment_Measurement_Info.msPerTimeStep, ...
                    biophysicalParams.cellSize_nm,...
                    biophysicalParams.viscosiyDomainSize_nm,...
                    biophysicalParams.gammaValsByBox,...
                    biophysicalParams.xiValsByBox);
    
        % Subtract the cell centroid
        for dimensionNum = 1:Experiment_Measurement_Info.numDimensions
            trackPositions_nm(trackIDs,dimensionNum,:) = ...
                trackPositions_nm(trackIDs,dimensionNum,:) - ...
                (biophysicalParams.cellSize_nm(dimensionNum)/2);
        end
            
    end
    
    % Remove any time points not collected in the experimental data
        trackPositions_nm(isnan(Experiment_Measurement_Info.experimentalTracks)) = nan;
    
    %% Analyze the results
        
    % Package the data into a structure    
        Simulation_Results = packageSimulationResults(globalSimulationDatasetInfo,...
            Experiment_Measurement_Info,simulationDatasetInfo,biophysicalParams,trackPositions_nm);    
        
        
    % Reset the conditions to be all one condition
        Simulation_Results.conditionIDByCell = ...
            ones(size(Simulation_Results.conditionIDByCell));
        Simulation_Results.conditionIDByTrack = ...
            ones(size(Simulation_Results.conditionIDByTrack));
        Simulation_Results.numConditions = 1;
        
    % Perform the MSD analysis
    
        % Perform the time-averaged MSD vs time
            [Simulation_Results] = ...
                performMSD_AverageByTrackCellAndCondition(Simulation_Results);
        
        % Fit the MSD data
            [Simulation_Results] = ...
                performMSDFit_AverageByTrackCellAndCondition(Simulation_Results);
            
%     % Perform the velocity autocorrelation        
%         [Simulation_Results] = ...
%             performVelocityAutocorrelation_AverageByTrackCellAndCondition(Simulation_Results);
    
%     % Perform analysis of variance
%         [Simulation_Results] = run_ANOVA_TrackCellSessionDay(Simulation_Results);
%         close all hidden; % Close ANOVA tables
    
    % Save the file
    if Simulation_Results.simulationDatasetInfo.doSaveResults
        parsave(Simulation_Results)
    end

    toc
    
                  
end

beep

