This MATLAB code was written by Rikki M. Garner as a companion to the paper "Vast heterogeneity in cytoplasmic diffusion rates revealed by nanorheology and Doppelgänger simulations", available online at: https://doi.org/10.1101/2022.05.11.491518

***NOTE: This code requires MATLAB with the 'Deep Learning' and 'Statistics and Machine Learning' toolboxes to be installed.
You may optionally use the 'Parallel Computing' toolbox to parallelize simulation runs. If you do not have the Parallel Computing toolbox, or simply don't want to use parallelization, comment out lines 126-128 in \Code\Simulation_Generation_Code\Wrappers\*.m and replace "parfor" with "for" on line 130.***

Overview: The code is can be found in the Code folder. The remaining zipped files contain data. The Code folder is distributed into (1) Analysis_Core, which contains functions used to analyze both experimental and simulated data identically. (2) Experimental_Analysis_Code, which contains wrappers used to call the Analysis_Core functions to analyze the experimental data.  (3) Simulation_Analysis_Code, which contains wrappers used to call the Analysis_Core functions to analyze the simulated data. (4) Simulation_Generation_Code, which creates simulated data. And (5) Figure_Code, which can be used to re-gnerate the figures from the data.

1. To download the code directly from the Gitlab repository webpage, click the download button (down arrow icon next to the blue "Clone" button). Unzip the file, and rename the top-most level folder to Garner_Molines_et_al. Alternatively, you can use git command line tools. First, install the git command line tools: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git. Next, open the command prompt and navigate the folder you'd like to download the code. Then type git clone https://gitlab.com/theriot_lab/vast-heterogeneity-in-cytoplasmic-diffusion-rates-revealed-by-nanorheology-and-doppelgaenger-simulations.git Garner_Molines_et_al

2. To run the code, start by opening MATLAB and adding the the Garner_Molines_et_al folder and subfolders to your MATLAB path. Make sure this is the only folder on your path. ***Note: If you are getting an error when loading or saving data, you will need to rename any filepath variables with the full path to the location where the code is stored on your personal computer (e.g. in line 12 of analyzeGEMTracks_LoopThroughDataSetStructureByTracks_Controls.m 'Garner_Molines_et_al\...' will need to be replaced with 'C:\Users\YourUsername\Documents\Garner_Molines_et_al\...')***

3. Unzip all of the data files in place. Due to long filenames in the Simulation_Data folder, you may need to install 7-zip (https://www.7-zip.org/) to unzip this folder.

4. To see how experimental data was analyzed, open \Code\Experimental_Analysis_Code\analyzeGEMTracks_LoopThroughDataSetStructureByTracks_Controls.m (or the corresponding files ending in _CytoDepolym, _Shock, or _Temp). 

5. To see how simulations were generated and analyzed, or to run simlations yourself, go to \Code\Simulation_Generation_Code\Wrappers\*.m

6. To re-create figures, run the file corresponding to the associated figure panel under Figure_Code.

Please feel free to contact me (Rikki) at rikkimgarner@gmail.com for advice and discussion. If you end up using this code, I would love to know about it!
